/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu)
 *				 Shen Zuobing(shcwxz@163.com)
 *   
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * This source plays a role as bridiging disksim and flash simulator. 
 * 
 * Request processing flow: 
 *
 *  1. Request is sent to the simple flash device module. 
 *  2. This interface determines FTL type. Then, it sends the request
 *     to the lower layer according to the FTL type. 
 *  3. It returns total time taken for processing the request in the flash. 
 *  4. The DFTL has been rewritten for SDFTL(Shen Zuobing).
 *  5. 修正连续缓存预取多个而不对连续请求直接处理的问题
 *  6. 综合连续缓存和二级缓存的情况
 *
 *
 */

#include "ssd_interface.h"
#include "disksim_global.h"
#include "dftl.h"

extern int merge_switch_num;
extern int merge_partial_num;
extern int merge_full_num;
int old_merge_switch_num = 0;
int old_merge_partial_num = 0;
int old_merge_full_num= 0;
int old_flash_gc_read_num = 0;
int old_flash_erase_num = 0;
int req_count_num = 1;
int rqst_cnt;
int page_padding_count;
int blk_update_count;
int page_update_count;
int ordinary_request_count;

int cache_cmt_hit;
int cache_scmt_hit;
int cache_slcmt_hit;

//shzb: cache buffer statistics
int buffer_cnt = 0, buffer_hit = 0, buffer_read_hit = 0;

int flag1 = 1;
int count = 0;
int update_flag = 0;//shzb:用于判断连续缓存中的部分映射项更新标志是否置零
translation_read_num = 0;
translation_write_num = 0;

int page_num_for_2nd_map_table;

//shzb:odftl
int indexofseq = 0,indexofarr = 0;
int indexold = 0,indexnew = 0,index2nd=0;
int maxentry=0,MC=0;//分别为出现最多的项的值和出现的次数

int index_cache;
int in_cache_flag;
int page_index;

#define MAP_REAL_MAX_ENTRIES 3274// real map table size in bytes
#define MAP_GHOST_MAX_ENTRIES 822// ghost_num is no of entries chk if this is ok
#define MAP_SEQ_MAX_ENTRIES 1536
#define MAP_SECOND_MAX_ENTRIES 2560//shzb: the second-level cache

#define THRESHOLD 2
#define NUM_ENTRIES_PER_TIME 4

//shzb: for cache algorithm
#define CACHE_MAX_ENTRIES 256
#define INIT_SIZE 32
#define INCREMENT 8
#define NUM_PER_BLK 8

int sequential_count=0;
page_padding_count=0;
blk_update_count = 0;
ordinary_request_count=0;
page_update_count = 0;

int ghost_arr[MAP_GHOST_MAX_ENTRIES];
int real_arr[MAP_REAL_MAX_ENTRIES];
int seq_arr[MAP_SEQ_MAX_ENTRIES];
int second_arr[MAP_SECOND_MAX_ENTRIES];

int cache_arr[CACHE_MAX_ENTRIES];

/***********************************************************************
  Variables for statistics    
 ***********************************************************************/
unsigned int cnt_read = 0;
unsigned int cnt_write = 0;
unsigned int cnt_delete = 0;
unsigned int cnt_evict_from_flash = 0;
unsigned int cnt_evict_into_disk = 0;
unsigned int cnt_fetch_miss_from_disk = 0;
unsigned int cnt_fetch_miss_into_flash = 0;

double sum_of_queue_time = 0.0;
double sum_of_service_time = 0.0;
double sum_of_response_time = 0.0;
unsigned int total_num_of_req = 0;

/***********************************************************************
  Mapping table
 ***********************************************************************/
int real_min = -1;
int real_max = 0;

/***********************************************************************
  Cache
 ***********************************************************************/
int cache_min = -1;
int cache_max = 0;

//shzb: set arguments for BPLRU
struct lru *blk;
//memset(blk,0xFF,INIT_SIZE*sizeof(struct lru));
int blk_index = 0;
int blk_entry_num = 0;
int blksize = INIT_SIZE;

// Interface between disksim & fsim 

void reset_flash_stat()
{
  flash_read_num = 0;
  flash_write_num = 0;
  flash_gc_read_num = 0;
  flash_gc_write_num = 0; 
  flash_erase_num = 0;
  flash_oob_read_num = 0;
  flash_oob_write_num = 0; 
}

FILE *fp_flash_stat;
FILE *fp_gc;
FILE *fp_gc_timeseries;
double gc_di =0 ,gc_ti=0;


double calculate_delay_flash()
{
  double delay;
  double read_delay, write_delay;
  double erase_delay;
  double gc_read_delay, gc_write_delay;
  double oob_write_delay, oob_read_delay;

  oob_read_delay  = (double)OOB_READ_DELAY  * flash_oob_read_num;
  oob_write_delay = (double)OOB_WRITE_DELAY * flash_oob_write_num;

  read_delay     = (double)READ_DELAY  * flash_read_num; 
  write_delay    = (double)WRITE_DELAY * flash_write_num; 
  erase_delay    = (double)ERASE_DELAY * flash_erase_num; 

  gc_read_delay  = (double)GC_READ_DELAY  * flash_gc_read_num; 
  gc_write_delay = (double)GC_WRITE_DELAY * flash_gc_write_num; 


  delay = read_delay + write_delay + erase_delay + gc_read_delay + gc_write_delay + 
    oob_read_delay + oob_write_delay;

  if( flash_gc_read_num > 0 || flash_gc_write_num > 0 || flash_erase_num > 0 ) {
    gc_ti += delay;
  }
  else {
    gc_di += delay;
  }

  if(warm_done == 1){
    fprintf(fp_gc_timeseries, "%d\t%d\t%d\t%d\t%d\t%d\n", 
      req_count_num, merge_switch_num - old_merge_switch_num, 
      merge_partial_num - old_merge_partial_num, 
      merge_full_num - old_merge_full_num, 
      flash_gc_read_num,
      flash_erase_num);

    old_merge_switch_num = merge_switch_num;
    old_merge_partial_num = merge_partial_num;
    old_merge_full_num = merge_full_num;
    req_count_num++;
  }

  reset_flash_stat();

  return delay;
}

//shzb:cache algortihm
double calculate_delay_cache()
{
	double delay;
	double cache_read_delay, cache_write_delay;

	cache_read_delay=(double)CACHE_READ_DELAY * cache_read_num;
	cache_write_delay=(double)CACHE_WRITE_DELAY * cache_write_num;

	delay=cache_read_delay + cache_write_delay;

	reset_cache_stat();

	return delay;
}

void reset_cache_stat()
{
	cache_read_num = 0;
	cache_write_num = 0;
}


/***********************************************************************
  Initialize Flash Drive 
  ***********************************************************************/

void initFlash()
{
  blk_t total_blk_num;
  blk_t total_util_blk_num;
  blk_t total_extr_blk_num;

  // total number of sectors    
  total_util_sect_num  = flash_numblocks;
  total_extra_sect_num = flash_extrblocks;
  total_sect_num = total_util_sect_num + total_extra_sect_num; 

  // total number of blocks 
  total_blk_num      = total_sect_num / SECT_NUM_PER_BLK;     // total block number
  total_util_blk_num = total_util_sect_num / SECT_NUM_PER_BLK;    // total unique block number

  global_total_blk_num = total_util_blk_num;

  total_extr_blk_num = total_blk_num - total_util_blk_num;        // total extra block number

  ASSERT(total_extr_blk_num != 0);

  if (nand_init(total_blk_num, 3) < 0) {
    EXIT(-4); 
  }

  switch(ftl_type){

    // pagemap
    case 1: ftl_op = pm_setup(); break;
    // blockmap
    //case 2: ftl_op = bm_setup(); break;
    // o-pagemap 
    case 3: ftl_op = opm_setup(); break;
    // fast
    case 4: ftl_op = lm_setup(); break;

    default: break;
  }

  ftl_op->init(total_util_blk_num, total_extr_blk_num);

  nand_stat_reset();
}

void printWearout()
{
  int i;
  FILE *fp = fopen("wearout", "w");
  
  for(i = 0; i<nand_blk_num; i++)
  {
    fprintf(fp, "%d %d\n", i, nand_blk[i].state.ec); 
  }

  fclose(fp);
}


void endFlash()
{
  nand_stat_print(outputfile);
  ftl_op->end;
  nand_end();
}  

/***********************************************************************
  Send request (lsn, sector_cnt, operation flag)
  ***********************************************************************/

void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag)
{
	int size;
	//size_t (*op_func)(sect_t lsn, size_t size);
	size_t (*op_func)(sect_t lsn, size_t size, int mapdir_flag);

        if((start_blk_no + block_cnt) >= total_util_sect_num){
          printf("start_blk_no: %d, block_cnt: %d, total_util_sect_num: %d\n", 
              start_blk_no, block_cnt, total_util_sect_num);
          exit(0);
        }

	switch(operation){

	//write
	case 0:

		op_func = ftl_op->write;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;
	//read
	case 1:


		op_func = ftl_op->read;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;

	default: 
		break;
	}
}

void find_real_max()
{
  int i; 

  for(i=0;i < MAP_REAL_MAX_ENTRIES; i++) {
      if(opagemap[real_arr[i]].map_age > opagemap[real_max].map_age) {
          real_max = real_arr[i];
      }
  }
}

void find_real_min()
{
  
  int i,index; 
  int temp = 99999999;

  for(i=0; i < MAP_REAL_MAX_ENTRIES; i++) {
        if(opagemap[real_arr[i]].map_age <= temp) {
            real_min = real_arr[i];
            temp = opagemap[real_arr[i]].map_age;
            index = i;
        }
  }    
}

int find_min_ghost_entry()
{
  int i; 

  int ghost_min = 0;
  int temp = 99999999; 

  for(i=0; i < MAP_GHOST_MAX_ENTRIES; i++) {
    if( opagemap[ghost_arr[i]].map_age <= temp) {
      ghost_min = ghost_arr[i];
      temp = opagemap[ghost_arr[i]].map_age;
    }
  }
  return ghost_min;
}

void init_arr()
{

  int i;
  for( i = 0; i < MAP_REAL_MAX_ENTRIES; i++) {
      real_arr[i] = -1;
  }
  for( i = 0; i < MAP_GHOST_MAX_ENTRIES; i++) {
      ghost_arr[i] = -1;
  }
  for( i= 0; i < MAP_SEQ_MAX_ENTRIES; i++) {
	  seq_arr[i] = -1;
  }
  for( i = 0; i < MAP_SECOND_MAX_ENTRIES; i++){
	  second_arr[i] = -1;
  }
  for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
      cache_arr[i] = -1;
  }

	blk = (struct lru *)malloc(blksize*sizeof(struct lru));
//	memset(blk, 0xFF, blksize*sizeof(struct lru));
    for( i = 0; i < INIT_SIZE; i++) {//shzb: for BPLRU
	  blk[i].blknum = -1;
	  blk[i].age = 0;
  }
}

int search_table(int *arr, int size, int val) 
{
    int i;
    for(i =0 ; i < size; i++) {
        if(arr[i] == val) {
            return i;
        }
    }

    printf("shouldnt come here for search_table()=%d,%d",val,size);
    for( i = 0; i < size; i++) {
      if(arr[i] != -1) {
        printf("arr[%d]=%d ",i,arr[i]);
      }
    }
    exit(1);
    return -1;
}

int find_free_pos( int *arr, int size)
{
    int i;
    for(i = 0 ; i < size; i++) {
        if(arr[i] == -1) {
            return i;
        }
    } 
    printf("shouldn't come here for find_free_pos()\n");
    exit(1);
    return -1;
}

void find_min_cache()
{
  int i; 
  int temp = 999999;

  for(i=0; i < CACHE_MAX_ENTRIES ;i++) {
      if(opagemap[cache_arr[i]].cache_age <= temp ) {
          cache_min = cache_arr[i];
          temp = opagemap[cache_arr[i]].cache_age;
      }
  }
}

void find_MC_entries(int *arr, int size)
{
	int i,j,k;
	int maxCount=1;
	int temp=99999999;
	int *arr1=(int *)malloc(sizeof(int)*size);
	for(k=0;k < size;k++)
		*(arr1+k)=(arr[k]-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE;
	for(i=size-1;i>=0;i--)
	{
		if(arr1[i]!=temp)
		{
			for(j=i-1;j>=0;j--)
			{
				if(arr1[j]==arr1[i])
				{
					maxCount++;
					arr1[j]=temp;
				}
			}
			if(maxCount>=MC)
			{
				MC=maxCount;
				maxentry=arr1[i];
			}
			maxCount=1;
		}
	}
	free(arr1);
//	return i;
}

int not_in_cache(unsigned int pageno)
{
	int flag = 0;
	unsigned int i;
	for(i=pageno;i<pageno+NUM_ENTRIES_PER_TIME;i++)
	{
		if((opagemap[i].map_status == 0)||(opagemap[i].map_status == MAP_INVALID))
			flag=1;
		else
		{
			flag=0;
			break;
		}
	}
	return flag;
}

//shzb:the next 6 functions used in BPLRU
void find_min_blk()
{
	int i;
	int temp = 999999999;
	for(i=0; i < blksize; i++) {
		if((blk[i].age <= temp)&&(blk[i].blknum >= 0)) {
			cache_min = i;
			temp = blk[i].age;
		}
	}
}

void find_max_blk()
{
  int i; 

  for(i=0;i < blksize; i++) {
      if((blk[i].age > blk[cache_max].age)&&(blk[i].blknum >= 0)) {
          cache_max = i;
      }
  }
}

int find_free_blk_pos(struct lru *arr, int size)
{
    int i;
    for(i = 0 ; i < size; i++) {
        if(arr[i].blknum == -1) {
            return i;
        }
    } 
    printf("shouldnt come here for find_free_blk_pos()\n");
    exit(1);
    return -1;
}

int search_blk_table(struct lru *arr, int size, int val)
{
    int i;
    for(i =0 ; i < size; i++) {
        if(arr[i].blknum == val) {
            return i;
        }
    }

    printf("shouldnt come here for search_blk_table()=%d,%d",val,size);
    for( i = 0; i < size; i++) {
      if(arr[i].blknum != -1) {
        printf("arr[%d].blknum=%d ",i,arr[i].blknum);
      }
    }
    exit(1);
    return -1;
}

int Isincache(unsigned int number,int num)
{
	int i = 0, j = 0;
	while((i < blksize)&&(j < blk_entry_num))
	{
		if(number/num == blk[i].blknum)
			return 1;
		if(-1!=blk[i].blknum)
			j++;
		i++;
	}
	return 0;
}

int Is_blk_in_cache(int number,int num)
{
	int i;
	for(i = number; i < (number+num); i++)
		if(opagemap[i].cache_status != CACHE_REAL)
			return 0;
	return 1;
}

int youkim_flag1=0;

double cacheManage(unsigned int secno, int scount, int operation)
{
	double delay, flash_delay = 0, cache_delay = 0;
	int bcount;
	unsigned int blkno,pageno;
	int cnt;

	int page_pos = -1;

	if(ftl_type ==1){}

	if(ftl_type == 3) {
	    page_num_for_2nd_map_table = (opagemap_num / MAP_ENTRIES_PER_PAGE);
    
    	if(youkim_flag1 == 0 ) {
			youkim_flag1 = 1;
        	init_arr();
    	}

    	if((opagemap_num % MAP_ENTRIES_PER_PAGE) != 0){
			page_num_for_2nd_map_table++;
    	}
	}
	// page based FTL 
	if(ftl_type == 1 ) { 
	   blkno = secno / 4;
	   bcount = (secno + scount -1)/4 - (secno)/4 + 1;
	}  
	// block based FTL 
	else if(ftl_type == 2){
		blkno = secno/4;
		bcount = (secno + scount -1)/4 - (secno)/4 + 1;
	}
	// o-pagemap scheme
	else if(ftl_type == 3 ) { 
		blkno = secno / 4;
//		blkno += page_num_for_2nd_map_table;
		bcount = (secno + scount -1)/4 - (secno)/4 + 1;
	}  
	// FAST scheme
	else if(ftl_type == 4){
		blkno = secno/4;
		bcount = (secno + scount -1)/4 - (secno)/4 + 1;
	}

	cnt = bcount;
	switch(cache_type)
	{
		case 1://BPLRU
			while(cnt > 0)
			{
				cnt--;
				if(operation==0)
				{
					buffer_cnt++;
//shzb:1. cache hit, the blkno is in cache
					if(opagemap[blkno].cache_status == CACHE_REAL)
					{
						buffer_hit++;
						blk_index = search_blk_table(blk,blksize,blkno/64);
						if(!Is_blk_in_cache((blkno/64)*64,64))
						{
							blk[blk_index].age = blk[cache_max].age + 1;
							cache_max = blk_index;
						}
						else
						{
							find_min_blk();
							blk[blk_index].age=blk[cache_min].age-1;
							cache_min = blk_index;
							find_max_blk();
						}
						cache_write_num++;
					}
//shzb:2. cache miss,load the entry into cache
					else
					{
//shzb:2.1 the cache buffer is full
						if((CACHE_MAX_ENTRIES-CACHE_REAL_NUM_ENTRIES)==0)
						{
							find_min_blk();//shzb: cache_min
							//shzb:page padding
							for(pageno = blk[cache_min].blknum*64; pageno < (blk[cache_min].blknum+1)*64; pageno++)
							{
								if(opagemap[pageno].cache_status == CACHE_REAL)
								{
									opagemap[pageno].cache_status = CACHE_INVALID;
									page_pos = search_table(cache_arr,CACHE_MAX_ENTRIES,pageno);
									cache_arr[page_pos] = -1;
									CACHE_REAL_NUM_ENTRIES--;
									cache_read_num++;
								}
								else
								{
									flash_delay += callFsim(pageno*4,4,1);
									page_padding_count++;
								}
							}
							flash_delay += callFsim(blk[cache_min].blknum*4*64,4*64,0);
							blk_index = cache_min;
							blk[blk_index].blknum = -1;
							blk[blk_index].age = 0;
							blk_entry_num--;
							find_max_blk();
							blk_update_count++;
						}
//shzb:2.2 the cache buffer is not full
//shzb:2.2.1 The block number of the page is not in blk[i] array.
						if(!Isincache(blkno,64))
						{
//shzb:2.2.1.1 The blk array is full, so add the capcity of blk array.
							if((blksize-blk_entry_num) == 0)
							{
								blk = realloc(blk,sizeof(struct lru)*(blksize+INCREMENT));
								for(blk_index = blksize; blk_index < (blksize+INCREMENT); blk_index++)
								{
									blk[blk_index].blknum = -1;
									blk[blk_index].age = 0;
								}
								blksize += INCREMENT;
							}
//shzb:2.2.1.2 Add the blk contain the blkno page into the blk array.
							blk_index = find_free_blk_pos(blk,blksize);
							blk[blk_index].blknum = blkno/64;
							blk[blk_index].age = blk[cache_max].age + 1;
							cache_max = blk_index;
							blk_entry_num++;
							opagemap[blkno].cache_status = CACHE_REAL;
							page_pos = find_free_pos(cache_arr,CACHE_MAX_ENTRIES);
							cache_arr[page_pos] = blkno;
							CACHE_REAL_NUM_ENTRIES++;
							cache_write_num++;
						}
//shzb:2.2.2 The block number of the page is in blk[i] array
						else
						{
							opagemap[blkno].cache_status = CACHE_REAL;
							page_pos = find_free_pos(cache_arr,CACHE_MAX_ENTRIES);
							cache_arr[page_pos] = blkno;
							CACHE_REAL_NUM_ENTRIES++;
							cache_write_num++;
							blk_index = search_blk_table(blk,blksize,blkno/64);
							if(Is_blk_in_cache((blkno/64)*64,64))
							{
								find_min_blk();
								blk[blk_index].age = blk[cache_min].age - 1;
								cache_min = blk_index;
								find_max_blk();
							}
							else
							{
								blk[blk_index].age = blk[cache_max].age + 1;
								cache_max = blk_index;
							}
						}
					}
				}
				else
				{
					if((opagemap[blkno].cache_status == CACHE_REAL)&&(cnt>0))
					{
						buffer_read_hit++;
						cache_read_num++;
					}
					else
					{
						in_cache_flag=0;
						for(index_cache=blkno;index_cache<(cnt+1);index_cache++)
						{
							if(opagemap[index_cache].cache_status==CACHE_REAL)
								in_cache_flag=1;
						}
						if(in_cache_flag==0)
						{
							flash_delay+=callFsim(blkno*4,(cnt+1)*4,1);
							ordinary_request_count+=cnt;
							cnt=0;
						}
						else
							flash_delay+=callFsim(blkno*4,4,1);
					}

					ordinary_request_count++;
				}
				blkno++;
			}
		break;
	case 2://WA-BPLRU
			while(cnt > 0)
			{
				cnt--;
				if(operation==0)
				{
					buffer_cnt++;
//shzb:1. cache hit, the blkno is in cache
					if(opagemap[blkno].cache_status == CACHE_REAL)
					{
						buffer_hit++;
						blk_index = search_blk_table(blk,blksize,blkno/NUM_PER_BLK);
						if(!Is_blk_in_cache((blkno/NUM_PER_BLK)*NUM_PER_BLK,NUM_PER_BLK))
						{
							blk[blk_index].age = blk[cache_max].age + 1;
							cache_max = blk_index;
						}
						cache_write_num++;
					}
//shzb:2. cache miss,load the entry into cache
					else if((cnt+1)<8)
					{
//shzb:2.1 the cache buffer is full
						if((CACHE_MAX_ENTRIES-CACHE_REAL_NUM_ENTRIES)==0)
						{
							find_min_blk();//shzb: cache_min
//shzb:evitct entries from cache
							for(pageno = blk[cache_min].blknum*NUM_PER_BLK; pageno < (blk[cache_min].blknum+1)*NUM_PER_BLK; pageno++)
							{
								if(opagemap[pageno].cache_status == CACHE_REAL)
								{
									opagemap[pageno].cache_status = CACHE_INVALID;
									page_pos = search_table(cache_arr,CACHE_MAX_ENTRIES,pageno);
									cache_arr[page_pos] = -1;
									CACHE_REAL_NUM_ENTRIES--;
									cache_read_num++;
									flash_delay+=callFsim(pageno*4,4,0);
									page_padding_count++;
								}
							}
							blk_index = cache_min;
							blk[blk_index].blknum = -1;
							blk[blk_index].age = 0;
							blk_entry_num--;
							find_max_blk();
							blk_update_count++;
						}
//shzb:2.2 the cache buffer is not full
//shzb:2.2.1 The block number of the page is not in blk[i] array.
						if(!Isincache(blkno,NUM_PER_BLK))
						{
//shzb:2.2.1.1 The blk array is full, so add the capcity of blk array.
							if((blksize-blk_entry_num) == 0)
							{
								blk = realloc(blk,sizeof(struct lru)*(blksize+INCREMENT));
								for(blk_index = blksize; blk_index < (blksize+INCREMENT); blk_index++)
								{
									blk[blk_index].blknum = -1;
									blk[blk_index].age = 0;
								}
								blksize += INCREMENT;
							}
//shzb:2.2.1.2 Add the blk contain the blkno page into the blk array.
							blk_index = find_free_blk_pos(blk,blksize);
							blk[blk_index].blknum = blkno/NUM_PER_BLK;
							blk[blk_index].age = blk[cache_max].age + 1;
							cache_max = blk_index;
							blk_entry_num++;
							opagemap[blkno].cache_status = CACHE_REAL;
							page_pos = find_free_pos(cache_arr,CACHE_MAX_ENTRIES);
							cache_arr[page_pos] = blkno;
							CACHE_REAL_NUM_ENTRIES++;
							cache_write_num++;
						}
//shzb:2.2.2 The block number of the page is in blk[i] array
						else
						{
							opagemap[blkno].cache_status = CACHE_REAL;
							page_pos = find_free_pos(cache_arr,CACHE_MAX_ENTRIES);
							cache_arr[page_pos] = blkno;
							CACHE_REAL_NUM_ENTRIES++;
							cache_write_num++;
							blk_index = search_blk_table(blk,blksize,blkno/NUM_PER_BLK);
							if(Is_blk_in_cache((blkno/NUM_PER_BLK)*NUM_PER_BLK,NUM_PER_BLK))
							{
								find_min_blk();
								blk[blk_index].age = blk[cache_min].age - 1;
								cache_min = blk_index;
								find_max_blk();
							}
							else
							{
								blk[blk_index].age = blk[cache_max].age + 1;
								cache_max = blk_index;
							}
						}
					}
					else
					{
						in_cache_flag=0;
						for(index_cache=blkno;index_cache<(blkno+cnt+1);index_cache++)
						{
							if(opagemap[index_cache].cache_status==CACHE_REAL)
							{
								in_cache_flag=1;
								continue;
							}
						}
						if(in_cache_flag==0)
						{
							flash_delay+=callFsim(blkno*4,(cnt+1)*4,operation);
							ordinary_request_count+=cnt;
							cnt=0;
						}
						else
						{
//shzb:2.1 the cache buffer is full
						if((CACHE_MAX_ENTRIES-CACHE_REAL_NUM_ENTRIES)==0)
						{
							find_min_blk();//shzb: cache_min
//shzb:evict entries from cache
							for(pageno = blk[cache_min].blknum*NUM_PER_BLK; pageno < (blk[cache_min].blknum+1)*NUM_PER_BLK; pageno++)
							{
								if(opagemap[pageno].cache_status == CACHE_REAL)
								{
									opagemap[pageno].cache_status = CACHE_INVALID;
									page_pos = search_table(cache_arr,CACHE_MAX_ENTRIES,pageno);
									cache_arr[page_pos] = -1;
									CACHE_REAL_NUM_ENTRIES--;
									cache_read_num++;
									flash_delay+=callFsim(pageno*4,4,0);
									page_padding_count++;
								}
							}
							blk_index = cache_min;
							blk[blk_index].blknum = -1;
							blk[blk_index].age = 0;
							blk_entry_num--;
							find_max_blk();
							blk_update_count++;
						}
//shzb:2.2 the cache buffer is not full
//shzb:2.2.1 The block number of the page is not in blk[i] array.
						if(!Isincache(blkno,NUM_PER_BLK))
						{
//shzb:2.2.1.1 The blk array is full, so add the capcity of blk array.
							if((blksize-blk_entry_num) == 0)
							{
								blk = realloc(blk,sizeof(struct lru)*(blksize+INCREMENT));
								for(blk_index = blksize; blk_index < (blksize+INCREMENT); blk_index++)
								{
									blk[blk_index].blknum = -1;
									blk[blk_index].age = 0;
								}
								blksize += INCREMENT;
							}
//shzb:2.2.1.2 Add the blk contain the blkno page into the blk array.
							blk_index = find_free_blk_pos(blk,blksize);
							blk[blk_index].blknum = blkno/NUM_PER_BLK;
							blk[blk_index].age = blk[cache_max].age + 1;
							cache_max = blk_index;
							blk_entry_num++;
							opagemap[blkno].cache_status = CACHE_REAL;
							page_pos = find_free_pos(cache_arr,CACHE_MAX_ENTRIES);
							cache_arr[page_pos] = blkno;
							CACHE_REAL_NUM_ENTRIES++;
							cache_write_num++;
						}
//shzb:2.2.2 The block number of the page is in blk[i] array
						else
						{
							opagemap[blkno].cache_status = CACHE_REAL;
							page_pos = find_free_pos(cache_arr,CACHE_MAX_ENTRIES);
							cache_arr[page_pos] = blkno;
							CACHE_REAL_NUM_ENTRIES++;
							cache_write_num++;
							blk_index = search_blk_table(blk,blksize,blkno/NUM_PER_BLK);
							if(Is_blk_in_cache((blkno/NUM_PER_BLK)*NUM_PER_BLK,NUM_PER_BLK))
							{
								find_min_blk();
								blk[blk_index].age = blk[cache_min].age - 1;
								cache_min = blk_index;
								find_max_blk();
							}
							else
							{
								blk[blk_index].age = blk[cache_max].age + 1;
								cache_max = blk_index;
							}
						}
						}
					}
				}
				else
				{
					if((opagemap[blkno].cache_status == CACHE_REAL)&&(cnt>0))
					{
						buffer_read_hit++;
						cache_read_num++;
						buffer_cnt++;
					}
					else
					{
						in_cache_flag=0;
						for(index_cache=blkno;index_cache<(blkno+cnt+1);index_cache++)
						{
							if(opagemap[index_cache].cache_status==CACHE_REAL)
								in_cache_flag=1;
						}
						if(in_cache_flag==0)
						{
							flash_delay+=callFsim(blkno*4,(cnt+1)*4,1);
							ordinary_request_count+=cnt+1;
							cnt=0;
						}
						else
						{
							flash_delay+=callFsim(blkno*4,4,1);
							ordinary_request_count++;
						}
					}
				}
				blkno++;
			}
		break;
	case 3://lru test
		while(cnt > 0)
		{
			cnt--;
			buffer_cnt++;
//shzb:1. cache hit, the blkno is in cache
			if(opagemap[blkno].cache_status == CACHE_REAL)
			{
				buffer_hit++;
				page_index = search_table(cache_arr,CACHE_MAX_ENTRIES,blkno);
				opagemap[blkno].cache_age = opagemap[cache_max].cache_age + 1;
				cache_max = page_index;
				if(operation==0)
				{
					cache_write_num++;
					opagemap[blkno].update_flag=1;
				}
				else
					cache_read_num++;
			}
//shzb:2. cache miss,load the entry into cache
			else if((cnt+1)<8)
			{
//shzb:2.1 the cache buffer is full
				if((CACHE_MAX_ENTRIES-CACHE_REAL_NUM_ENTRIES)==0)
				{
					find_min_cache();//shzb: cache_min
					page_index=search_table(cache_arr,CACHE_MAX_ENTRIES,cache_min);
					opagemap[cache_min].cache_status=CACHE_INVALID;
					opagemap[cache_min].cache_age = 0;
					cache_arr[page_index] = -1;
					CACHE_REAL_NUM_ENTRIES--;
					page_update_count++;
					if(opagemap[cache_min].update_flag==1)
						flash_delay+=callFsim(cache_min*4,4,0);
				}
				page_index=find_free_pos(cache_arr,CACHE_MAX_ENTRIES);
				cache_arr[page_index] = blkno;
				opagemap[blkno].cache_status = CACHE_REAL;
				opagemap[blkno].cache_age = opagemap[cache_max].cache_age+1;
				cache_max = blkno;
				CACHE_REAL_NUM_ENTRIES++;
				if(operation==0)
					opagemap[blkno].update_flag=1;
				else
					flash_delay+=callFsim(blkno*4,4,1);
				cache_write_num++;
			}
			else
			{
				in_cache_flag=0;
				for(index_cache=blkno;index_cache<(cnt+1);index_cache++)
				{
					if(opagemap[index_cache].cache_status==CACHE_REAL)
						in_cache_flag=1;
				}
				if(in_cache_flag==0)
				{
					flash_delay+=callFsim(blkno*4,(cnt+1)*4,operation);
					ordinary_request_count+=cnt;
					cnt=0;
				}
				else
					flash_delay+=callFsim(blkno*4,4,operation);
			}
			blkno++;
		}
		break;
	default:
		printf("Can not detect which cache algorithm is used!\n");
		exit(0);
	}
	cache_delay = calculate_delay_cache();
	delay = flash_delay + cache_delay;
	return delay;	
}

double callFsim(unsigned int secno, int scount, int operation)
{
  double delay; 
  int bcount;
  unsigned int blkno; // pageno for page based FTL
  int cnt,z; int min_ghost;

  int pos=-1,pos_real=-1,pos_ghost=-1,pos_2nd=-1;

  if(ftl_type == 1){ }

  if(ftl_type == 3) {
      page_num_for_2nd_map_table = (opagemap_num / MAP_ENTRIES_PER_PAGE);
    
      if(youkim_flag1 == 0 ) {
        youkim_flag1 = 1;
        init_arr();
      }

      if((opagemap_num % MAP_ENTRIES_PER_PAGE) != 0){
        page_num_for_2nd_map_table++;
      }
  }
      
  // page based FTL 
  if(ftl_type == 1 ) { 
    blkno = secno / 4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // block based FTL 
  else if(ftl_type == 2){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }
  // o-pagemap scheme
  else if(ftl_type == 3 ) { 
    blkno = secno / 4;
    blkno += page_num_for_2nd_map_table;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // FAST scheme
  else if(ftl_type == 4){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }

  cnt = bcount;

  switch(operation)
  {
    //write/read
    case 0:
    case 1:

    while(cnt > 0)
    {
          cnt--;

        // page based FTL
        if(ftl_type == 1){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // blck based FTL
        else if(ftl_type == 2){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // opagemap ftl scheme
        else if(ftl_type == 3)
        {

          /************************************************
            primary map table 
          *************************************************/
          //1. pagemap in SRAM 

            rqst_cnt++;
          if((opagemap[blkno].map_status == MAP_REAL) || (opagemap[blkno].map_status == MAP_GHOST))
          {
            cache_cmt_hit++;

            opagemap[blkno].map_age++;

            if(opagemap[blkno].map_status == MAP_GHOST){

              if ( real_min == -1 ) {
                real_min = 0;
                find_real_min();
              }    
              if(opagemap[real_min].map_age <= opagemap[blkno].map_age) 
              {
                find_real_min();  // probably the blkno is the new real_min alwaz
                opagemap[blkno].map_status = MAP_REAL;
                opagemap[real_min].map_status = MAP_GHOST;

                pos_ghost = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,blkno);
                ghost_arr[pos_ghost] = -1;
                
                pos_real = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos_real] = -1;

                real_arr[pos_real]   = blkno; 
                ghost_arr[pos_ghost] = real_min; 
              }
            }
            else if(opagemap[blkno].map_status == MAP_REAL) 
            {
              if ( real_max == -1 ) {
                real_max = 0;
                find_real_max();
                printf("Never happend\n");
              }

              if(opagemap[real_max].map_age <= opagemap[blkno].map_age)
              {
                real_max = blkno;
              }  
            }
            else {
              printf("forbidden/shouldnt happen real =%d , ghost =%d\n",MAP_REAL,MAP_GHOST);
            }
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
		  continue;
          }
		//2.shzb:请求在连续缓存中
		else if((opagemap[blkno].map_status == MAP_SEQ)||(opagemap[blkno].map_status == MAP_SECOND))
		{
//			cache_hit++;
			if(opagemap[blkno].map_status == MAP_SEQ){
				cache_scmt_hit++;
				opagemap[blkno].map_age++;
			if(opagemap[blkno].map_age >1)
			{
			//if PATC is full
            if((MAP_REAL_MAX_ENTRIES - MAP_REAL_NUM_ENTRIES) == 0)
            {
              if((MAP_GHOST_MAX_ENTRIES - MAP_GHOST_NUM_ENTRIES) == 0)
              { //evict one entry from ghost cache to DRAM or Disk, delay = DRAM or disk write, 1 oob write for invalidation 
                min_ghost = find_min_ghost_entry();
                  evict++;

                if(opagemap[min_ghost].update == 1)
				{
					if((MAP_SECOND_MAX_ENTRIES-MAP_SECOND_NUM_ENTRIES) ==0)
					{
						MC=0;
						find_MC_entries(second_arr,MAP_SECOND_MAX_ENTRIES);
						send_flash_request(maxentry*4,4,1,2);
						translation_read_num++;
						send_flash_request(maxentry*4,4,0,2);
						translation_write_num++;
						for(indexold = 0;indexold < MAP_SECOND_MAX_ENTRIES; indexold++)
						{
							if(((second_arr[indexold]-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE) == maxentry)
							{
								opagemap[second_arr[indexold]].update = 0;
								opagemap[second_arr[indexold]].map_status = MAP_INVALID;
								opagemap[second_arr[indexold]].map_age = 0;
								second_arr[indexold]=-1;
								MAP_SECOND_NUM_ENTRIES--;
							}
						}
					}
					opagemap[min_ghost].map_status = MAP_SECOND;
					MAP_GHOST_NUM_ENTRIES--;
					pos_2nd = find_free_pos(second_arr,MAP_SECOND_MAX_ENTRIES);
					second_arr[pos_2nd]=-1;
					second_arr[pos_2nd]=min_ghost;
					MAP_SECOND_NUM_ENTRIES++;
					if(MAP_SECOND_NUM_ENTRIES > MAP_SECOND_MAX_ENTRIES)
					{
						printf("The second cache is overflow!\n");
						exit(0);
					}
                }
				else
				{
                opagemap[min_ghost].map_status = MAP_INVALID;
				opagemap[min_ghost].map_age = 0;
                MAP_GHOST_NUM_ENTRIES--;
				}

                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                MAP_GHOST_NUM_ENTRIES++;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;

                pos = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,min_ghost);
                ghost_arr[pos]=-1;

                
                ghost_arr[pos]= real_min;
                
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;
              }
              else{
                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;
               
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;

                pos = find_free_pos(ghost_arr,MAP_GHOST_MAX_ENTRIES);
                ghost_arr[pos]=real_min;
                
                MAP_GHOST_NUM_ENTRIES++;
              }
            }
            opagemap[blkno].map_status = MAP_REAL;

            opagemap[blkno].map_age = opagemap[real_max].map_age + 1;
            real_max = blkno;
            MAP_REAL_NUM_ENTRIES++;
            
            pos = find_free_pos(real_arr,MAP_REAL_MAX_ENTRIES);
            real_arr[pos] = blkno;
			}
			}
			else
				cache_slcmt_hit++;
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
		  continue;
		}
		//3.shzb:连续请求加入连续缓存中
		else if((cnt+1) >= THRESHOLD)//shzb:THRESHOLD=2,表示大于或等于4KB的请求，当作连续请求来处理。
		{
			if(not_in_cache(blkno))
			{
			opagemap[blkno].map_age++;
			if((MAP_SEQ_MAX_ENTRIES-MAP_SEQ_NUM_ENTRIES)==0)
			{
				for(indexofarr = 0;indexofarr < NUM_ENTRIES_PER_TIME;indexofarr++)
				{
					if((opagemap[seq_arr[indexofarr]].update == 1)&&(opagemap[seq_arr[indexofarr]].map_status == MAP_SEQ))
					{
						update_reqd++;
						update_flag=1;
						opagemap[seq_arr[indexofarr]].update=0;
						opagemap[seq_arr[indexofarr]].map_status = MAP_INVALID;
						opagemap[seq_arr[indexofarr]].map_age = 0;
					}
					else if((opagemap[seq_arr[indexofarr]].update == 0)&&(opagemap[seq_arr[indexofarr]].map_status ==MAP_SEQ))
					{
						opagemap[seq_arr[indexofarr]].map_status = MAP_INVALID;
						opagemap[seq_arr[indexofarr]].map_age = 0;
					}
				}
				if(update_flag == 1)
				{
					send_flash_request(((seq_arr[0]-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4,4,1,2);
					translation_read_num++;
					send_flash_request(((seq_arr[0]-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4,4,0,2);
					translation_write_num++;
					update_flag=0;
				}
				for(indexofarr = 0;indexofarr <= MAP_SEQ_MAX_ENTRIES-1-NUM_ENTRIES_PER_TIME; indexofarr++)
					seq_arr[indexofarr] = seq_arr[indexofarr+NUM_ENTRIES_PER_TIME];
				MAP_SEQ_NUM_ENTRIES-=NUM_ENTRIES_PER_TIME;
			}
			flash_hit++;
			send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);
			translation_read_num++;
			for(indexofseq=0; indexofseq < NUM_ENTRIES_PER_TIME;indexofseq++)//NUM_ENTRIES_PER_TIME在这里表示一次加载4个映射表信息
			{
				opagemap[blkno+indexofseq].map_status=MAP_SEQ;
				seq_arr[MAP_SEQ_NUM_ENTRIES] = (blkno+indexofseq);
				MAP_SEQ_NUM_ENTRIES++;
			}
			if(MAP_SEQ_NUM_ENTRIES > MAP_SEQ_MAX_ENTRIES)
			{
				printf("The sequential cache is overflow!\n");
				exit(0);
			}
			if(operation==0)
			{
				write_count++;
				opagemap[blkno].update=1;
			}
			else
				read_count++;
			send_flash_request(blkno*4,4,operation,1);
			blkno++;
			sequential_count = 0;
			for(;(cnt>0)&&(sequential_count<NUM_ENTRIES_PER_TIME-1);cnt--)
			{
				opagemap[blkno].map_age++;
				cache_scmt_hit++;
				if(operation==0)
				{
					write_count++;
					opagemap[blkno].update=1;
				}
				else
					read_count++;
				send_flash_request(blkno*4,4,operation,1);
				blkno++;
				rqst_cnt++;
				sequential_count++;
			}
			continue;
			}
		}

          //4. opagemap not in SRAM 
            //if map table in SRAM is full
            if((MAP_REAL_MAX_ENTRIES - MAP_REAL_NUM_ENTRIES) == 0)
            {
              if((MAP_GHOST_MAX_ENTRIES - MAP_GHOST_NUM_ENTRIES) == 0)
              { //evict one entry from ghost cache to DRAM or Disk, delay = DRAM or disk write, 1 oob write for invalidation 
                min_ghost = find_min_ghost_entry();
                  evict++;

                if(opagemap[min_ghost].update == 1)
				{
					if((MAP_SECOND_MAX_ENTRIES-MAP_SECOND_NUM_ENTRIES) == 0)
					{
						MC=0;
						find_MC_entries(second_arr,MAP_SECOND_MAX_ENTRIES);
						send_flash_request(maxentry*4,4,1,2);
						translation_read_num++;
						send_flash_request(maxentry*4,4,0,2);
						translation_write_num++;
						for(indexold = 0;indexold < MAP_SECOND_MAX_ENTRIES; indexold++)
						{
							if(((second_arr[indexold]-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE) == maxentry)
							{
								opagemap[second_arr[indexold]].update = 0;
								opagemap[second_arr[indexold]].map_status = MAP_INVALID;
								opagemap[second_arr[indexold]].map_age = 0;
								second_arr[indexold] = -1;
								MAP_SECOND_NUM_ENTRIES--;
							}
						}
					}
					opagemap[min_ghost].map_status = MAP_SECOND;
					MAP_GHOST_NUM_ENTRIES--;
					pos_2nd = find_free_pos(second_arr,MAP_SECOND_MAX_ENTRIES);
					second_arr[pos_2nd]=-1;
					second_arr[pos_2nd]=min_ghost;
					MAP_SECOND_NUM_ENTRIES++;
					if(MAP_SECOND_NUM_ENTRIES > MAP_SECOND_MAX_ENTRIES)
					{
						printf("The second cache is overflow!\n");
						exit(0);
					}
                }
				else
				{ 
                opagemap[min_ghost].map_status = MAP_INVALID;
				opagemap[min_ghost].map_age = 0;
                MAP_GHOST_NUM_ENTRIES--;
				}

                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                MAP_GHOST_NUM_ENTRIES++;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;

                pos = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,min_ghost);
                ghost_arr[pos]=-1;

                
                ghost_arr[pos]= real_min;
                
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;
              }
              else{
                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;
               
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;

                pos = find_free_pos(ghost_arr,MAP_GHOST_MAX_ENTRIES);
                ghost_arr[pos]=real_min;
                
                MAP_GHOST_NUM_ENTRIES++;
              }
            }

            flash_hit++;
            send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table
			translation_read_num++;
            opagemap[blkno].map_status = MAP_REAL;

            opagemap[blkno].map_age = opagemap[real_max].map_age + 1;
            real_max = blkno;
            MAP_REAL_NUM_ENTRIES++;
            
            pos = find_free_pos(real_arr,MAP_REAL_MAX_ENTRIES);
            real_arr[pos] = blkno;
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // FAST scheme  
        else if(ftl_type == 4){ 

          if(operation == 0){
            write_count++;
          }
          else read_count++;

          send_flash_request(blkno*4, 4, operation, 1); //cache_min is a page for page baseed FTL
          blkno++;
        }
    }
    break;
  }

  delay = calculate_delay_flash();

  return delay;
}

