/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu_
 *   
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * This source plays a role as bridiging disksim and flash simulator. 
 * 
 * Request processing flow: 
 *
 *  1. Request is sent to the simple flash device module. 
 *  2. This interface determines FTL type. Then, it sends the request
 *     to the lower layer according to the FTL type. 
 *  3. It returns total time taken for processing the request in the flash. 
 *
 */

#include "ssd_interface.h"
#include "disksim_global.h"
#include "dftl.h"

extern int merge_switch_num;
extern int merge_partial_num;
extern int merge_full_num;
int old_merge_switch_num = 0;
int old_merge_partial_num = 0;
int old_merge_full_num= 0;
int old_flash_gc_read_num = 0;
int old_flash_erase_num = 0;
int req_count_num = 1;
int cache_hit, rqst_cnt;
int flag1 = 1;
int count = 0;

int page_num_for_2nd_map_table;

#define MAP_REAL_MAX_ENTRIES 6552// real map table size in bytes
#define MAP_GHOST_MAX_ENTRIES 1640// ghost_num is no of entries chk if this is ok

#define CACHE_MAX_ENTRIES 4096
int ghost_arr[MAP_GHOST_MAX_ENTRIES];
int real_arr[MAP_REAL_MAX_ENTRIES];
int cache_arr[CACHE_MAX_ENTRIES];

/***********************************************************************
  Variables for statistics    
 ***********************************************************************/
unsigned int cnt_read = 0;
unsigned int cnt_write = 0;
unsigned int cnt_delete = 0;
unsigned int cnt_evict_from_flash = 0;
unsigned int cnt_evict_into_disk = 0;
unsigned int cnt_fetch_miss_from_disk = 0;
unsigned int cnt_fetch_miss_into_flash = 0;

double sum_of_queue_time = 0.0;
double sum_of_service_time = 0.0;
double sum_of_response_time = 0.0;
unsigned int total_num_of_req = 0;

/**********************************************
 * 	ZHOU JIE-end
**********************************************/
int lru_cache_arr[CACHE_MAX_ENTRIES] ;
int clru_cache_arr[CACHE_MAX_ENTRIES] ;
int dlru_cache_arr[CACHE_MAX_ENTRIES] ;
/***********************************************/
int ObserveCycle=1000;
int CycleCount=0;
double DelaySum=0.0;
double AveDelay=0.0;
//last
int Last_buffer_read_miss=0;
int Last_bufer_write_miss=0;
int Last_buffer_cnt=0;
int Last_buffer_miss_cnt=0;
//curr
int Curr_buffer_read_miss;
int Curr_buffer_write_miss;
int Curr_buffer_cnt;
int Curr_buff_miss_cnt;
/**********************************************
 * 	ZHOU JIE-end
**********************************************/

/***********************************************************************
  Mapping table
 ***********************************************************************/
int real_min = -1;
int real_max = 0;

/***********************************************************************
  Cache
 ***********************************************************************/
int cache_min = -1;
int cache_max = 0;

// Interface between disksim & fsim 

void reset_flash_stat()
{
  flash_read_num = 0;
  flash_write_num = 0;
  flash_gc_read_num = 0;
  flash_gc_write_num = 0; 
  flash_erase_num = 0;
  flash_oob_read_num = 0;
  flash_oob_write_num = 0; 
}

FILE *fp_flash_stat;
FILE *fp_gc;
FILE *fp_gc_timeseries;
double gc_di =0 ,gc_ti=0;


double calculate_delay_flash()
{
  double delay;
  double read_delay, write_delay;
  double erase_delay;
  double gc_read_delay, gc_write_delay;
  double oob_write_delay, oob_read_delay;

  oob_read_delay  = (double)OOB_READ_DELAY  * flash_oob_read_num;
  oob_write_delay = (double)OOB_WRITE_DELAY * flash_oob_write_num;

  read_delay     = (double)READ_DELAY  * flash_read_num; 
  write_delay    = (double)WRITE_DELAY * flash_write_num; 
  erase_delay    = (double)ERASE_DELAY * flash_erase_num; 

  gc_read_delay  = (double)GC_READ_DELAY  * flash_gc_read_num; 
  gc_write_delay = (double)GC_WRITE_DELAY * flash_gc_write_num; 


  delay = read_delay + write_delay + erase_delay + gc_read_delay + gc_write_delay + 
    oob_read_delay + oob_write_delay;

  if( flash_gc_read_num > 0 || flash_gc_write_num > 0 || flash_erase_num > 0 ) {
    gc_ti += delay;
  }
  else {
    gc_di += delay;
  }

  if(warm_done == 1){
    fprintf(fp_gc_timeseries, "%d\t%d\t%d\t%d\t%d\t%d\n", 
      req_count_num, merge_switch_num - old_merge_switch_num, 
      merge_partial_num - old_merge_partial_num, 
      merge_full_num - old_merge_full_num, 
      flash_gc_read_num,
      flash_erase_num);

    old_merge_switch_num = merge_switch_num;
    old_merge_partial_num = merge_partial_num;
    old_merge_full_num = merge_full_num;
    req_count_num++;
  }

  reset_flash_stat();

  return delay;
}


/***********************************************************************
  Initialize Flash Drive 
  ***********************************************************************/

void initFlash()
{
  blk_t total_blk_num;
  blk_t total_util_blk_num;
  blk_t total_extr_blk_num;

  // total number of sectors    
  total_util_sect_num  = flash_numblocks;
  total_extra_sect_num = flash_extrblocks;
  total_sect_num = total_util_sect_num + total_extra_sect_num; 

  // total number of blocks 
  total_blk_num      = total_sect_num / SECT_NUM_PER_BLK;     // total block number
  total_util_blk_num = total_util_sect_num / SECT_NUM_PER_BLK;    // total unique block number

  global_total_blk_num = total_util_blk_num;

  total_extr_blk_num = total_blk_num - total_util_blk_num;        // total extra block number

  ASSERT(total_extr_blk_num != 0);

  if (nand_init(total_blk_num, 3) < 0) {
    EXIT(-4); 
  }

  switch(ftl_type){

    // pagemap
    case 1: ftl_op = pm_setup(); break;
    // blockmap
    //case 2: ftl_op = bm_setup(); break;
    // o-pagemap 
    case 3: ftl_op = opm_setup(); break;
    // fast
    case 4: ftl_op = lm_setup(); break;

    default: break;
  }

  ftl_op->init(total_util_blk_num, total_extr_blk_num);

  nand_stat_reset();
}

void printWearout()
{
  int i;
  FILE *fp = fopen("wearout", "w");
  
  for(i = 0; i<nand_blk_num; i++)
  {
    fprintf(fp, "%d %d\n", i, nand_blk[i].state.ec); 
  }

  fclose(fp);
}


void endFlash()
{
  nand_stat_print(outputfile);
  ftl_op->end;
  nand_end();
}  

/***********************************************************************
  Send request (lsn, sector_cnt, operation flag)
  ***********************************************************************/

void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag)
{
	int size;
	//size_t (*op_func)(sect_t lsn, size_t size);
	size_t (*op_func)(sect_t lsn, size_t size, int mapdir_flag);

        if((start_blk_no + block_cnt) >= total_util_sect_num){
          printf("start_blk_no: %d, block_cnt: %d, total_util_sect_num: %d\n", 
              start_blk_no, block_cnt, total_util_sect_num);
          exit(0);
        }

	switch(operation){

	//write
	case 0:

		op_func = ftl_op->write;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;
	//read
	case 1:


		op_func = ftl_op->read;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;

	default: 
		break;
	}
}

void find_real_max()
{
  int i; 

  for(i=0;i < MAP_REAL_MAX_ENTRIES; i++) {
      if(opagemap[real_arr[i]].map_age > opagemap[real_max].map_age) {
          real_max = real_arr[i];
      }
  }
}

void find_real_min()
{
  
  int i,index; 
  int temp = 99999999;

  for(i=0; i < MAP_REAL_MAX_ENTRIES; i++) {
        if(opagemap[real_arr[i]].map_age <= temp) {
            real_min = real_arr[i];
            temp = opagemap[real_arr[i]].map_age;
            index = i;
        }
  }    
}

int find_min_ghost_entry()
{
  int i; 

  int ghost_min = 0;
  int temp = 99999999; 

  for(i=0; i < MAP_GHOST_MAX_ENTRIES; i++) {
    if( opagemap[ghost_arr[i]].map_age <= temp) {
      ghost_min = ghost_arr[i];
      temp = opagemap[ghost_arr[i]].map_age;
    }
  }
  return ghost_min;
}

void init_arr()
{

  int i;
  for( i = 0; i < MAP_REAL_MAX_ENTRIES; i++) {
      real_arr[i] = -1;
  }
  for( i = 0; i < MAP_GHOST_MAX_ENTRIES; i++) {
      ghost_arr[i] = -1;
  }
  for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
      cache_arr[i] = -1;
  }

}

int search_table(int *arr, int size, int val) 
{
    int i;
    for(i =0 ; i < size; i++) {
        if(arr[i] == val) {
            return i;
        }
    }

    printf("shouldnt come here for search_table()=%d,%d",val,size);
    for( i = 0; i < size; i++) {
      if(arr[i] != -1) {
        printf("arr[%d]=%d ",i,arr[i]);
      }
    }
    exit(1);
    return -1;
}

int find_free_pos( int *arr, int size)
{
    int i;
    for(i = 0 ; i < size; i++) {
        if(arr[i] == -1) {
            return i;
        }
    } 
//    printf("shouldnt come here for find_free_pos()");
//    exit(1);
    return -1;
}

void find_min_cache()
{
  int i; 
  int temp = 999999;

  for(i=0; i < CACHE_MAX_ENTRIES ;i++) {
      if(opagemap[cache_arr[i]].cache_age <= temp ) {
          cache_min = cache_arr[i];
          temp = opagemap[cache_arr[i]].cache_age;
      }
  }
}

int youkim_flag1=0;

double callFsim(unsigned int secno, int scount, int operation)
{
  double delay; 
  int bcount;
  unsigned int blkno; // pageno for page based FTL
  int cnt,z; int min_ghost;

  int pos=-1,pos_real=-1,pos_ghost=-1;

  if(ftl_type == 1){ }

  if(ftl_type == 3) {
      page_num_for_2nd_map_table = (opagemap_num / MAP_ENTRIES_PER_PAGE);
    
      if(youkim_flag1 == 0 ) {
        youkim_flag1 = 1;
        init_arr();
      }

      if((opagemap_num % MAP_ENTRIES_PER_PAGE) != 0){
        page_num_for_2nd_map_table++;
      }
  }
      
  // page based FTL 
  if(ftl_type == 1 ) { 
    blkno = secno / 4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // block based FTL 
  else if(ftl_type == 2){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }
  // o-pagemap scheme
  else if(ftl_type == 3 ) { 
    blkno = secno / 4;
    blkno += page_num_for_2nd_map_table;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // FAST scheme
  else if(ftl_type == 4){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }

  cnt = bcount;

  switch(operation)
  {
    //write/read
    case 0:
    case 1:

    while(cnt > 0)
    {
          cnt--;

        // page based FTL
        if(ftl_type == 1){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // blck based FTL
        else if(ftl_type == 2){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // opagemap ftl scheme
        else if(ftl_type == 3)
        {

          /************************************************
            primary map table 
          *************************************************/
          //1. pagemap in SRAM 

            rqst_cnt++;
          if((opagemap[blkno].map_status == MAP_REAL) || (opagemap[blkno].map_status == MAP_GHOST))
          {
            cache_hit++;

            opagemap[blkno].map_age++;

            if(opagemap[blkno].map_status == MAP_GHOST){

              if ( real_min == -1 ) {
                real_min = 0;
                find_real_min();
              }    
              if(opagemap[real_min].map_age <= opagemap[blkno].map_age) 
              {
                find_real_min();  // probably the blkno is the new real_min alwaz
                opagemap[blkno].map_status = MAP_REAL;
                opagemap[real_min].map_status = MAP_GHOST;

                pos_ghost = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,blkno);
                ghost_arr[pos_ghost] = -1;
                
                pos_real = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos_real] = -1;

                real_arr[pos_real]   = blkno; 
                ghost_arr[pos_ghost] = real_min; 
              }
            }
            else if(opagemap[blkno].map_status == MAP_REAL) 
            {
              if ( real_max == -1 ) {
                real_max = 0;
                find_real_max();
                printf("Never happend\n");
              }

              if(opagemap[real_max].map_age <= opagemap[blkno].map_age)
              {
                real_max = blkno;
              }  
            }
            else {
              printf("forbidden/shouldnt happen real =%d , ghost =%d\n",MAP_REAL,MAP_GHOST);
            }
          }

          //2. opagemap not in SRAM 
          else
          {
            //if map table in SRAM is full
            if((MAP_REAL_MAX_ENTRIES - MAP_REAL_NUM_ENTRIES) == 0)
            {
              if((MAP_GHOST_MAX_ENTRIES - MAP_GHOST_NUM_ENTRIES) == 0)
              { //evict one entry from ghost cache to DRAM or Disk, delay = DRAM or disk write, 1 oob write for invalidation 
                min_ghost = find_min_ghost_entry();
                  evict++;

                if(opagemap[min_ghost].update == 1) {
                  update_reqd++;
                  opagemap[min_ghost].update = 0;
                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table then update it

                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 0, 2);   // write into 2nd mapping table 
                } 
                opagemap[min_ghost].map_status = MAP_INVALID;

                MAP_GHOST_NUM_ENTRIES--;

                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                MAP_GHOST_NUM_ENTRIES++;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;

                pos = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,min_ghost);
                ghost_arr[pos]=-1;

                
                ghost_arr[pos]= real_min;
                
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;
              }
              else{
                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;
               
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;

                pos = find_free_pos(ghost_arr,MAP_GHOST_MAX_ENTRIES);
                ghost_arr[pos]=real_min;
                
                MAP_GHOST_NUM_ENTRIES++;
              }
            }

            flash_hit++;
            send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table

            opagemap[blkno].map_status = MAP_REAL;

            opagemap[blkno].map_age = opagemap[real_max].map_age + 1;
            real_max = blkno;
            MAP_REAL_NUM_ENTRIES++;
            
            pos = find_free_pos(real_arr,MAP_REAL_MAX_ENTRIES);
            real_arr[pos] = blkno;
          }

         //comment out the next line when using cache
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // FAST scheme  
        else if(ftl_type == 4){ 

          if(operation == 0){
            write_count++;
          }
          else read_count++;

          send_flash_request(blkno*4, 4, operation, 1); //cache_min is a page for page baseed FTL
          blkno++;
        }
    }
    break;
  }

  delay = calculate_delay_flash();

  return delay;
}


/********************************************
 * 	Zhou Jie ----------Function
 *******************************************/
//reset cache state
void reset_cache_stat()
{
    cache_read_num = 0;
    cache_write_num = 0;
}

//计算cache的操作时延
double calculate_delay_cache()
{
    double delay;
    double cache_read_delay=0.0,cache_write_delay=0.0;
    cache_read_delay=(double)CACHE_READ_DELAY*cache_read_num;
    cache_write_delay=(double)CACHE_WRITE_DELAY*cache_write_num;
    delay=cache_read_delay+cache_write_delay;
    reset_cache_stat();
    return delay;
}

//初始化对应统计变量
void InitVariable()
{
    int i,j;
    cache_max_index=0;
    cache_min_index=0;
    //初始化统计变量
    buffer_cnt=0;
    buffer_hit_cnt=0;
    buffer_miss_cnt=0;
    buffer_write_hit=0;
    buffer_write_miss=0;
    buffer_read_hit=0;
    buffer_read_miss=0;
    physical_read=0;
    physical_write=0;
    //初始化相应的CACHE中间变量
    CACHE_LRU_NUM_ENTRIES=0;
    CACHE_CLRU_NUM_ENTRIES=0;
    CACHE_DLRU_NUM_ENTRIES=0;
    //周期调整Tau的值设定
    UpdateTauCycle=CACHE_MAX_ENTRIES*1;
    T_count=0;
    //阈值设定为0.5
    Threshold=0.5;
    //启动页填充的阈值
    TriggerThreshold=PAGE_NUM_PER_BLK*0.5;
    //初始值为总buffer的一半
    Tau=CACHE_MAX_ENTRIES/2;
    //初始化观测周期内的统计变量
    CDHit_CRH=0;
    CDHit_CWH=0;
    CDHit_DRH=0;
    CDHit_DWH=0;
    //用来计算周期内读写的综合延迟是多少，用来计算周期内平均的读写延迟
    cycle_physical_read=0;
    cycle_physical_write=0;
    cycle_flash_read_delay=0.0;
    cycle_flash_write_delay=0.0;
}

void FreeVariable()
{
    //释放对应的内存
    if(NandPage!=NULL)
    {
        free(NandPage);
    }
    if(BlkTable!=NULL)
    {
        free(BlkTable);
    }
}

//输出对应的统计结果
void PrintResultStat()
{
    //输出和缓冲区统计相关的信息
    printf("****************************************\n");
    printf("the all buffer req count is %d\n",buffer_cnt);
    printf("the buffer miss count is %d\n",buffer_miss_cnt);
    printf("the hit rate is %f\n",(double)(buffer_cnt-buffer_miss_cnt)/buffer_cnt);
    printf("the buffer read miss count is %d\n",buffer_read_miss);
    printf("the buffer write miss count is %d\n",buffer_write_miss);
    printf("the buffer read hit count is %d\n",buffer_read_hit);
    printf("the buffer write hit count is %d\n",buffer_write_hit);
    printf("the physical read count is %d\n",physical_read);
    printf("the physical write count is %d\n",physical_write);
}

int my_find_cache_max(int *arr,int arrMaxSize)
{
    int i;
    int temp=-1;
    int cache_max=-1;
    for ( i = 0; i <arrMaxSize ; ++i) {
        if (arr[i]!=-1){
            if (NandPage[arr[i]].cache_age>temp){
                temp=NandPage[arr[i]].cache_age;
                cache_max=arr[i];
            }
        }
    }
    return cache_max;
}

//找到数组中索引对应的LPN的age最大或最小,返回的是LPN号
int my_find_cache_min(int *arr,int arrMaxSize)
{
    int i;
    int temp=99999999;
    int cache_min=-1;
    for (i = 0; i <arrMaxSize ; ++i) {
        //attention arr[i]=-1 ,the NandPage[-1] is error
        if(arr[i]!=-1){
            if(NandPage[arr[i]].cache_age<temp){
                temp=NandPage[arr[i]].cache_age;
                cache_min=arr[i];
            }
        }
    }
    return cache_min;
}

//计算数组中非负数个数
int calculate_arr_positive_num(int *arr,int size)
{
    int i;
    int count=0;
    for ( i = 0; i <size ; ++i) {
        if(arr[i]>=0){
            count++;
        }
    }
    return count;
}

//初始化数组，参考init_arr
void init_my_arr()
{
    int i;
    for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
        lru_cache_arr[i] = -1;
        clru_cache_arr[i] =-1;
        dlru_cache_arr[i] =-1;
    }
}

int myMax(int a,int b)
{
    if(a>b)
        return a;
    else
        return b;
}

int myMin(int a,int b)
{
    if(a>b)
        return b;
    else
        return a;
}


//重置对应的NandPage的状态为初始状态的值
void ResetNandPageStat(int LPN)
{
    //先做一个错误，是否存在LPN超出NandPage的大小
    if(LPN>NandPage_Num)
    {
        printf("error happened in ResetPageStat function\n");
        printf("the LPN:%d over the database volume\n",LPN);
        printf("the NandPage Num is %d\n",NandPage_Num);
        exit(1);
    }
    NandPage[LPN].cache_age=0;
    NandPage[LPN].cache_status=My_CACHE_INVALID;
    NandPage[LPN].cache_update=0;
}

//将数据插入到(int)arr数组指定的位置pos，pos之后的数据往后挪动一位
int InsertArr(int *arr,int size,int data,int pos)
{
    int j;
    //首先做一个错误检测
    if(pos>=size&&pos<0){
        printf("error happend in InsertArr: Insert-pos:%d over bound:0-%d",pos,size-1);
        exit(-1);
    }
    for ( j = size-1; j >pos ; j--) {
        arr[j]=arr[j-1];
    }
    //在pos的位置插入数据
    arr[pos]=data;
    return 0;
}

/********************Function Core**********************************/

//该函数完成对目标Tau的自适应的更新
int UpdateTau()
{
    int D_Tau,TempTau,MinTau,MaxTau;//表示脏队列的目标长度
    double B_CLRU,B_DLRU;//表示各自队列的单位收益
    //四射五入
    MinTau=(int)(MinTauRatio*CACHE_MAX_ENTRIES+0.5);
    MaxTau=(int)(MaxTauRatio*CACHE_MAX_ENTRIES+0.5);
    D_Tau=CACHE_MAX_ENTRIES-Tau;
    //未引入从底层得到flash读写延迟的代码
    B_CLRU=(CDHit_CWH*FLASH_WRITE_DELAY*wAmp+CDHit_CRH*FLASH_READ_DELAY)/Tau;
    B_DLRU=(CDHit_DWH*FLASH_WRITE_DELAY*wAmp+CDHit_DRH*FLASH_READ_DELAY)/D_Tau;
/*
    //如果从底层得到周期的读写时延
    ave_flash_read_delay=cycle_flash_read_delay/cycle_physical_read;
    ave_flash_write_delay=cycle_flash_write_delay/cycle_physical_write;
    B_CLRU=(CDHit_CRH*ave_flash_read_delay+CDHit_CWH*ave_flash_write_delay)/Tau;
    B_DLRU=(CDHit_DRH*ave_flash_read_delay+CDHit_CWH*ave_flash_write_delay)/D_Tau;
*/

    //四舍五入
    TempTau=(int)((B_CLRU/(B_CLRU+B_DLRU)*CACHE_MAX_ENTRIES)+0.5);
    TempTau=myMax(MinTau,TempTau);
    TempTau=myMin(MaxTau,TempTau);
    Tau=TempTau;

    /*
    //适当的嵌入代码调试的测试语句
    printf("----------------------------------------------------------\n");
    printf("The Cycle Stat is:\n");
    printf("CLRU write Hit is %d\t Read Hit is %d\n",CDHit_CWH,CDHit_CRH);
    printf("DLRU write Hit is %d\t Read Hit is %d\n",CDHit_DWH,CDHit_DRH);
    printf("sum write delay is %f\t read delay is %f\n",cycle_flash_write_delay,cycle_flash_read_delay);
    printf("ave write delay is %f\t read delay is %f\n",ave_flash_write_delay,ave_flash_read_delay);
    printf("cycle physical write is %d\t physical read is %d\n",cycle_physical_write,cycle_physical_read);
    printf("-----------------------------------------------------------\n");
    */

    //重置相应的周期统计变量
    CDHit_CWH=0;
    CDHit_CRH=0;
    CDHit_DRH=0;
    CDHit_DWH=0;
    T_count=1;
    cycle_physical_write=0;
    cycle_physical_read=0;
    cycle_flash_write_delay=0.0;
    cycle_flash_read_delay=0.0;
    //返回更新后的Tau值，以备输出测试
    return Tau;
}


//命中CLRU的操作
void HitCLRU(int LPN,int operation)
{
    int HitIndex,tempBlkNum;
    int victim=-1;
    int free_pos=-1,NewIndex;
    int i;
    int DL,CL;
    //首先更新对应的NandPage的age状态,之后别忘了更新其他的状态
    NandPage[LPN].cache_age=NandPage[cache_max_index].cache_age+1;
    cache_max_index=LPN;
    //从CLRU的数组中找到存放LPN的位置
    HitIndex=search_table(clru_cache_arr,CACHE_MAX_ENTRIES,LPN);
    //错误判读
    if(HitIndex==-1){
        printf("error happend in HitCLRU :can not Find LPN %d in clru_cache_arr\n",LPN);
        exit(-1);
    }

    //根据命中的类型（写需要移动到DLRU队列中去，更复杂）
    if(operation!=0){
        //读命中,不需要移动数据项
        CDHit_CRH++;
        buffer_read_hit++;
        cache_read_num++;
    }else{
        //写命中，需要移动数据项
        CDHit_CWH++;
        buffer_write_hit++;
        cache_write_num++;
        //删除clru_cache中的数据
        clru_cache_arr[HitIndex]=-1;
        CACHE_CLRU_NUM_ENTRIES--;
        //将其存入DLRU队列
        free_pos=find_free_pos(dlru_cache_arr,CACHE_MAX_ENTRIES);
        if(free_pos==-1){
            printf("error happen in HitCLRU, can not find free_pos in dlr-cache-arr\n");
            exit(-1);
        }
        dlru_cache_arr[free_pos]=LPN;
        CACHE_DLRU_NUM_ENTRIES++;
        //更新NandPage的标志位
        NandPage[LPN].cache_status=CACHE_INDLRU;
        NandPage[LPN].cache_update=1;
        //针对CLRU和DLRU的长度做一个错误检测
        if(CACHE_CLRU_NUM_ENTRIES!=calculate_arr_positive_num(clru_cache_arr,CACHE_MAX_ENTRIES)){
            printf("error happened in HitCLRU,the clru-size error\n");
            printf("CACHE_CLRU_NUM_ENTRIES is %d\n",CACHE_CLRU_NUM_ENTRIES);
            printf("clru list num is %d\n",calculate_arr_positive_num(clru_cache_arr,CACHE_MAX_ENTRIES));
            exit(-1);
        }
        if(CACHE_DLRU_NUM_ENTRIES!=calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES)){
            printf("error happened in HitCLRU,the dlru-size error\n");
            printf("CACHE_DLRU_NUM_ENTRIES is %d\n",CACHE_DLRU_NUM_ENTRIES);
            printf("dlru list num is %d\n",calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES));
            exit(-1);
        }
        //更新块索引的数据
        //计算对应的块索引
        tempBlkNum=LPN/PAGE_NUM_PER_BLK;
        //删除对应clist上的索引(HitIndex)，将新的dlru位置索引(free_pos)加入dlist
        victim=search_table(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK,HitIndex);
        //错误检测
        if(victim==-1){
            printf("error happend in HitInCLRU can not find HitIndex:%d In BLkTable[%d]-Clist\n",HitIndex,tempBlkNum);
            printf("the Clist Num is %d\t CleanNum is %d\n",CL,BlkTable[tempBlkNum].CleanNum);
            //依次打印输出当前的Clist的队列的值
            for(i=0;i<PAGE_NUM_PER_BLK;i++){
                if(BlkTable[tempBlkNum].Clist[i]!=-1){
                    printf("%d\n",BlkTable[tempBlkNum].Clist[i]);
                }
            }
            exit(-1);
        }
        BlkTable[tempBlkNum].Clist[victim]=-1;
        BlkTable[tempBlkNum].CleanNum--;
        NewIndex=free_pos;//NewIndex是之前dlru_cache_arr的位置索引
        free_pos=find_free_pos(BlkTable[tempBlkNum].Dlist,PAGE_NUM_PER_BLK);
        //错误检测
        if(free_pos==-1){
            printf("error happend in HitCLRU can not find free_pos in BLKTable[%d]-Dlist for NewIndex %d\n",tempBlkNum,NewIndex);
            printf("the Dlist Num is %d\t DirtyNum is %d\n",DL,BlkTable[tempBlkNum].DirtyNum);
            //依次打印输出当前的Dlist的队列的值
            for(i=0;i<PAGE_NUM_PER_BLK;i++){
                if(BlkTable[tempBlkNum].Dlist[i]!=-1){
                    printf("%d\n",BlkTable[tempBlkNum].Dlist[i]);
                }
            }
            exit(-1);
        }
        BlkTable[tempBlkNum].Dlist[free_pos]=NewIndex;
        BlkTable[tempBlkNum].DirtyNum++;
        //错误检测需要的值
        CL=calculate_arr_positive_num(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK);
        DL=calculate_arr_positive_num(BlkTable[tempBlkNum].Dlist,PAGE_NUM_PER_BLK);
//        做一个错误检测判断CL和DL是否一直一致
        if(CL!=BlkTable[tempBlkNum].CleanNum||DL!=BlkTable[tempBlkNum].DirtyNum){
            printf("error happend in HitCLRU,Clist or Dlist size is error\n");
            printf("the Dlist Num is %d\t DirtyNum is %d\n",DL,BlkTable[tempBlkNum].DirtyNum);
            printf("the Clist Num is %d\t CleanNum is %d\n",CL,BlkTable[tempBlkNum].CleanNum);
            exit(-1);
        }
    }

}


//命中DLRU的操作
void HitDLRU(int LPN,int operation)
{
    //更新对应的NandPage的状态标识
    NandPage[LPN].cache_age=NandPage[cache_max_index].cache_age+1;
    cache_max_index=LPN;
    if(operation==0){
        CDHit_DWH++;
        cache_write_num++;
        buffer_write_hit++;
    }else{
        CDHit_DRH++;
        cache_read_num++;
        buffer_read_hit++;
    }
}


//删除CLRU的lru位置的数据项
void DelLPNInCLRU()
{
    int MinAgeLPN,Victim;
    //BlkTable删除需要的中间变量
    int ClistVictim,ClistIndex;
    int tempBlkNum;
    //错误检测
    if(CACHE_CLRU_NUM_ENTRIES<=0){
        printf("error happend in DelLPNInCLRU: can not del CLRU entry\n");
        printf("CACHE_CLRU_NUM_ENTRIES is %d\n",CACHE_CLRU_NUM_ENTRIES);
        exit(-1);
    }
    //注意这里返回的是LPN号,不是位置索引
    MinAgeLPN=my_find_cache_min(clru_cache_arr,CACHE_MAX_ENTRIES);
    //重置NandPage相关的状态
    ResetNandPageStat(MinAgeLPN);
    Victim=search_table(clru_cache_arr,CACHE_MAX_ENTRIES,MinAgeLPN);
    //错误检测
    if(Victim==-1){
        printf("error happend in DelLPNInCLRU: can not find MinAgeLPN %d In clru-cache-arr\n",MinAgeLPN);
        exit(1);
    }
    //删除clru中的数据
    clru_cache_arr[Victim]=-1;
    CACHE_CLRU_NUM_ENTRIES--;
    //删除对应的LPN的块索引
    tempBlkNum=MinAgeLPN/PAGE_NUM_PER_BLK;
//    遍历找到对应删除的MinAgeLPN(LPN)-->Victim(ClistVictim)在Clist上的位置(ClistIndex)
    ClistVictim=Victim;
    ClistIndex=search_table(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK,ClistVictim);
    //错误检测
    if(ClistIndex==-1){
        printf("error happend in DelLPNInCLRU: can not find ClistVictim %d In BlkTable[%d]-Clist\n",ClistVictim,tempBlkNum);
        exit(1);
    }
    //删除对应的数据项
    BlkTable[tempBlkNum].Clist[ClistIndex]=-1;
    //对应的统计量的修改
    BlkTable[tempBlkNum].BlkSize--;
    BlkTable[tempBlkNum].CleanNum--;
    //错误检测
    if(BlkTable[tempBlkNum].CleanNum+BlkTable[tempBlkNum].DirtyNum!=BlkTable[tempBlkNum].BlkSize){
        printf("error happend in DelLPNinCLRU: BlkTableSize is error\n");
        printf("BlkTable[%d]-CleanNum is %d\t DirtyNum is %d\t BlkSize is %d\n",tempBlkNum,BlkTable[tempBlkNum].CleanNum,BlkTable[tempBlkNum].DirtyNum,BlkTable[tempBlkNum].BlkSize);
        printf("Clist-num is %d\t Dlist num is %d\n",calculate_arr_positive_num(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK),calculate_arr_positive_num(BlkTable[tempBlkNum].Dlist,PAGE_NUM_PER_BLK));
        exit(-1);
    }
    //错误检测
    if(BlkTable[tempBlkNum].CleanNum!=calculate_arr_positive_num(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK)){
        printf("error happend in DelLPNinCLRU:Clist size is error\n");
        printf("BlkTable[%d]-CleanNum is %d\t Clist-num is %d\n",tempBlkNum,BlkTable[tempBlkNum].CleanNum,calculate_arr_positive_num(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK));
        exit(-1);
    }
}


//根据冷热区分的BoundAge选择部分的热脏页保留,冷脏页剔除
//参数说明，KeepCluster和VictimCluster都是在dlru-cache-arr的位置索引,BoudAge
//函数返回的是保持脏页的个数
int FindKeepCluser(int Dlist[],int DirtyNum,int *KeepCluster,int *VictimCluster,int BoundAge)
{
    int K_index,V_index,Index,L,j;
    //先计算Dlist中的有效数据项
    L=calculate_arr_positive_num(Dlist,PAGE_NUM_PER_BLK);
    //错误检测
    if(L!=DirtyNum){
        printf("error happend in FindKeepCluster:\n");
        printf("Dlist-num is %d\t DirtyNum is %d\n",L,DirtyNum);
        exit(1);
    }
    for ( Index = 0,j=0,K_index=0,V_index=0; Index <PAGE_NUM_PER_BLK&&j<L ; ++Index) {
//        记住Dlist存的是dlru_cache_arr的位置索引
        if (Dlist[Index]!=-1){
            j++;
            if(NandPage[dlru_cache_arr[Dlist[Index]]].cache_age>=BoundAge){
                KeepCluster[K_index]=Dlist[Index];
                K_index++;
            }else{
                VictimCluster[V_index]=Dlist[Index];
                V_index++;
            }

        }
    }
    //做个错误判断
    if(K_index+V_index!=L){
        printf("error happend in FindKeepCluster\n");
        printf("KeepCluster-size:%d+VictimCluster-size:%d!=Dlist-size:%d\n",K_index,V_index,L);
        exit(1);
    }
    //返回需要保留的页数个数
    return K_index;
}


//该函数是调用底层的flash回写函数,将blk块中零散的脏页聚簇连续的回写
//输入的参数BlkNum是要聚簇删除的脏页块号,Alogrithm是否选择启用页填充算法(1:yes,0:no)
double ClusterWriteToFlash(int BlkNum,int Alogrithm)
{
    double delay=0.0,temp_delay=0.0;
    int i;
    int ReqStart,ReqSize=0;
    int offset;
    if(Alogrithm==0) {
//        不启用页填充的算法，只是聚簇回写
//        首先定位到脏页在该块中的第一个起始位置
        ReqStart = BlkNum * PAGE_NUM_PER_BLK;
        for (i = 0; i < PAGE_NUM_PER_BLK; ++i) {
            if (NandPage[ReqStart].cache_status == CACHE_INDLRU) {
                offset = ReqStart;
                ReqSize = 1;
                break;
            }
            ReqStart++;
        }
//      开始连续重构回写
        for (i = ReqStart; i < (BlkNum + 1) * PAGE_NUM_PER_BLK; ++i) {
            //出现中断,考虑将积累的连续请求一次回写(前提有请求,reqSize!=0)
            if (NandPage[i].cache_status != CACHE_INDLRU) {
                if (ReqSize != 0) {
                    physical_write += ReqSize;
                    cycle_physical_write += ReqSize;
//              注意代码内嵌的时候调用这里的函数
					temp_delay=callFsim(offset*4,ReqSize*4,0);
//                    temp_delay = ReqSize * FLASH_WRITE_DELAY;
                    cycle_flash_write_delay += temp_delay;
                    delay += temp_delay;
//              同时注意删除后重置对应的标记
                    ReqSize = 0;
                }
            }

            if (NandPage[i].cache_status == CACHE_INDLRU) {
                //说明之前刚刚聚簇回写过一次
                if (ReqSize == 0) {
                    offset = i;
                    ReqSize = 1;
                } else {
                    ReqSize++;
                }

            }
        }//end-for
        
        /***************debug********************/
        if(ReqSize!=0){
			physical_write+=ReqSize;
			cycle_physical_write+=ReqSize;
			temp_delay=callFsim(offset*4,ReqSize*4,0);
			cycle_flash_write_delay+=temp_delay;
			delay+=temp_delay;
			ReqSize=0;
		}
        
    } else{
        //启用页填充的算法
        //依次读取数据页到缓冲区
        ReqStart=BlkNum*PAGE_NUM_PER_BLK;
        for ( i = ReqStart; i <PAGE_NUM_PER_BLK+ReqStart ; ++i) {
            if(NandPage[i].cache_status==My_CACHE_INVALID){
                physical_read++;
                cycle_physical_read++;
//               temp_delay=FLASH_READ_DELAY;
                temp_delay=callFsim(i*4,4,1);
                cycle_flash_read_delay+=temp_delay;
                delay+=temp_delay;
            }
        }
//        整块回写
        physical_write+=PAGE_NUM_PER_BLK;
        cycle_physical_write+=PAGE_NUM_PER_BLK;
//        temp_delay=FLASH_WRITE_DELAY*PAGE_NUM_PER_BLK;
        temp_delay=callFsim(ReqStart*4,PAGE_NUM_PER_BLK*4,0);
        cycle_flash_read_delay+=temp_delay;
        delay+=temp_delay;
    }
    return delay;
}

/*************************************************************************************************************
 * 该函数完成将dlru-cache-arr的保留热数据页移动到CLRU中,BlkNum是操作的块号,Keeplist是要保留的脏页在dlru-cache-arr中的位置索引
 * 同时完成了对NandPage的状态修改，块索引的修改
**************************************************************************************************************/
int MoveDLRUToCLRU(int BlkNum,int *Keeplist,int KeepSize,int *VictimCluster,int VictimSize)
{
    int free_pos=-1,clru_index;
    int VictimIndex;
    int tempKeep,tempLPN,tempVictim;
    int i;
//    保留部分的热数据页到clru
    for ( i = 0; i <KeepSize ; ++i) {
        tempKeep=Keeplist[i];
        tempLPN=dlru_cache_arr[tempKeep];
        //修改对应的NandPage的状态
        NandPage[tempLPN].cache_status=CACHE_INCLRU;
        NandPage[tempLPN].cache_update=0;
//          将其加入到clru_cache中
        free_pos=find_free_pos(clru_cache_arr,CACHE_MAX_ENTRIES);
        if(free_pos==-1){
            printf("error happend in MoveDLRUToCLRU:can not find free pos for tempLPN %d\n",tempLPN);
            exit(-1);
        }
//        将脏页变为干净页的LPN存入到clru-cache
        clru_cache_arr[free_pos]=tempLPN;
        CACHE_CLRU_NUM_ENTRIES++;
        clru_index=free_pos;
//        更新Clist（块索引的）
        free_pos=find_free_pos(BlkTable[BlkNum].Clist,PAGE_NUM_PER_BLK);
        if(free_pos==-1){
            printf("error happend in MoveDLRUToCLRU:can not find free pos for clru_index in Clist %d\n",clru_index);
            exit(-1);
        }

        BlkTable[BlkNum].Clist[free_pos]=clru_index;
        BlkTable[BlkNum].CleanNum++;

        /****************错误检测********************/
        if(BlkTable[BlkNum].CleanNum!=calculate_arr_positive_num(BlkTable[BlkNum].Clist,PAGE_NUM_PER_BLK)){
            printf("error happend in MoveDLRUToCLRU:Clist size is error\n");
            printf("BlkTable[%d]-CleanNum is %d\t Clist-num is %d\n",BlkNum,BlkTable[BlkNum].CleanNum,calculate_arr_positive_num(BlkTable[BlkNum].Clist,PAGE_NUM_PER_BLK));
            exit(-1);
        }

//      删除dlru中关于脏页的LPN
        dlru_cache_arr[tempKeep]=-1;
        CACHE_DLRU_NUM_ENTRIES--;
//      删除Dlist中的索引
        VictimIndex=search_table(BlkTable[BlkNum].Dlist,PAGE_NUM_PER_BLK,tempKeep);
        if(VictimIndex==-1){
            printf("error happened in MoveDLRUToCLRU:can not find tempKeep %d In BlkTable[%d]-Dlist",tempKeep,BlkNum);
            exit(-1);
        }
        BlkTable[BlkNum].Dlist[VictimIndex]=-1;
        BlkTable[BlkNum].DirtyNum--;

        /****************错误检测********************/
        if(BlkTable[BlkNum].DirtyNum!=calculate_arr_positive_num(BlkTable[BlkNum].Dlist,PAGE_NUM_PER_BLK)){
            printf("error happend in MoveDLRUToCLRU:Dlist size is error and Save HotDirty\n");
            printf("BlkTable[%d]-DirtyNum is %d\t Dlist-num is %d\n",BlkNum,BlkTable[BlkNum].DirtyNum,calculate_arr_positive_num(BlkTable[BlkNum].Dlist,PAGE_NUM_PER_BLK));
            exit(-1);
        }
//
    }

//    删除冷的数据页包含其索引
    for ( i = 0; i <VictimSize ; ++i) {
//        注意tempVictim都只是dlru-cache-arr的位置索引
        tempVictim=VictimCluster[i];
        tempLPN=dlru_cache_arr[tempVictim];
//        重置NandPage的状态
        ResetNandPageStat(tempLPN);
//        直接删除dlru中关于LPN
        dlru_cache_arr[tempVictim]=-1;
        CACHE_DLRU_NUM_ENTRIES--;
//       删除Dlist中的索引
        VictimIndex=search_table(BlkTable[BlkNum].Dlist,PAGE_NUM_PER_BLK,tempVictim);
        if(VictimIndex==-1){
            printf("error happened in MoveDLRUToCLRU:can not find tempVictim %d In BlkTable[%d]-Dlist",tempVictim,BlkNum);
            exit(-1);
        }
        BlkTable[BlkNum].Dlist[VictimIndex]=-1;
        BlkTable[BlkNum].DirtyNum--;
//      同时注意更新对应的块的当前大小
        BlkTable[BlkNum].BlkSize--;

        /****************错误检测********************/
        if(BlkTable[BlkNum].DirtyNum!=calculate_arr_positive_num(BlkTable[BlkNum].Dlist,PAGE_NUM_PER_BLK)){
            printf("error happend in MoveDLRUToCLRU:Dlist size is error and Delete cold dirty\n");
            printf("BlkTable[%d]-DirtyNum is %d\t Dlist-num is %d\n",BlkNum,BlkTable[BlkNum].DirtyNum,calculate_arr_positive_num(BlkTable[BlkNum].Dlist,PAGE_NUM_PER_BLK));
            exit(-1);
        }


    }
//    最后做一个错误检测，可以删除,后续的代码调试可以加入
    if(CACHE_CLRU_NUM_ENTRIES!=calculate_arr_positive_num(clru_cache_arr,CACHE_MAX_ENTRIES)||CACHE_DLRU_NUM_ENTRIES!=calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES)){
        printf("error happend in MoveDLRUToCLRU:\n");
        printf("clru-num is %d\t CACHE_CLRU_NUM_ENTRIES %d\n",calculate_arr_positive_num(clru_cache_arr,CACHE_MAX_ENTRIES),CACHE_CLRU_NUM_ENTRIES);
        printf("dlru-num is %d\t CACHE_DLRU_NUM_ENTRIES %d\n",calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES),CACHE_DLRU_NUM_ENTRIES);
        exit(-1);
    }


    return 0;
}


//删除DLRU中的数据项，聚簇回写，涉及底层的物理读写，返回操作时延
double DelLPNInDLRU()
{
    double delay=0.0,temp_delay=0.0;
    int i,j,k,InsertFLag;
    int MinAgeIndex=-1,MinAge=9999999;
    //要删除的LPN(尾部的LPN)
    int LRUVictim,tempBlkNum;
    //聚簇要删除的冷页和要保留的热脏页(dlru_cache_arr的位置索引)
    int VictimCluster[PAGE_NUM_PER_BLK],KeepCluser[PAGE_NUM_PER_BLK];
    //要剔除的脏页总数,保留的页数（转移到CLRU的个数）
    int DelSize,KeepSize,VictimSize;
    int Bound,BoundAgeIndex,BoundAge,BoundAgeLPN;
    //Bound是当前dlru队列的冷热界限，[1,2,3,4,5],如果1,2,3位置为热，3就是Bound
    //之后将DLRU中的LPN根据其age的大小，降序排列，DescendIndex是其在dlru-cache-arr中的位置索引，不是LPN
    //同理BoundAgeIndex也是dlru-cache-arr的位置索引，BoundAge是其age值
    int DescendIndex[CACHE_DLRU_NUM_ENTRIES];
    //初始化，-1的索引是无效值(dlru_cache_arr[-1]是不存在的)
    for ( i = 0; i <CACHE_DLRU_NUM_ENTRIES ; ++i) {
        DescendIndex[i]=-1;
    }
    //初始化VictimCluster和KeepCluser
    for ( i = 0; i <PAGE_NUM_PER_BLK ; ++i) {
        VictimCluster[i]=-1;
        KeepCluser[i]=-1;
    }

    /*************变量初始化结束************************/
    //错误判断
    if(CACHE_DLRU_NUM_ENTRIES<=0){
        printf("error happend in DelLPNInDLRU:CACHE_DLRU_NUM_ENTRIES==0\n");
        exit(1);
    }
    if(CACHE_DLRU_NUM_ENTRIES!=calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES))
    {
        printf("error happend in DelLPNInDLRU:dlru size is error\n");
        printf("CACHE_DLRU_NUM_ENTRIES is %d\t dlru num is %d\n",CACHE_DLRU_NUM_ENTRIES,calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES));
        exit(1);
    }
    /*****************找到DLRU中age降序排列的前a%的分界的BoudAgeIndex**************************/
    for ( i = 0,j=0; i <CACHE_MAX_ENTRIES&&j<CACHE_DLRU_NUM_ENTRIES ; ++i) {
        if(dlru_cache_arr[i]>=0){
            if(NandPage[dlru_cache_arr[i]].cache_age<MinAge){
                MinAge=NandPage[dlru_cache_arr[i]].cache_age;
                //MinAgeIndex就是要删除的LPN在dlru_cache_arr的位置
                MinAgeIndex=i;
            }
            //同时读入有效的LPN到DescendIndex数组中，按照降序排列
            InsertFLag=0;
            for ( k = 0; k <j ; ++k) {
                if(NandPage[dlru_cache_arr[DescendIndex[k]]].cache_age<NandPage[dlru_cache_arr[i]].cache_age){
                    //如果出现插入的新索引的age比该位置的大，则返回这个插入的位置，同时，将这之后的数据都向后移动一位
                    InsertArr(DescendIndex,CACHE_DLRU_NUM_ENTRIES,i,k);
                    InsertFLag=1;
                    break;
                }
            }
            //如果上述的Age都比i的大,插入尾部(j),注意j是当前的空闲的位置
            if(InsertFLag==0){
                DescendIndex[j]=i;
            }
            //错误检测
            if(DescendIndex[j]==-1){
                printf("error happend in DelLPNInDLRU：order DesenedIndex\n");
                printf("Insert Index to DescendIndex exist error:\n");
                exit(-1);
            }
            j++;
        }
    }
    //经过上述的排序，可以得到对应的DescendIndex的降序排序（age）
    Bound=(int)(Threshold*CACHE_DLRU_NUM_ENTRIES+0.5);//四舍五入
    BoundAgeIndex=DescendIndex[Bound];
    BoundAgeLPN=dlru_cache_arr[BoundAgeIndex];
    BoundAge=NandPage[BoundAgeLPN].cache_age;
    /**********定位到尾部（LRU）的LPN,和对应的块索引***************/
    LRUVictim=dlru_cache_arr[MinAgeIndex];
    tempBlkNum=LRUVictim/PAGE_NUM_PER_BLK;
    KeepSize=FindKeepCluser(BlkTable[tempBlkNum].Dlist,BlkTable[tempBlkNum].DirtyNum,KeepCluser,VictimCluster,BoundAge);
    DelSize=BlkTable[tempBlkNum].DirtyNum;
    VictimSize=DelSize-KeepSize;
    /****************计算回写的时间延迟***0:不启用页填充*********************************/
    delay=ClusterWriteToFlash(tempBlkNum,0);
    //注意更改NandPage的状态（回写后的）
    //更改对应的块索引
    MoveDLRUToCLRU(tempBlkNum,KeepCluser,KeepSize,VictimCluster,VictimSize);

    return delay;
}


//将新的数据项加入到缓冲区，涉及到底层的物理读操作，返回时延
double  AddNewToBuffer(int LPN,int operation)
{
    double delay=0.0;
    int free_pos=-1,dlru_index,clru_index;
    int tempBlkNum;

    physical_read++;
    cycle_physical_read++;
    if (operation==0){
        buffer_write_miss++;
        cache_write_num++;
//        找到dlru中的空闲位置
        free_pos=find_free_pos(dlru_cache_arr,CACHE_MAX_ENTRIES);
        if (free_pos==-1){
            printf("error happend in AddNewToBuffer: can not find free pos for LPN %d in dlru-cache-arr\n",LPN);
            exit(1);
        }
        dlru_cache_arr[free_pos]=LPN;
        dlru_index=free_pos;
        CACHE_DLRU_NUM_ENTRIES++;
//        更新相应的NandPage的状态
        NandPage[LPN].cache_update=1;
        NandPage[LPN].cache_status=CACHE_INDLRU;
        NandPage[LPN].cache_age=NandPage[cache_max_index].cache_age+1;
        cache_max_index=LPN;
//        计算读写的时延
        delay=callFsim(LPN*4,4,1);
//        delay=FLASH_READ_DELAY;
        cycle_flash_read_delay+=delay;
//        更新对应的块索引
        tempBlkNum=LPN/PAGE_NUM_PER_BLK;
        BlkTable[tempBlkNum].BlkSize++;
        BlkTable[tempBlkNum].DirtyNum++;
        free_pos=find_free_pos(BlkTable[tempBlkNum].Dlist,PAGE_NUM_PER_BLK);
        if (free_pos==-1){
            printf("error happend in AddNewToBuffer: can not find free pos for dlru_index %d in BlkTable[%d]-Dlist\n",dlru_index,tempBlkNum);
            exit(1);
        }
        BlkTable[tempBlkNum].Dlist[free_pos]=dlru_index;
        /****************错误检测********************/
        if(BlkTable[tempBlkNum].DirtyNum!=calculate_arr_positive_num(BlkTable[tempBlkNum].Dlist,PAGE_NUM_PER_BLK)){
            printf("error happend in  AddNewToBuffer:Dlist size is error\n");
            printf("BlkTable[%d]-DirtyNum is %d\t Dlist-num is %d\n",tempBlkNum,BlkTable[tempBlkNum].DirtyNum,calculate_arr_positive_num(BlkTable[tempBlkNum].Dlist,PAGE_NUM_PER_BLK));
            exit(-1);
        }

    }else{
        buffer_read_miss++;
        cache_read_num++;
//       找到clru中空闲的位置
        free_pos=find_free_pos(clru_cache_arr,CACHE_MAX_ENTRIES);
        if (free_pos==-1){
            printf("error happend in AddNewToBuffer: can not find free pos for LPN %d in clru-cache-arr\n",LPN);
            exit(1);
        }
        clru_cache_arr[free_pos]=LPN;
        clru_index=free_pos;
        CACHE_CLRU_NUM_ENTRIES++;
//        更新相应的NandPage的状态
        NandPage[LPN].cache_update=0;
        NandPage[LPN].cache_status=CACHE_INCLRU;
        NandPage[LPN].cache_age=NandPage[cache_max_index].cache_age+1;
        cache_max_index=LPN;
//        计算读写延迟
//        delay=callFsim(LPN*4,4,1);
        delay=FLASH_READ_DELAY;
        cycle_flash_read_delay+=delay;
//       更新对应的块索引
        tempBlkNum=LPN/PAGE_NUM_PER_BLK;
        BlkTable[tempBlkNum].BlkSize++;
        BlkTable[tempBlkNum].CleanNum++;
        free_pos=find_free_pos(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK);
        if (free_pos==-1){
            printf("error happend in AddNewToBuffer: can not find free pos for clru_index %d in BlkTable[%d]-Clist\n",clru_index,tempBlkNum);
            exit(1);
        }
        BlkTable[tempBlkNum].Clist[free_pos]=clru_index;
        /****************错误检测********************/
        if(BlkTable[tempBlkNum].CleanNum!=calculate_arr_positive_num(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK)){
            printf("error happend in AddNewToBuffer:Clist size is error\n");
            printf("BlkTable[%d]-CleanNum is %d\t Clist-num is %d\n",tempBlkNum,BlkTable[tempBlkNum].CleanNum,calculate_arr_positive_num(BlkTable[tempBlkNum].Clist,PAGE_NUM_PER_BLK));
            exit(-1);
        }

    }
    return  delay;
}


int ZJ_flag=0;

double CacheManage(unsigned int secno,int scount,int operation)
{
    //定义时延变量
    double delay,flash_delay=0,cache_delay=0;
    int bcount,entry_count;
    unsigned int blkno;
    int  cnt=0;
    //页对齐操作
    blkno=secno/4;
    bcount=(secno+scount-1)/4-(secno)/4+1;
    
    //is first call this function
    if(ZJ_flag==0){
		ZJ_flag=1;
		InitVariable();
		init_my_arr();
	}
    
    //重置cache_stat相关状态
    reset_cache_stat();
    cnt=bcount;
    while(cnt>0){
        buffer_cnt++;
        //如果命中缓冲区
        if(NandPage[blkno].cache_status!=My_CACHE_INVALID){
            buffer_hit_cnt++;
            if(NandPage[blkno].cache_status==CACHE_INCLRU){
                //命中CLRU
                HitCLRU(blkno,operation);
            }else{
                //命中DLRU
                HitDLRU(blkno,operation);
            }
        }else{
            //没有命中，缓冲区满？，选择剔除操作,缓冲区没有命中才会涉及flash的读写操作
            if(CACHE_DLRU_NUM_ENTRIES+CACHE_CLRU_NUM_ENTRIES>=CACHE_MAX_ENTRIES){
                //首先错误检测是否CLRU和DLRU的长度是正常
                if(CACHE_CLRU_NUM_ENTRIES!=calculate_arr_positive_num(clru_cache_arr,CACHE_MAX_ENTRIES)||CACHE_DLRU_NUM_ENTRIES!=calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES)){
                    printf("CACHE_CLRU_NUM_ENTRIES is %d\t clru-arr num is %d\n",CACHE_CLRU_NUM_ENTRIES,calculate_arr_positive_num(clru_cache_arr,CACHE_MAX_ENTRIES));
                    printf("CACHE_DLRU_NUM_ENTRIES is %d\t dlru-arr num is %d\n",CACHE_DLRU_NUM_ENTRIES,calculate_arr_positive_num(dlru_cache_arr,CACHE_MAX_ENTRIES));
                    exit(1);
                }
                //根据Tau选择删除CLRU还是DLRU
                if(CACHE_CLRU_NUM_ENTRIES>=Tau){
                        //选择删除CLRU,不涉及读写延迟
                        DelLPNInCLRU();
                }else{
                        //涉及到回写操作，会有flash_delay的延迟
                        flash_delay+=DelLPNInDLRU();
                }
            }
            buffer_miss_cnt++;
            //添加新的数据到缓冲区,涉及到物理读操作，块状态的更新
            flash_delay+=AddNewToBuffer(blkno,operation);
        }

        //最后缓冲区操作完成开始检测是否需要更新Tau的值
        if(T_count==UpdateTauCycle){
            UpdateTau();
        }else {
            T_count++;
        }
        //开始遍历下一个LPN
        cnt--;
        blkno++;
    }//end-while
    cache_delay=calculate_delay_cache();
    delay=cache_delay+flash_delay;


    if(0 == CycleCount % ObserveCycle && CycleCount != 0){
        AveDelay=DelaySum/ObserveCycle;
        DelaySum=0.0;
        printf("the %d Request,the AveDelay is %f\n",CycleCount,AveDelay);
        Curr_buff_miss_cnt=buffer_miss_cnt-Last_buffer_miss_cnt;
        Curr_buffer_cnt=buffer_cnt-Last_buffer_cnt;
        Curr_buffer_read_miss=buffer_read_miss-Last_buffer_read_miss;
        Curr_buffer_write_miss=buffer_write_miss-Last_bufer_write_miss;
        printf("the Curr_buff_cnt is %d\tbuffer miss cnt is %d\n",Curr_buffer_cnt,Curr_buff_miss_cnt);
        printf("the Curr buff read miss is %d\t write miss is %d\n",Curr_buffer_read_miss,Curr_buffer_write_miss);
        Last_buffer_cnt=buffer_cnt;
        Last_buffer_miss_cnt=buffer_miss_cnt;
        Last_buffer_read_miss=buffer_read_miss;
        Last_bufer_write_miss=buffer_write_miss;
        printf("curr clru-size is %d\t dlru-size%d\t Tau is %d\n",CACHE_CLRU_NUM_ENTRIES,CACHE_DLRU_NUM_ENTRIES,Tau);
        printf("..............................\n");
        CycleCount++;
    }else{
        DelaySum+=delay;
        CycleCount++;
    }


    //test point
//    printf("the request blkno is %d\tcache_delay is %f flash_delay is %f\n",(blkno-bcount),cache_delay,flash_delay);
    return delay;
}

