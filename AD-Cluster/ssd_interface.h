/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu)
 *
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * Description: This is a header file for ssd_interface.c.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fast.h"
#include "pagemap.h"
#include "flash.h"
#include "type.h"

#define READ_DELAY        (0.1309/4)
#define WRITE_DELAY       (0.4059/4)
#define ERASE_DELAY       1.5 
#define GC_READ_DELAY  READ_DELAY    // gc read_delay = read delay    
#define GC_WRITE_DELAY WRITE_DELAY  // gc write_delay = write delay 

#define OOB_READ_DELAY    0.0
#define OOB_WRITE_DELAY   0.0

struct ftl_operation * ftl_op;

#define PAGE_READ     0
#define PAGE_WRITE    1
#define OOB_READ      2
#define OOB_WRITE     3
#define BLOCK_ERASE   4
#define GC_PAGE_READ  5
#define GC_PAGE_WRITE 6

void reset_flash_stat();
double calculate_delay_flash();
void initFlash();
void endFlash();
void printWearout();
void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag);
void find_real_max();
void find_real_min();
int find_min_ghost_entry();
void synchronize_disk_flash();
void find_min_cache();
double callFsim(unsigned int secno, int scount, int operation);

int write_count;
int read_count;

int flash_read_num;
int flash_write_num;
int flash_gc_read_num;
int flash_gc_write_num;
int flash_erase_num;
int flash_oob_read_num;
int flash_oob_write_num;

int map_flash_read_num;
int map_flash_write_num;
int map_flash_gc_read_num;
int map_flash_gc_write_num;
int map_flash_erase_num;
int map_flash_oob_read_num;
int map_flash_oob_write_num;

int ftl_type;

extern int total_util_sect_num; 
extern int total_extra_sect_num;

int global_total_blk_num;

int warm_done; 

int total_er_cnt;
int flag_er_cnt;
int block_er_flag[20000];
int block_dead_flag[20000];
int wear_level_flag[20000];
int unique_blk_num; 
int unique_log_blk_num;
int last_unique_log_blk;

int total_extr_blk_num;
int total_init_blk_num;

/****************Zhoujie**********************/
//和缓冲区相关的状态标识
#define My_CACHE_INVALID 0
#define CACHE_INCLRU 1
#define CACHE_INDLRU 2
//定义cache的读写延时
#define CACHE_READ_DELAY 0.0005
#define CACHE_WRITE_DELAY 0.0005
//自己定义flash的读写延迟
#define FLASH_READ_DELAY 0.05
#define FLASH_WRITE_DELAY 0.25
//定义写入放大系数为1.35
#define wAmp 1.35
//cache (read/write count) variable
int cache_read_num;
int cache_write_num;
//表示当前的CLRU和DLRU状态
int CACHE_CLRU_NUM_ENTRIES;
int CACHE_DLRU_NUM_ENTRIES;
int CACHE_LRU_NUM_ENTRIES;


//当前的cache的age最小的索引，也就是LPN，NandPage的下标
int cache_min_index;
int cache_max_index;

//定义算法需要用到的结构体
//my_page_entry表示所用数据页在缓冲区可能的状态
struct my_page_entry{
    int cache_status;
    int cache_age;//在缓冲区滞留的时间，越久age越大
    int cache_update;//缓冲区更新的标志
};
unsigned  int NandPage_Num;//只表示数据页
struct  my_page_entry *NandPage;

struct BlkTable_entry{
    int BlkSize;
    int CleanNum;
    int DirtyNum;
    int Clist[PAGE_NUM_PER_BLK];
    int Dlist[PAGE_NUM_PER_BLK];
};
unsigned int BlkTableNum;//表示对应数据块的个数
struct  BlkTable_entry *BlkTable;

//关于cache的读写统计 在对应的fast.c/dftl.c中的lm_init初始化
int buffer_cnt;
int buffer_hit_cnt;
int buffer_miss_cnt;
int buffer_write_hit;
int buffer_write_miss;
int buffer_read_hit;
int buffer_read_miss;
int physical_write;
int physical_read;
//和算法相关的函数定义
//干净页目标队列长度
//定义热数据页前a%的阈值设定
double Threshold;
int Tau;
//定义Tau占缓冲区最小的比例和最大的比例限制
#define MaxTauRatio 0.9
#define MinTauRatio 0.1
//循环累计的变量
int T_count;
//周期调整Tau值的周期值,一般和buffer的长度成倍数
int UpdateTauCycle;
//周期内命中CLRU和DLRU的情况统计变量
int CDHit_CWH;
int CDHit_CRH;
int CDHit_DWH;
int CDHit_DRH;
//周期内物理读写的次数，用于计算读写平均时延
double cycle_flash_read_delay;
double cycle_flash_write_delay;
int cycle_physical_read;
int cycle_physical_write;
//周期内读写平均时延，之后的代码优化需要用到，替换掉宏定义的写放大和估计的读写延迟
double ave_flash_read_delay;
double ave_flash_write_delay;
//启动页填充的脏页触发值
int TriggerThreshold;

//测试需要用的初始化函数
void InitVariable();//初始化统计变量,还有分配NandPage的内存
//打印输出最后统计的结果
void PrintResultStat();
//注意释放分配的内存
void FreeVariable();
//重置对应的NandPage的状态为初始状态的值
void ResetNandPageStat(int LPN);
void init_my_arr();//初始化自己算法中所需要用到的数组
void reset_cache_stat();
double calculate_delay_cache();
int my_find_cache_min(int *arr,int arrMaxSize);
int my_find_cache_max(int *arr,int arrMaxSize);
int search_table(int *arr,int size,int val);
int calculate_arr_positive_num(int *arr,int size);
int find_free_pos(int *arr,int size);
double CacheManage(unsigned int secno,int scount,int operation);
int myMax(int a,int b);
int myMin(int a,int b);

int UpdateTau();
//将数据插入到(int)arr数组指定的位置pos，pos之后的数据往后挪动一位
int InsertArr(int *arr,int size,int data,int pos);
//根据冷热区分的BoundAge选择部分的热脏页保留,冷脏页剔除
//参数说明，KeepCluster和VictimCluster都是在dlru-cache-arr的位置索引，boudAge
int FindKeepCluser(int Dlist[],int DirtyNum,int *KeepCluster,int *VictimCluster,int BoundAge);
//该函数是调用底层的flash回写函数,将blk块中零散的脏页聚簇连续的回写
//输入的参数BlkNum是要聚簇删除的脏页块号,Alogrithm是否选择启用页填充算法(1:yes,0:no)
double ClusterWriteToFlash(int BlkNum,int Alogrithm);
//该函数完成将dlru-cache-arr的保留热数据页移动到CLRU中,BlkNum是操作的块号,Keeplist是要保留的脏页在dlru-cache-arr中的位置索引
//同时完成了对NandPage的状态修改，块索引的修改
int MoveDLRUToCLRU(int BlkNum,int *Keeplist,int KeepSize,int *VictimCluster,int VictimSize);
//删除CLRU的lru位置的数据项
void DelLPNInCLRU();
//命中DLRU的操作
void HitDLRU(int LPN,int operation);
//命中CLRU的操作
void HitCLRU(int LPN,int operation);

