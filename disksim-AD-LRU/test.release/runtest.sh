# Script writer: Youngjae Kim (youkim@cse.,psu.edu)

#!/bin/sh

PREFIX=./trace/test.file

# echo "Running Pagemap FTL..."
# ../src/disksim pagemap.parv pagemap.outv ascii ${PREFIX} 0 
# grep "IOdriver Response time average:" pagemap.outv
# grep "IOdriver Response time std.dev." pagemap.outv

# echo "Running DFTL..."
# ../src/disksim dftl.parv dftl.outv ascii ${PREFIX} 0
# grep "IOdriver Response time average:" dftl.outv
# grep "IOdriver Response time std.dev." dftl.outv

echo "Running FAST..."
../src/disksim fast.parv fast.outv ascii ${PREFIX} 0
grep "IOdriver Response time average:" fast.outv
grep "IOdriver Response time std.dev." fast.outv

