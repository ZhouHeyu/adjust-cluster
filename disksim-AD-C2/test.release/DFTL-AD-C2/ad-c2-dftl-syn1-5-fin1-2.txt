DFTL:
************************************************************************
Syn1:
bufferStatResult is:
the all buffer req count is 1719998
the buffer miss count is 1051948
the hit rate is 0.388402
the buffer read miss count is 692630
the buffer write miss count is 359318
the buffer read hit count is 439129
the buffer write hit count is 228921
the physical read count is 1051948
the physical write count is 478195

IODRIVER STATISTICS
-------------------

IOdriver Total Requests handled:	399996
IOdriver Requests per second:   	62.424360
IOdriver Completely idle time:  	5717161.104840   	0.892234
IOdriver Response time average: 	2.858279
IOdriver Response time std.dev.:	7.618943
IOdriver Response time maximum:	97.657250

FLASH STATISTICS
------------------------------------------------------------
 Page read (#):  3050962    Page write (#):   961255    Block erase (#):    50588
 GC page read (#):  2276375    GC page write (#):  2276375
------------------------------------------------------------

************************************************************************
Syn2:
bufferStatResult is:
the all buffer req count is 2233069
the buffer miss count is 1101624
the hit rate is 0.506677
the buffer read miss count is 395177
the buffer write miss count is 706447
the buffer read hit count is 408789
the buffer write hit count is 722656
the physical read count is 1101624
the physical write count is 854250

IODRIVER STATISTICS
-------------------

IOdriver Total Requests handled:	449999
IOdriver Requests per second:   	50.038507
IOdriver Completely idle time:  	7884480.250913   	0.876730
IOdriver Response time average: 	3.793280
IOdriver Response time std.dev.:	8.262564
IOdriver Response time maximum:	83.791251

FLASH STATISTICS
------------------------------------------------------------
 Page read (#):  3061705    Page write (#):  1713819    Block erase (#):    91123
 GC page read (#):  4118001    GC page write (#):  4118001
------------------------------------------------------------

************************************************************************
Syn3:
bufferStatResult is:
the all buffer req count is 2223625
the buffer miss count is 1514701
the hit rate is 0.318815
the buffer read miss count is 1207866
the buffer write miss count is 306835
the buffer read hit count is 566695
the buffer write hit count is 142229
the physical read count is 1514701
the physical write count is 405497

IODRIVER STATISTICS
-------------------

IOdriver Total Requests handled:	299999
IOdriver Requests per second:   	15.374196
IOdriver Completely idle time:  	18627083.668790   	0.954591
IOdriver Response time average: 	4.156584
IOdriver Response time std.dev.:	14.282481
IOdriver Response time maximum:	212.246200

FLASH STATISTICS
------------------------------------------------------------
 Page read (#):  3832084    Page write (#):   817770    Block erase (#):    63723
 GC page read (#):  3260494    GC page write (#):  3260494
------------------------------------------------------------

************************************************************************
Syn5:
bufferStatResult is:
the all buffer req count is 1785717
the buffer miss count is 1009845
the hit rate is 0.434488
the buffer read miss count is 709716
the buffer write miss count is 300129
the buffer read hit count is 544791
the buffer write hit count is 231081
the physical read count is 1009845
the physical write count is 415670

IODRIVER STATISTICS
-------------------

IOdriver Total Requests handled:	249999
IOdriver Requests per second:   	50.000451
IOdriver Completely idle time:  	4494049.309607   	0.898822
IOdriver Response time average: 	2.975968
IOdriver Response time std.dev.:	7.494734
IOdriver Response time maximum:	263.194506

FLASH STATISTICS
------------------------------------------------------------
 Page read (#):  2838570    Page write (#):   836464    Block erase (#):    32144
 GC page read (#):  1220757    GC page write (#):  1220757
------------------------------------------------------------

************************************************************************
Fin1:
bufferStatResult is:
the all buffer req count is 13127187
the buffer miss count is 4996199
the hit rate is 0.619401
the buffer read miss count is 832193
the buffer write miss count is 4164006
the buffer read hit count is 1495881
the buffer write hit count is 6635107
the physical read count is 4996199
the physical write count is 4609083

IODRIVER STATISTICS
-------------------

IOdriver Total Requests handled:	5334944
IOdriver Requests per second:   	122.046908
IOdriver Completely idle time:  	40887228.126171   	0.935372
IOdriver Response time average: 	1.286658
IOdriver Response time std.dev.:	2.935705
IOdriver Response time maximum:	736.381100

FLASH STATISTICS
------------------------------------------------------------
 Page read (#): 14347132    Page write (#):  9091237    Block erase (#):   178390
 GC page read (#):  2325773    GC page write (#):  2325773
------------------------------------------------------------

************************************************************************
Fin2:
bufferStatResult is:
the all buffer req count is 7191056
the buffer miss count is 3486274
the hit rate is 0.515193
the buffer read miss count is 3043989
the buffer write miss count is 442285
the buffer read hit count is 2725877
the buffer write hit count is 978905
the physical read count is 3486274
the physical write count is 624971

IODRIVER STATISTICS
-------------------

IOdriver Total Requests handled:	3698863
IOdriver Requests per second:   	90.236658
IOdriver Completely idle time:  	40094759.952198   	0.978143
IOdriver Response time average: 	0.339728
IOdriver Response time std.dev.:	1.202758
IOdriver Response time maximum:	83.711200

FLASH STATISTICS
------------------------------------------------------------
 Page read (#):  7126118    Page write (#):  1257609    Block erase (#):    23438
 GC page read (#):   242457    GC page write (#):   242457
------------------------------------------------------------

