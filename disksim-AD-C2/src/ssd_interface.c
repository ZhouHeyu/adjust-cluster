/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu_
 *   
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * This source plays a role as bridiging disksim and flash simulator. 
 * 
 * Request processing flow: 
 *
 *  1. Request is sent to the simple flash device module. 
 *  2. This interface determines FTL type. Then, it sends the request
 *     to the lower layer according to the FTL type. 
 *  3. It returns total time taken for processing the request in the flash. 
 *
 */

#include "ssd_interface.h"
#include "disksim_global.h"
#include "dftl.h"

extern int merge_switch_num;
extern int merge_partial_num;
extern int merge_full_num;
int old_merge_switch_num = 0;
int old_merge_partial_num = 0;
int old_merge_full_num= 0;
int old_flash_gc_read_num = 0;
int old_flash_erase_num = 0;
int req_count_num = 1;
int cache_hit, rqst_cnt;
int flag1 = 1;
int count = 0;

int page_num_for_2nd_map_table;

#define MAP_REAL_MAX_ENTRIES 6552// real map table size in bytes
#define MAP_GHOST_MAX_ENTRIES 1640// ghost_num is no of entries chk if this is ok

#define CACHE_MAX_ENTRIES 300
int ghost_arr[MAP_GHOST_MAX_ENTRIES];
int real_arr[MAP_REAL_MAX_ENTRIES];
int cache_arr[CACHE_MAX_ENTRIES];

/***********************************************************************
  Variables for statistics    
 ***********************************************************************/
unsigned int cnt_read = 0;
unsigned int cnt_write = 0;
unsigned int cnt_delete = 0;
unsigned int cnt_evict_from_flash = 0;
unsigned int cnt_evict_into_disk = 0;
unsigned int cnt_fetch_miss_from_disk = 0;
unsigned int cnt_fetch_miss_into_flash = 0;

double sum_of_queue_time = 0.0;
double sum_of_service_time = 0.0;
double sum_of_response_time = 0.0;
unsigned int total_num_of_req = 0;

/***********************************************************************
  Mapping table
 ***********************************************************************/
int real_min = -1;
int real_max = 0;

/***********************************************************************
  Cache
 ***********************************************************************/
int cache_min = -1;
int cache_max = 0;

// Interface between disksim & fsim 

void reset_flash_stat()
{
  flash_read_num = 0;
  flash_write_num = 0;
  flash_gc_read_num = 0;
  flash_gc_write_num = 0; 
  flash_erase_num = 0;
  flash_oob_read_num = 0;
  flash_oob_write_num = 0; 
}

FILE *fp_flash_stat;
FILE *fp_gc;
FILE *fp_gc_timeseries;
double gc_di =0 ,gc_ti=0;


double calculate_delay_flash()
{
  double delay;
  double read_delay, write_delay;
  double erase_delay;
  double gc_read_delay, gc_write_delay;
  double oob_write_delay, oob_read_delay;

  oob_read_delay  = (double)OOB_READ_DELAY  * flash_oob_read_num;
  oob_write_delay = (double)OOB_WRITE_DELAY * flash_oob_write_num;

  read_delay     = (double)READ_DELAY  * flash_read_num; 
  write_delay    = (double)WRITE_DELAY * flash_write_num; 
  erase_delay    = (double)ERASE_DELAY * flash_erase_num; 

  gc_read_delay  = (double)GC_READ_DELAY  * flash_gc_read_num; 
  gc_write_delay = (double)GC_WRITE_DELAY * flash_gc_write_num; 


  delay = read_delay + write_delay + erase_delay + gc_read_delay + gc_write_delay + 
    oob_read_delay + oob_write_delay;

  if( flash_gc_read_num > 0 || flash_gc_write_num > 0 || flash_erase_num > 0 ) {
    gc_ti += delay;
  }
  else {
    gc_di += delay;
  }

  if(warm_done == 1){
    fprintf(fp_gc_timeseries, "%d\t%d\t%d\t%d\t%d\t%d\n", 
      req_count_num, merge_switch_num - old_merge_switch_num, 
      merge_partial_num - old_merge_partial_num, 
      merge_full_num - old_merge_full_num, 
      flash_gc_read_num,
      flash_erase_num);

    old_merge_switch_num = merge_switch_num;
    old_merge_partial_num = merge_partial_num;
    old_merge_full_num = merge_full_num;
    req_count_num++;
  }

  reset_flash_stat();

  return delay;
}


/***********************************************************************
  Initialize Flash Drive 
  ***********************************************************************/

void initFlash()
{
  blk_t total_blk_num;
  blk_t total_util_blk_num;
  blk_t total_extr_blk_num;

  // total number of sectors    
  total_util_sect_num  = flash_numblocks;
  total_extra_sect_num = flash_extrblocks;
  total_sect_num = total_util_sect_num + total_extra_sect_num; 

  // total number of blocks 
  total_blk_num      = total_sect_num / SECT_NUM_PER_BLK;     // total block number
  total_util_blk_num = total_util_sect_num / SECT_NUM_PER_BLK;    // total unique block number

  global_total_blk_num = total_util_blk_num;

  total_extr_blk_num = total_blk_num - total_util_blk_num;        // total extra block number

  ASSERT(total_extr_blk_num != 0);

  if (nand_init(total_blk_num, 3) < 0) {
    EXIT(-4); 
  }

  switch(ftl_type){

    // pagemap
    case 1: ftl_op = pm_setup(); break;
    // blockmap
    //case 2: ftl_op = bm_setup(); break;
    // o-pagemap 
    case 3: ftl_op = opm_setup(); break;
    // fast
    case 4: ftl_op = lm_setup(); break;

    default: break;
  }

  ftl_op->init(total_util_blk_num, total_extr_blk_num);

  nand_stat_reset();
}

void printWearout()
{
  int i;
  FILE *fp = fopen("wearout", "w");
  
  for(i = 0; i<nand_blk_num; i++)
  {
    fprintf(fp, "%d %d\n", i, nand_blk[i].state.ec); 
  }

  fclose(fp);
}


void endFlash()
{
  nand_stat_print(outputfile);
  ftl_op->end;
  nand_end();
}  

/***********************************************************************
  Send request (lsn, sector_cnt, operation flag)
  ***********************************************************************/

void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag)
{
	int size;
	//size_t (*op_func)(sect_t lsn, size_t size);
	size_t (*op_func)(sect_t lsn, size_t size, int mapdir_flag);

        if((start_blk_no + block_cnt) >= total_util_sect_num){
          printf("start_blk_no: %d, block_cnt: %d, total_util_sect_num: %d\n", 
              start_blk_no, block_cnt, total_util_sect_num);
          exit(0);
        }

	switch(operation){

	//write
	case 0:

		op_func = ftl_op->write;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;
	//read
	case 1:


		op_func = ftl_op->read;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;

	default: 
		break;
	}
}

void find_real_max()
{
  int i; 

  for(i=0;i < MAP_REAL_MAX_ENTRIES; i++) {
      if(opagemap[real_arr[i]].map_age > opagemap[real_max].map_age) {
          real_max = real_arr[i];
      }
  }
}

void find_real_min()
{
  
  int i,index; 
  int temp = 99999999;

  for(i=0; i < MAP_REAL_MAX_ENTRIES; i++) {
        if(opagemap[real_arr[i]].map_age <= temp) {
            real_min = real_arr[i];
            temp = opagemap[real_arr[i]].map_age;
            index = i;
        }
  }    
}

int find_min_ghost_entry()
{
  int i; 

  int ghost_min = 0;
  int temp = 99999999; 

  for(i=0; i < MAP_GHOST_MAX_ENTRIES; i++) {
    if( opagemap[ghost_arr[i]].map_age <= temp) {
      ghost_min = ghost_arr[i];
      temp = opagemap[ghost_arr[i]].map_age;
    }
  }
  return ghost_min;
}

void init_arr()
{

  int i;
  for( i = 0; i < MAP_REAL_MAX_ENTRIES; i++) {
      real_arr[i] = -1;
  }
  for( i = 0; i < MAP_GHOST_MAX_ENTRIES; i++) {
      ghost_arr[i] = -1;
  }
  for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
      cache_arr[i] = -1;
  }

}

int search_table(int *arr, int size, int val) 
{
    int i;
    for(i =0 ; i < size; i++) {
        if(arr[i] == val) {
            return i;
        }
    }

    printf("shouldnt come here for search_table()=%d,%d",val,size);
    for( i = 0; i < size; i++) {
      if(arr[i] != -1) {
        printf("arr[%d]=%d ",i,arr[i]);
      }
    }
    exit(1);
    return -1;
}

int find_free_pos( int *arr, int size)
{
    int i;
    for(i = 0 ; i < size; i++) {
        if(arr[i] == -1) {
            return i;
        }
    } 
    printf("shouldnt come here for find_free_pos()");
    exit(1);
    return -1;
}

void find_min_cache()
{
  int i; 
  int temp = 999999;

  for(i=0; i < CACHE_MAX_ENTRIES ;i++) {
      if(opagemap[cache_arr[i]].cache_age <= temp ) {
          cache_min = cache_arr[i];
          temp = opagemap[cache_arr[i]].cache_age;
      }
  }
}

int youkim_flag1=0;

double callFsim(unsigned int secno, int scount, int operation)
{
  double delay; 
  int bcount;
  unsigned int blkno; // pageno for page based FTL
  int cnt,z; int min_ghost;

  int pos=-1,pos_real=-1,pos_ghost=-1;

  if(ftl_type == 1){ }

  if(ftl_type == 3) {
      page_num_for_2nd_map_table = (opagemap_num / MAP_ENTRIES_PER_PAGE);
    
      if(youkim_flag1 == 0 ) {
        youkim_flag1 = 1;
        init_arr();
      }

      if((opagemap_num % MAP_ENTRIES_PER_PAGE) != 0){
        page_num_for_2nd_map_table++;
      }
  }
      
  // page based FTL 
  if(ftl_type == 1 ) { 
    blkno = secno / 4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // block based FTL 
  else if(ftl_type == 2){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }
  // o-pagemap scheme
  else if(ftl_type == 3 ) { 
    blkno = secno / 4;
    blkno += page_num_for_2nd_map_table;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // FAST scheme
  else if(ftl_type == 4){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }

  cnt = bcount;

  switch(operation)
  {
    //write/read
    case 0:
    case 1:

    while(cnt > 0)
    {
          cnt--;

        // page based FTL
        if(ftl_type == 1){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // blck based FTL
        else if(ftl_type == 2){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // opagemap ftl scheme
        else if(ftl_type == 3)
        {

          /************************************************
            primary map table 
          *************************************************/
          //1. pagemap in SRAM 

            rqst_cnt++;
          if((opagemap[blkno].map_status == MAP_REAL) || (opagemap[blkno].map_status == MAP_GHOST))
          {
            cache_hit++;

            opagemap[blkno].map_age++;

            if(opagemap[blkno].map_status == MAP_GHOST){

              if ( real_min == -1 ) {
                real_min = 0;
                find_real_min();
              }    
              if(opagemap[real_min].map_age <= opagemap[blkno].map_age) 
              {
                find_real_min();  // probably the blkno is the new real_min alwaz
                opagemap[blkno].map_status = MAP_REAL;
                opagemap[real_min].map_status = MAP_GHOST;

                pos_ghost = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,blkno);
                ghost_arr[pos_ghost] = -1;
                
                pos_real = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos_real] = -1;

                real_arr[pos_real]   = blkno; 
                ghost_arr[pos_ghost] = real_min; 
              }
            }
            else if(opagemap[blkno].map_status == MAP_REAL) 
            {
              if ( real_max == -1 ) {
                real_max = 0;
                find_real_max();
                printf("Never happend\n");
              }

              if(opagemap[real_max].map_age <= opagemap[blkno].map_age)
              {
                real_max = blkno;
              }  
            }
            else {
              printf("forbidden/shouldnt happen real =%d , ghost =%d\n",MAP_REAL,MAP_GHOST);
            }
          }

          //2. opagemap not in SRAM 
          else
          {
            //if map table in SRAM is full
            if((MAP_REAL_MAX_ENTRIES - MAP_REAL_NUM_ENTRIES) == 0)
            {
              if((MAP_GHOST_MAX_ENTRIES - MAP_GHOST_NUM_ENTRIES) == 0)
              { //evict one entry from ghost cache to DRAM or Disk, delay = DRAM or disk write, 1 oob write for invalidation 
                min_ghost = find_min_ghost_entry();
                  evict++;

                if(opagemap[min_ghost].update == 1) {
                  update_reqd++;
                  opagemap[min_ghost].update = 0;
                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table then update it

                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 0, 2);   // write into 2nd mapping table 
                } 
                opagemap[min_ghost].map_status = MAP_INVALID;

                MAP_GHOST_NUM_ENTRIES--;

                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                MAP_GHOST_NUM_ENTRIES++;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;

                pos = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,min_ghost);
                ghost_arr[pos]=-1;

                
                ghost_arr[pos]= real_min;
                
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;
              }
              else{
                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;
               
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;

                pos = find_free_pos(ghost_arr,MAP_GHOST_MAX_ENTRIES);
                ghost_arr[pos]=real_min;
                
                MAP_GHOST_NUM_ENTRIES++;
              }
            }

            flash_hit++;
            send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table

            opagemap[blkno].map_status = MAP_REAL;

            opagemap[blkno].map_age = opagemap[real_max].map_age + 1;
            real_max = blkno;
            MAP_REAL_NUM_ENTRIES++;
            
            pos = find_free_pos(real_arr,MAP_REAL_MAX_ENTRIES);
            real_arr[pos] = blkno;
          }

         //comment out the next line when using cache
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // FAST scheme  
        else if(ftl_type == 4){ 

          if(operation == 0){
            write_count++;
          }
          else read_count++;

          send_flash_request(blkno*4, 4, operation, 1); //cache_min is a page for page baseed FTL
          blkno++;
        }
    }
    break;
  }

  delay = calculate_delay_flash();

  return delay;
}


/**********************ZhouJie***************************/


//创建双向链表,返回的是头节点
pNode CreateList()
{
    //分配初始内存
    pNode  pHead=(pNode)malloc(sizeof(Node));
    if(NULL==pHead){
        printf("malloc for pHead failed!\n");
        exit(-1);
    }
    pHead->LPN=-1;
    pHead->isD=-1;
    pHead->Pre=pHead;
    pHead->Next=pHead;

    return pHead;
}

pBNode CreateBIndex()
{
    int i;
    pBNode pHead=(pBNode)malloc(sizeof(BNode));
    if (NULL==pHead){
        printf("malloc for pBHead failed!\n");
        exit(-1);
    }
    pHead->BlkNum=-1;
    pHead->DirtyNum=0;
    pHead->CleanNum=0;
    pHead->Size=0;
    pHead->Pre=pHead;
    pHead->Next=pHead;
    for ( i = 0; i <PAGE_PRE_BLK ; ++i) {
        pHead->CleanList[i]=-1;
        pHead->DirtyList[i]=-1;
    }
    return pHead;
}

//删除整个链表，释放内存

void FreeList(pNode pHead)
{
    pNode ps,pt=pHead->Next;
    while(pt!=pHead){
        ps=pt;
        pt=pt->Next;
        free(ps);
    }
    free(pHead);
}

void FreeBList(pBNode pHead)
{
    pBNode ps,pt=pHead->Next;
    while(pt!=pHead){
        ps=pt;
        pt=pt->Next;
        free(ps);
    }
    free(pHead);
}

//初始化相应的配置参数
void InitVariable()
{
    buf_size=1024*4;
    //后续的统计变量
    buffer_cnt=0;
    buffer_hit=0;
    buffer_miss_cnt=0;
    buffer_read_hit=0;
    buffer_read_miss=0;
    buffer_write_hit=0;
    buffer_write_miss=0;
    //物理读写次数
    physical_read=0;
    physical_write=0;
    //设定当前的clru和dlru的长度
    CLRU_Length=0;
    DLRU_Length=0;
    BlkTable_Size=0;
    //设定当前的冷热比例的阈值
    Threshold=0.5;
    //初始化的tau
    Tau=buf_size/2;
    MinTauRatio=0.1;
    MaxTauRatio=0.9;
//    和周期更新Tau的数据相关
    CDHit_CWH=0;
    CDHit_CRH=0;
    CDHit_DRH=0;
    CDHit_DWH=0;
//    写入放大系数
    wAmp=1.35;
    UpdateTauCycle=buf_size*1;
    T_count=0;
    cycle_physical_write=0;
    cycle_physical_read=0;
    cycle_flash_write_delay=0.0;
    cycle_flash_read_delay=0.0;

}


//输出对应的统计结果
void PrintResultStat()
{
    //输出和缓冲区统计相关的信息
    printf("****************************************\n");
    printf("the all buffer req count is %d\n",buffer_cnt);
    printf("the buffer miss count is %d\n",buffer_miss_cnt);
    printf("the buffer hit count is %d\n",buffer_hit);
    printf("the hit rate is %f\n",((double)buffer_hit)/buffer_cnt);
    printf("the buffer read miss count is %d\n",buffer_read_miss);
    printf("the buffer write miss count is %d\n",buffer_write_miss);
    printf("the buffer read hit count is %d\n",buffer_read_hit);
    printf("the buffer write hit count is %d\n",buffer_write_hit);
    printf("the physical read count is %d\n",physical_read);
    printf("the physical write count is %d\n",physical_write);
}

void resetCacheStat()
{
    cache_read_num=0;
    cache_write_num=0;
}

double ComputeCacheDelay()
{
    double delay=0.0;
    double cache_read_delay=0.0;
    double cache_write_delay=0.0;
    cache_read_delay=(double)cache_read_num*(double)CACHE_READ_DELAY;
    cache_write_delay=(double)cache_write_num*(double)CACHE_WRITE_DELAY;
    delay=cache_read_delay+cache_write_delay;
    resetCacheStat();
    return delay;
}

//判断链表是否为空
int IsEmptyList(pNode pHead)
{
    pNode pt=pHead->Next;
    if(pt==pHead)
    {
        return 1;
    }else
    {
        return 0;
    }
}

//返回链表的长度
int GetListLength(pNode pHead)
{
    int length=0;
    pNode pt=pHead->Next;
    while (pt !=pHead)
    {
        length++;
        pt=pt->Next;
    }
    return length;
}

//返回链表的长度
int GetBIndexLength(pBNode pHead)
{
    int length=0;
    pBNode pt=pHead->Next;
    while (pt !=pHead)
    {
        length++;
        pt=pt->Next;
    }
    return length;
}

//输出打印链表
void PrintList(pNode pHead)
{
    pNode pt=pHead->Next;
    printf("the list is follow:\n");
    while(pt!=pHead)
    {
        printf("the LPN is %d\t",pt->LPN);
        printf("the isD is %d\n",pt->isD);
        pt=pt->Next;
    }
}

//从链表中插入新的节点,这里之后有读写操作的operation
int InsertEleList(pNode pHead,int pos,int LPN,int operation)
{
    pNode pt=NULL,p_new=NULL;
    pt=pHead->Next;
    if(pos>0&&pos<GetListLength(pHead)+2)
    {
        p_new=(pNode)malloc(sizeof(Node));
        if(NULL==p_new)
        {
            printf("malloc for New Node!\n");
            exit(-1);
        }

        while(1)
        {
            pos--;
            if(0==pos)
                break;
            pt=pt->Next;
        }
        //新建节点
        p_new->LPN=LPN;
        if(operation==0)
            p_new->isD=1;
        else
            p_new->isD=0;
        //这里的双链表是首位相接的，也就是head前面就是最后一个节点
        //插入pt的前面
        p_new->Pre=pt->Pre;
        p_new->Next=pt;
        //衔接
        pt->Pre->Next=p_new;
        pt->Pre=p_new;
        return 1;
    }else {
        return  0;
    }
}

//向链表中删除节点
int DeleteEleList(pNode pHead,int pos)
{
    pNode pt=pHead,ps=NULL;
    //pos=0就是pHead
    if(pos>0&&pos<GetListLength(pHead)+1){
        while(1)
        {
            pos--;
            if(pos==0)
                break;
            pt=pt->Next;
        }
        //此时的pt是pos的前一个节点
        //ps才是要删除的节点位置
        ps=pt->Next;
        pt->Next=ps->Next;
        ps->Next->Pre=pt;
        //释放ps指向的节点
        free(ps);
        return 1;
    }else{
        printf("delete pos %d is error\n",pos);
        return 0;
    }

}

//从链表中找到特定的LPN值，并返回节点的指针位置,如果不存在返回NULL
pNode FindLPNinList(pNode pHead,int LPN)
{
    pNode ps=NULL,pt=pHead->Next;
    int count=0;
    while(pt!=pHead)
    {
        count++;
        if(pt->LPN==LPN){
            ps=pt;
            break;
        }
        pt=pt->Next;
    }
    //调试输出语句遍历循环了多少次
//    printf("the while count is %d\n",count);
    return ps;
}

int myMax(int a,int b)
{
    if(a>b)
        return a;
    else
        return b;
}

int myMin(int a,int b)
{
    if(a>b)
        return b;
    else
        return a;
}

//该函数完成对目标Tau的自适应的更新
int UpdateTau()
{
    int D_Tau,TempTau,MinTau,MaxTau;//表示脏队列的目标长度
    //四射五入
    double B_CLRU,B_DLRU;//表示各自队列的单位收益
    MinTau=(int)(MinTauRatio*buf_size+0.5);
    MaxTau=(int)(MaxTauRatio*buf_size+0.5);

    D_Tau=buf_size-Tau;
    
 /*   
    //未引入从底层得到flash读写延迟的代码
    B_CLRU=(CDHit_CWH*FLASH_WRITE_DELAY*wAmp+CDHit_CRH*FLASH_READ_DELAY)/Tau;
    B_DLRU=(CDHit_DWH*FLASH_WRITE_DELAY*wAmp+CDHit_DRH*FLASH_READ_DELAY)/D_Tau;
*/

    //如果从底层得到周期的读写时延
    ave_flash_read_delay=cycle_flash_read_delay/cycle_physical_read;
    ave_flash_write_delay=cycle_flash_write_delay/cycle_physical_write;
    B_CLRU=(CDHit_CRH*ave_flash_read_delay+CDHit_CWH*ave_flash_write_delay)/Tau;
    B_DLRU=(CDHit_DRH*ave_flash_read_delay+CDHit_CWH*ave_flash_write_delay)/D_Tau;


    //四舍五入
    TempTau=(int)((B_CLRU/(B_CLRU+B_DLRU)*buf_size)+0.5);
    TempTau=myMax(MinTau,TempTau);
    TempTau=myMin(MaxTau,TempTau);
    Tau=TempTau;

    /*
    //适当的嵌入代码调试的测试语句
    printf("----------------------------------------------------------\n");
    printf("The Cycle Stat is:\n");
    printf("CLRU write Hit is %d\t Read Hit is %d\n",CDHit_CWH,CDHit_CRH);
    printf("DLRU write Hit is %d\t Read Hit is %d\n",CDHit_DWH,CDHit_DRH);
    printf("sum write delay is %f\t read delay is %f\n",cycle_flash_write_delay,cycle_flash_read_delay);
    printf("ave write delay is %f\t read delay is %f\n",ave_flash_write_delay,ave_flash_read_delay);
    printf("cycle physical write is %d\t physical read is %d\n",cycle_physical_write,cycle_physical_read);
    printf("-----------------------------------------------------------\n");
    */

    //重置相应的周期统计变量
    CDHit_CWH=0;
    CDHit_CRH=0;
    CDHit_DRH=0;
    CDHit_DWH=0;
    T_count=1;
    cycle_physical_write=0;
    cycle_physical_read=0;
    cycle_flash_write_delay=0.0;
    cycle_flash_read_delay=0.0;
    //返回更新后的Tau值，以备输出测试
    return Tau;
}

//找到块索引链表中的位置
pBNode FindBlkTable(pBNode Head,int BlkNum)
{
    pBNode ps=NULL,pt=Head->Next;
    int count=0;
    while(pt!=Head){
        if(pt->BlkNum==BlkNum){
            ps=pt;
            break;
        }
        count++;
        pt=pt->Next;
    }
    return ps;
}


//命中CLRU的操作,存在命中的请求类型
int HitCLRU(pNode CHead,pNode DHead,pNode pHit,int operation)
{
    pNode pt;
    pBNode pblk;
    int tempBlk,tempLPN;
    int victim,free_pos;
    tempLPN=pHit->LPN;
    tempBlk=tempLPN/PAGE_PRE_BLK;
//如果是写请求命中，则需要将对应的请求移动到DLRU的队列中
    if(0!=operation){
//      读命中
        buffer_read_hit++;
        cache_read_num++;
        CDHit_CRH++;
//       将命中的请求移动到CLRU的头部
        //从原先位置断开链接
        pt=pHit->Pre;
        pt->Next=pHit->Next;
        pHit->Next->Pre=pt;
//        插入位置
        pHit->Next=CHead->Next;
        pHit->Pre=CHead;
        CHead->Next->Pre=pHit;
        CHead->Next=pHit;
    }else{
//      写命中
        pHit->isD=1;
        buffer_write_hit++;
        cache_write_num++;
        CDHit_CWH++;
//       命中的数据移动到DLRU
//        从原来的位置断开
        pt=pHit->Pre;
        pt->Next=pHit->Next;
        pHit->Next->Pre=pt;
//        嵌入到DLRU的头部
        pHit->Pre=DHead;
        pHit->Next=DHead->Next;
        DHead->Next->Pre=pHit;
        DHead->Next=pHit;
        CLRU_Length--;
        DLRU_Length++;
//        更新对应的块索引
        pblk=FindBlkTable(BHead,tempBlk);
        if(pblk==NULL){
            printf("error happend in HitCLRU:\n");
            printf("can not find tempBlk %d in Blk-list\n",tempBlk);
            exit(-1);
        }
//       将对应的块索引中的标识改掉
        victim=search_table(pblk->CleanList,PAGE_PRE_BLK,tempLPN);
        if(victim==-1){
            printf("error happend in HitCLRU:\n");
            printf("can not find tempLPN%d in CleanList\n",tempLPN);
            exit(-1);
        }
        pblk->CleanList[victim]=-1;
        pblk->CleanNum--;
        free_pos=find_free_pos(pblk->DirtyList,PAGE_PRE_BLK);
        if(free_pos==-1){
            printf("error happend in HitCLRU:\n");
            printf("can not find free-pos for tempLPN%d in DirtyList\n",tempLPN);
            exit(-1);
        }
        pblk->DirtyList[free_pos]=tempLPN;
        pblk->DirtyNum++;
    }
    return 0;
}

//命中DLRU的操作
int HitDLRU(pNode DHead,pNode pHit,int operation)
{
    pNode pt=NULL;
    if(operation==0){
        buffer_write_hit++;
        cache_write_num++;
        CDHit_DWH++;
    } else{
        buffer_read_hit++;
        cache_read_num++;
        CDHit_DRH++;
    }
//    Move to MRU
    pt=pHit->Pre;
    //Del
    pt->Next=pHit->Next;
    pHit->Next->Pre=pt;
    //MRU
    pHit->Next=DHead->Next;
    pHit->Pre=DHead;
    DHead->Next->Pre=pHit;
    DHead->Next=pHit;
    return 0;
}

//删除CLRU中的数据，返回的是删除的LPN号
int DelCLRU(pNode CHead)
{
    pNode pt;
    pBNode pblk;
    int DelLPN=-1,tempBlk;
    int victim;

    pt=CHead->Pre;
    DelLPN=pt->LPN;
    tempBlk=DelLPN/PAGE_PRE_BLK;
    //错误判断
    if(pt==CHead){
        printf("error happend in DelCLRU\n");
        printf("CLRU list is empty!\n");
        exit(-1);
    }
//    删除尾部的数据
    pt->Pre->Next=CHead;
    CHead->Pre=pt->Pre;
//    释放该尾部的节点
    free(pt);
    CLRU_Length--;
//    错误判断
    if(CLRU_Length!=GetListLength(CHead)){
        printf("error happend in DelCLRU\n");
        printf("CLRU list size is error!\n");
        printf("CLRU list-size is %d\t,CLRU_Length is %d\n",GetListLength(CHead),CLRU_Length);
        exit(-1);
    }
//    更新对应的块索引
    pblk=FindBlkTable(BHead,tempBlk);
    if(pblk==NULL){
        printf("error happend in DelCLRU\n");
        printf("can not find tempBlk %d in Blk-list\n",tempBlk);
        exit(-1);
    }
    victim=search_table(pblk->CleanList,PAGE_PRE_BLK,DelLPN);
    if(victim==-1){
        printf("error happend in DelCLRU\n");
        printf("can not find DelLPN%d in Clean-list\n",DelLPN);
        exit(-1);
    }
    pblk->CleanList[victim]=-1;
    pblk->Size--;
    pblk->CleanNum--;

//    如果当前的块大小为0，说明不需要该块索引，直接删除即可
    if(pblk->Size==0){
        pblk->Pre->Next=pblk->Next;
        pblk->Next->Pre=pblk->Pre;
        //释放该节点
        free(pblk);
        BlkTable_Size--;
    }
//    错误调试
    if(BlkTable_Size!=GetBIndexLength(BHead)){
        printf("error happend in DelCLRU\n");
        printf("BlkTable Size is %d\t BIndex-list Size is %d\n",BlkTable_Size,GetBIndexLength(BHead));
        exit(-1);
    }

    return DelLPN;
}

//非聚簇删除DLRU的数据，设计回写操作，有时延
double DelOneDLRU(pNode DHead)
{
    double delay=0.0;
    pNode pt;
    pBNode pblk;
    int DelLPN=-1,tempBlk;
    int victim;

    pt=DHead->Pre;
    DelLPN=pt->LPN;
    tempBlk=DelLPN/PAGE_PRE_BLK;
    //错误判断
    if(pt==DHead){
        printf("error happend in DelOneDLRU\n");
        printf("DLRU list is empty!\n");
        exit(-1);
    }
//    删除尾部的数据
    pt->Pre->Next=DHead;
    DHead->Pre=pt->Pre;
//    释放该尾部的节点
    free(pt);
    DLRU_Length--;
//    错误判断
    if(DLRU_Length!=GetListLength(DHead)){
        printf("error happend in DelOneDLRU\n");
        printf("CLRU list size is error!\n");
        printf("DLRU list-size is %d\t,DLRU_Length is %d\n",GetListLength(DHead),DLRU_Length);
        exit(-1);
    }
//    更新对应的块索引
    pblk=FindBlkTable(BHead,tempBlk);
    if(pblk==NULL){
        printf("error happend in DelOneDLRU\n");
        printf("can not find tempBlk %d in Blk-list\n",tempBlk);
        exit(-1);
    }
    victim=search_table(pblk->DirtyList,PAGE_PRE_BLK,DelLPN);
    if(victim==-1){
        printf("error happend in DelOneDLRU\n");
        printf("can not find DelLPN%d in Dirty-list\n",DelLPN);
        exit(-1);
    }
    pblk->DirtyList[victim]=-1;
    pblk->Size--;
    pblk->DirtyNum--;

//    如果当前的块大小为0，说明不需要该块索引，直接删除即可
    if(pblk->Size==0){
        pblk->Pre->Next=pblk->Next;
        pblk->Next->Pre=pblk->Pre;
        //释放该节点
        free(pblk);
        BlkTable_Size--;
    }
//    错误调试
    if(BlkTable_Size!=GetBIndexLength(BHead)){
        printf("error happend in DelOneDLRU\n");
        printf("BlkTable Size is %d\t BIndex-list Size is %d\n",BlkTable_Size,GetBIndexLength(BHead));
        exit(-1);
    }




//    回写的时延
    physical_write++;
    cycle_physical_write++;
	delay+=callFsim(DelLPN*4,4,0);
    //delay=FLASH_WRITE_DELAY;
    cycle_flash_write_delay+=delay;

    return delay;
}


//采用聚簇删除
double DelClusterInDLRU(pNode DHead)
{
    double delay=0.0;
    return delay;
}

//将新的请求加入到缓冲区,涉及到物理读操作，有延迟
double AddNewToBuffer(pNode CHead,pNode DHead,int LPN,int operation)
{
    double delay=0.0;
    pNode pt;
    pBNode pblk,ptblk;
    int tempBlk;
    int i,free_pos;
    tempBlk=LPN/PAGE_PRE_BLK;
    //    错误检测
    if(BlkTable_Size!=GetBIndexLength(BHead)){
        printf("error happend in AddNewToBuffer 1\n");
        printf("BlkTable Size is %d\t BIndex-list Size is %d\n",BlkTable_Size,GetBIndexLength(BHead));
        exit(-1);
    }

    pblk=FindBlkTable(BHead,tempBlk);

    if(pblk==NULL){
//       第一次加载该数据块没，添加其到链表中
        pblk=(pBNode)malloc(sizeof(BNode));
        if (NULL==pblk){
            printf("error happend in AddNewToBuffer\n");
            printf("malloc for pblk failed!\n");
            exit(-1);
        }
        pblk->BlkNum=tempBlk;
        pblk->DirtyNum=0;
        pblk->CleanNum=0;
        pblk->Size=0;
        for ( i = 0; i <PAGE_PRE_BLK ; ++i) {
            pblk->CleanList[i]=-1;
            pblk->DirtyList[i]=-1;
        }
//        将新的节点链接头部
        ptblk=BHead->Next;
//        插入关联
        pblk->Pre=BHead;
        pblk->Next=ptblk;
//        链接
        ptblk->Pre=pblk;
        BHead->Next=pblk;
        BlkTable_Size++;
    }

//   为新的节点分配内存
    pt=(pNode)malloc(sizeof(Node));
    if(pt==NULL){
        printf("error happend in AddNewToBuffer\n");
        printf("malloc for pNode failed!\n");
        exit(-1);
    }
    pt->LPN=LPN;
    physical_read++;
    cycle_physical_read++;
	delay=callFsim(LPN*4,4,1);
    //delay=FLASH_READ_DELAY;
    cycle_flash_read_delay+=delay;

//   根据不同的请求将数据页加载到对应的队列中
    if(operation==0){
        buffer_write_miss++;
        cache_write_num++;
        pt->isD=1;
//        Move to MRU
        pt->Pre=DHead;
        pt->Next=DHead->Next;
        DHead->Next->Pre=pt;
        DHead->Next=pt;
        DLRU_Length++;
//        更新对应的块索引
        free_pos=find_free_pos(pblk->DirtyList,PAGE_PRE_BLK);
        if(free_pos==-1){
            printf("error happend in AddNewToBuffer:\n");
            printf("can not find free-pos for tempLPN%d in DirtyList\n",LPN);
            exit(-1);
        }
        pblk->DirtyList[free_pos]=LPN;
        pblk->Size++;
        pblk->DirtyNum++;

    }else{
        buffer_read_miss++;
        cache_read_num++;
        pt->isD=0;
//        Move To CLRU MRU
        pt->Pre=CHead;
        pt->Next=CHead->Next;
        CHead->Next->Pre=pt;
        CHead->Next=pt;
        CLRU_Length++;
//        更新对应的块索引
        free_pos=find_free_pos(pblk->CleanList,PAGE_PRE_BLK);
        if(free_pos==-1){
            printf("error happend in AddNewToBuffer:\n");
            printf("can not find free-pos for tempLPN%d in CleanList\n",LPN);
            exit(-1);
        }
        pblk->CleanList[free_pos]=LPN;
        pblk->Size++;
        pblk->CleanNum++;
    }

//    错误检测
    if(CLRU_Length!=GetListLength(CHead)||DLRU_Length!=GetListLength(DHead)){
        printf("error happend in AddNewToBuffer:\n");
        printf("CLRU_Length is %d\t Clru-list size is %d\n",CLRU_Length,GetListLength(CHead));
        printf("DLRU_Length is %d\t Clru-list size is %d\n",DLRU_Length,GetListLength(DHead));
        exit(-1);
    }
//    错误检测
    if(BlkTable_Size!=GetBIndexLength(BHead)){
        printf("error happend in AddNewToBuffer 2\n");
        printf("BlkTable Size is %d\t BIndex-list Size is %d\n",BlkTable_Size,GetBIndexLength(BHead));
        exit(-1);
    }

    return delay;
}


int ZJ_flag=0;
//观测输出的变量
int ObserveCycle=100;
int CycleCount=0;
double DelaySum=0.0;
double AveDelay=0.0;
//last
int Last_buffer_read_miss=0;
int Last_bufer_write_miss=0;
int Last_buffer_cnt=0;
int Last_buffer_miss_cnt=0;
//curr
int Curr_buffer_read_miss;
int Curr_buffer_write_miss;
int Curr_buffer_cnt;
int Curr_buff_miss_cnt;



double CacheManage(int secno,int scount,int operation )
{
    double delay=0.0;
    double flash_delay=0.0, cache_delay=0.0;
    pNode pt=NULL;
    int tempBlk;
    int flag=0;//flag=0表示未命中缓冲区，1命中CLRU，2命中DLRU
    int blkno,cnt,bcount;
//    进行页对齐
    blkno = secno / SEC_PRE_PAGE;
    bcount = (secno + scount - 1) / SEC_PRE_PAGE - (secno) / SEC_PRE_PAGE + 1;
    cnt=bcount;
//   第一次调用该函数，则进行初始化
    if(ZJ_flag==0){
        CHead=CreateList();
        DHead=CreateList();
        BHead =CreateBIndex();
        InitVariable();
        ZJ_flag=1;
    }
    while(cnt>0){
        buffer_cnt++;
        tempBlk=blkno/PAGE_PRE_BLK;
//        查看是否在缓冲区中，命中缓冲区（存在CLRU移动到DLRU）
        flag=0;
        pt=FindLPNinList(CHead,blkno);
        if(pt!=NULL){
//         命中CLRU
            flag=1;
        } else{
//          未命中DLRU,查看是否命中DLRU
            pt=FindLPNinList(DHead,blkno);
            if(pt!=NULL){
//                命中DLRU
                flag=2;
            }
        }
//      针对不同的命中队列调用不同的操作函数
        if(pt!=NULL){
            buffer_hit++;
            if(flag==1){
//                命中CLRU
                HitCLRU(CHead,DHead,pt,operation);
            }else
            {
//             命中DLRU
                HitDLRU(DHead,pt,operation);
            }
        } else {
//        没有命中缓冲区，根据Tau值选择剔除队列
            buffer_miss_cnt++;
            if(CLRU_Length+DLRU_Length>=buf_size){
                if(CLRU_Length>Tau){
//                    删除CLRU中的数据项
                    DelCLRU(CHead);
                }else{
//                    删除DLRU中的数据项
                    flash_delay+=DelOneDLRU(DHead);
                }
            }
//        加载新的数据到缓冲区
            flash_delay+=AddNewToBuffer(CHead,DHead,blkno,operation);
        }
//      看是否触发，更新Tau的更新机制
        if(T_count==UpdateTauCycle){
            UpdateTau();
        }else {
            T_count++;
        }

        blkno++;
        cnt--;
    }

    //返回计算请求的时延
    cache_delay=ComputeCacheDelay();
    delay=cache_delay+flash_delay;
    //周期观测数据的输出
    if(0 == CycleCount % ObserveCycle && CycleCount != 0){
        AveDelay=DelaySum/ObserveCycle;
        DelaySum=0.0;
        printf("the %d Request,the AveDelay is %f\n",CycleCount,AveDelay);
        Curr_buff_miss_cnt=buffer_miss_cnt-Last_buffer_miss_cnt;
        Curr_buffer_cnt=buffer_cnt-Last_buffer_cnt;
        Curr_buffer_read_miss=buffer_read_miss-Last_buffer_read_miss;
        Curr_buffer_write_miss=buffer_write_miss-Last_bufer_write_miss;
        printf("the Curr_buff_cnt is %d\tbuffer miss cnt is %d\n",Curr_buffer_cnt,Curr_buff_miss_cnt);
        printf("the Curr buff read miss is %d\t write miss is %d\n",Curr_buffer_read_miss,Curr_buffer_write_miss);
        Last_buffer_cnt=buffer_cnt;
        Last_buffer_miss_cnt=buffer_miss_cnt;
        Last_buffer_read_miss=buffer_read_miss;
        Last_bufer_write_miss=buffer_write_miss;
        printf("Curr Tau is %d\n",Tau);
        printf("..............................................................\n");
        CycleCount++;
    }else{
        DelaySum+=delay;
        CycleCount++;
    }

    return delay;
}

