/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu)
 *
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * Description: This is a header file for ssd_interface.c.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fast.h"
#include "pagemap.h"
#include "flash.h"
#include "type.h"

#define READ_DELAY        (0.1309/4)
#define WRITE_DELAY       (0.4059/4)
#define ERASE_DELAY       1.5 
#define GC_READ_DELAY  READ_DELAY    // gc read_delay = read delay    
#define GC_WRITE_DELAY WRITE_DELAY  // gc write_delay = write delay 

#define OOB_READ_DELAY    0.0
#define OOB_WRITE_DELAY   0.0

struct ftl_operation * ftl_op;

#define PAGE_READ     0
#define PAGE_WRITE    1
#define OOB_READ      2
#define OOB_WRITE     3
#define BLOCK_ERASE   4
#define GC_PAGE_READ  5
#define GC_PAGE_WRITE 6

void reset_flash_stat();
double calculate_delay_flash();
void initFlash();
void endFlash();
void printWearout();
void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag);
void find_real_max();
void find_real_min();
int find_min_ghost_entry();
void synchronize_disk_flash();
void find_min_cache();
double callFsim(unsigned int secno, int scount, int operation);

int write_count;
int read_count;

int flash_read_num;
int flash_write_num;
int flash_gc_read_num;
int flash_gc_write_num;
int flash_erase_num;
int flash_oob_read_num;
int flash_oob_write_num;

int map_flash_read_num;
int map_flash_write_num;
int map_flash_gc_read_num;
int map_flash_gc_write_num;
int map_flash_erase_num;
int map_flash_oob_read_num;
int map_flash_oob_write_num;

int ftl_type;

extern int total_util_sect_num; 
extern int total_extra_sect_num;

int global_total_blk_num;

int warm_done; 

int total_er_cnt;
int flag_er_cnt;
int block_er_flag[20000];
int block_dead_flag[20000];
int wear_level_flag[20000];
int unique_blk_num; 
int unique_log_blk_num;
int last_unique_log_blk;

int total_extr_blk_num;
int total_init_blk_num;

//和缓冲区相关的状态标识
#define CACHE_INVALID 0
#define CACHE_VALID 1
#define CACHE_INCLRU 2
#define CACHE_INDLRU 3


//定义cache的读写延时
#define CACHE_READ_DELAY 0.0005
#define CACHE_WRITE_DELAY 0.0005

//cache (read/write count) variable
int cache_read_num;
int cache_write_num;

//表示当前的CLRU和DLRU状态
int CACHE_CLRU_NUM_ENTRIES;
int CACHE_DLRU_NUM_ENTRIES;
int CACHE_NUM_ENTRIES;

//定义算法需要用到的结构体
//page_entry表示所用数据页在缓冲区可能的状态
struct page_entry{
    int cache_status;
    int cache_age;//在缓冲区滞留的时间，越久age越大
    int cache_update;//缓冲区更新的标志
};
unsigned  int NandPage_Num;//只表示数据页
struct  page_entry *NandPage;
//关于cache的读写统计 在对应的fast.c中的lm_init初始化
int buffer_cnt;
int buffer_miss_cnt;
int buffer_write_hit;
int buffer_write_miss;
int buffer_read_hit;
int buffer_read_miss;
int physical_read;
int physical_write;
//和算法相关的函数定义
void init_my_arr();//初始化自己算法中所需要用到的数组
void reset_cache_stat();
double calculate_delay_cache();
int my_find_cache_min(int *arr,int arrMaxSize);
int my_find_cache_max(int *arr,int arrMaxSize);
int search_table(int *arr,int size,int val);
int calculate_arr_positive_num(int *arr,int size);
int find_free_pos(int *arr,int size);
double cacheManage(unsigned int secno,int scount,int operation);

//和AD-LRU算法相关的函数和数据定义
//定义节点
typedef struct Node
{
    int LPN;
    struct Node *Pre;
    struct Node *Next;
    int isD;
    int cold;
}Node ,*pNode;
//定义和AD-LRU相关的变量
int Min_lc;
pNode HotHead,ColdHead;
int HotLength,ColdLength;

//函数操作
//创建双向链表
pNode CreateList();
//初始化两个冷热队列
void InitDoubleList();
void InitVariable();
void DestoryDouleList();
//判断链表是否为空
int IsEmptyList(pNode pHead);
//输出打印链表
void PrintList(pNode pHead);
//计算链表长度
int GetListLength(pNode pHead);
//从链表中插入节点,这里之后有读写操作的operation
int InsertEleList(pNode pHead,int pos,int LPN,int operation);
//向链表中删除节点
int DeleteEleList(pNode pHead,int pos);
//删除整个链表，释放内存
void FreeList(pNode *ppHead);
//从链表中找到特定的LPN值，并返回节点的指针位置,如果不存在返回NULL
pNode FindLPNinList(pNode pHead,int LPN);
//找到剔除项AD-LRU的二次机会，返回的是要剔除的项的指针
pNode FindVicitmList(pNode pHad);
//这里需要注意针对MoveToHotMRU也包括了命中HotList移动的
int MoveToHotMRU(pNode HHead,pNode HitNode);
int DelLRUList(pNode pHead);
pNode FindLPNInBuffer(int LPN,int operation);
double ComputeCacheDelay();
double CacheManage(int secno,int scount,int operation );
void PrintBufferStat();
//删除操作,选择合适的队列剔除操作，根据min_lc
int ExcludeVicitm();
