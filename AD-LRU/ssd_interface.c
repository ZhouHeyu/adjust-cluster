/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu_
 *   
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * This source plays a role as bridiging disksim and flash simulator. 
 * 
 * Request processing flow: 
 *
 *  1. Request is sent to the simple flash device module. 
 *  2. This interface determines FTL type. Then, it sends the request
 *     to the lower layer according to the FTL type. 
 *  3. It returns total time taken for processing the request in the flash. 
 *
 */

#include "ssd_interface.h"
#include "disksim_global.h"
#include "dftl.h"

extern int merge_switch_num;
extern int merge_partial_num;
extern int merge_full_num;
int old_merge_switch_num = 0;
int old_merge_partial_num = 0;
int old_merge_full_num= 0;
int old_flash_gc_read_num = 0;
int old_flash_erase_num = 0;
int req_count_num = 1;
int cache_hit, rqst_cnt;
int flag1 = 1;
int count = 0;

int page_num_for_2nd_map_table;

#define MAP_REAL_MAX_ENTRIES 6552// real map table size in bytes
#define MAP_GHOST_MAX_ENTRIES 1640// ghost_num is no of entries chk if this is ok

#define CACHE_MAX_ENTRIES 1024
int ghost_arr[MAP_GHOST_MAX_ENTRIES];
int real_arr[MAP_REAL_MAX_ENTRIES];
int cache_arr[CACHE_MAX_ENTRIES];

/***********************************************************************
  Variables for statistics    
 ***********************************************************************/
unsigned int cnt_read = 0;
unsigned int cnt_write = 0;
unsigned int cnt_delete = 0;
unsigned int cnt_evict_from_flash = 0;
unsigned int cnt_evict_into_disk = 0;
unsigned int cnt_fetch_miss_from_disk = 0;
unsigned int cnt_fetch_miss_into_flash = 0;

double sum_of_queue_time = 0.0;
double sum_of_service_time = 0.0;
double sum_of_response_time = 0.0;
unsigned int total_num_of_req = 0;

/***********************************************************************
  Mapping table
 ***********************************************************************/
int real_min = -1;
int real_max = 0;

/***********************************************************************
  Cache
 ***********************************************************************/
int cache_min = -1;
int cache_max = 0;

// Interface between disksim & fsim 

void reset_flash_stat()
{
  flash_read_num = 0;
  flash_write_num = 0;
  flash_gc_read_num = 0;
  flash_gc_write_num = 0; 
  flash_erase_num = 0;
  flash_oob_read_num = 0;
  flash_oob_write_num = 0; 
}

FILE *fp_flash_stat;
FILE *fp_gc;
FILE *fp_gc_timeseries;
double gc_di =0 ,gc_ti=0;


double calculate_delay_flash()
{
  double delay;
  double read_delay, write_delay;
  double erase_delay;
  double gc_read_delay, gc_write_delay;
  double oob_write_delay, oob_read_delay;

  oob_read_delay  = (double)OOB_READ_DELAY  * flash_oob_read_num;
  oob_write_delay = (double)OOB_WRITE_DELAY * flash_oob_write_num;

  read_delay     = (double)READ_DELAY  * flash_read_num; 
  write_delay    = (double)WRITE_DELAY * flash_write_num; 
  erase_delay    = (double)ERASE_DELAY * flash_erase_num; 

  gc_read_delay  = (double)GC_READ_DELAY  * flash_gc_read_num; 
  gc_write_delay = (double)GC_WRITE_DELAY * flash_gc_write_num; 


  delay = read_delay + write_delay + erase_delay + gc_read_delay + gc_write_delay + 
    oob_read_delay + oob_write_delay;

  if( flash_gc_read_num > 0 || flash_gc_write_num > 0 || flash_erase_num > 0 ) {
    gc_ti += delay;
  }
  else {
    gc_di += delay;
  }

  if(warm_done == 1){
    fprintf(fp_gc_timeseries, "%d\t%d\t%d\t%d\t%d\t%d\n", 
      req_count_num, merge_switch_num - old_merge_switch_num, 
      merge_partial_num - old_merge_partial_num, 
      merge_full_num - old_merge_full_num, 
      flash_gc_read_num,
      flash_erase_num);

    old_merge_switch_num = merge_switch_num;
    old_merge_partial_num = merge_partial_num;
    old_merge_full_num = merge_full_num;
    req_count_num++;
  }

  reset_flash_stat();

  return delay;
}
/************************************************************************
 *  Zhou-Jie 导入的数据和函数
 ************************************************************************/

//选择缓冲区算法的cache_type
int cache_type=2;
//统计buffer的操作
int buffer_hit=0;
int buffer_cnt=0;
//关于cache当前最大age对应的LPN索引
int cache_max_age_index=0;
int cache_min_age_index=0;
//算法所需的数组
int lru_cache_arr[CACHE_MAX_ENTRIES];
int clru_cache_arr[CACHE_MAX_ENTRIES];
int dlru_cache_arr[CACHE_MAX_ENTRIES];

//初始化数组，参考init_arr
void init_my_arr()
{
    int i;
    for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
        lru_cache_arr[i] = -1;
        clru_cache_arr[i] =-1;
        dlru_cache_arr[i] =-1;
    }
}
//reset cache state
void reset_cache_stat()
{
    cache_read_num = 0;
    cache_write_num = 0;
}
//计算cache的操作时延
double calculate_delay_cache()
{
    double delay;
    double cache_read_delay=0.0,cache_write_delay=0.0;
    cache_read_delay=(double)CACHE_READ_DELAY*cache_read_num;
    cache_write_delay=(double)CACHE_WRITE_DELAY*cache_write_num;
    delay=cache_read_delay+cache_write_delay;
    reset_cache_stat();
    return delay;
}

//找到数组中索引对应的LPN的age最大或最小
int my_find_cache_min(int *arr,int arrMaxSize)
{
    int i;
    int temp=99999999;
    int cache_min=-1;
    for (i = 0; i <arrMaxSize ; ++i) {
        //attention arr[i]=-1 ,the NandPage[-1] is error
        if(arr[i]!=-1){
            if(NandPage[arr[i]].cache_age<temp){
                temp=NandPage[arr[i]].cache_age;
                cache_min=arr[i];
            }
        }
    }
    return cache_min;
}
int my_find_cache_max(int *arr,int arrMaxSize)
{
    int i;
    int temp=-1;
    int cache_max=-1;
    for ( i = 0; i <arrMaxSize ; ++i) {
        if (arr[i]!=-1){
            if (NandPage[arr[i]].cache_age>temp){
                temp=NandPage[arr[i]].cache_age;
                cache_max=arr[i];
            }
        }
    }
    return cache_max;
}
//从对应的数组中找到对应的数据项，并返回该数据项在数组的中位置
//没有找到对应的数据则返回-1
int search_table(int *arr,int size,int val)
{
    int i;
    for (i = 0; i <size ; ++i) {
        if(arr[i]==val){
            return i;
        }
    }
    return -1;
}
//计算数组中非负数个数
int calculate_arr_positive_num(int *arr,int size)
{
    int i;
    int count=0;
    for ( i = 0; i <size ; ++i) {
        if(arr[i]>=0){
            count++;
        }
    }
    return count;
}
//找到数组中空闲的位置(存放-1的位置)，如果没有返回-1
/*int find_free_pos(int *arr,int size)
{
    int i;
    for ( i = 0; i <size ; ++i) {
        if(arr[i]==-1){
            return i;
        }
    }
    printf("shouldnt come here for find_free_pos()");
    exit(1);
    return -1;
}*/

/****************************************************************************
  导入和AD-LRU相关的链表操作
 ****************************************************************************/
//创建双向链表
pNode CreateList()
{
    int i,length=0,data=0;
    //分配初始内存
    pNode  pHead=(pNode)malloc(sizeof(Node));
    if(NULL==pHead){
        printf("malloc for pHead failed!\n");
        exit(1);
    }
    pHead->LPN=-1;
    pHead->isD=-1;
    pHead->cold=-1;
    pHead->Pre=pHead;
    pHead->Next=pHead;

    return pHead;
}

//初始化两个队列的头部和长度
void InitDoubleList()
{
    HotHead=CreateList();
    HotLength=0;
    ColdHead=CreateList();
    ColdLength=0;
}

void InitVariable()
{
    Min_lc=0.2*CACHE_MAX_ENTRIES;
    physical_write=0;
    physical_read=0;
}
//删除整个链表，释放内存
void FreeList(pNode *ppHead)
{
    pNode pt=NULL;
    while(*ppHead!=NULL){
        pt=(*ppHead)->Next;
        free(*ppHead);
        if(NULL!=pt)
            pt->Pre=NULL;
        *ppHead=pt;
    }
}

//释放两个队列
void DestoryDouleList()
{
    FreeList(&HotHead);
    HotLength=0;
    FreeList(&ColdHead);
    ColdLength=0;
}
//判断链表是否为空
int IsEmptyList(pNode pHead)
{
    pNode pt=pHead->Next;
    if(pt==pHead)
    {
        return 1;
    }else
    {
        return 0;
    }
}

//返回链表的长度
int GetListLength(pNode pHead)
{
    int length=0;
    pNode pt=pHead->Next;
    while (pt !=pHead)
    {
        length++;
        pt=pt->Next;
    }
    return length;
}

//输出打印链表
void PrintList(pNode pHead)
{
    pNode pt=pHead->Next;
    printf("the list is follow:\n");
    while(pt!=pHead)
    {
        printf("the LPN is %d\t",pt->LPN);
        printf("the cold is %d\t",pt->cold);
        printf("the isD is %d\n",pt->isD);
        pt=pt->Next;
    }
}

//从链表中插入节点,这里之后有读写操作的operation
int InsertEleList(pNode pHead,int pos,int LPN,int operation)
{
    pNode pt=NULL,p_new=NULL;
    pt=pHead->Next;
    if(pos>0&&pos<GetListLength(pHead)+2)
    {
        p_new=(pNode)malloc(sizeof(Node));
        if(NULL==p_new)
        {
            printf("malloc for New Node!\n");
            exit(-1);
        }

        while(1)
        {
            pos--;
            if(0==pos)
                break;
            pt=pt->Next;
        }
        //新建节点
        p_new->LPN=LPN;
        if(operation==0)
            p_new->isD=1;
        else
            p_new->isD=0;
        //第一次插入的节点默认是非cold
        p_new->cold=0;
        //这里的双链表是首位相接的，也就是head前面就是最后一个节点
        //插入pt的前面
        p_new->Pre=pt->Pre;
        p_new->Next=pt;
        //衔接
        pt->Pre->Next=p_new;
        pt->Pre=p_new;
        return 1;
    }else {
        return  0;
    }
}

//向链表中删除节点
int DeleteEleList(pNode pHead,int pos)
{
    pNode pt=pHead,ps=NULL;
    //pos=0就是pHead
    if(pos>0&&pos<GetListLength(pHead)+1){
        while(1)
        {
            pos--;
            if(pos==0)
                break;
            pt=pt->Next;
        }
        //此时的pt是pos的前一个节点
        //ps才是要删除的节点位置
        ps=pt->Next;
        pt->Next=ps->Next;
        ps->Next->Pre=pt;
        //释放ps指向的节点
        free(ps);
        return 1;
    }else{
        printf("delete pos %d is error\n",pos);
        return 0;
    }

}

//删除链表的尾部的数据
int DelLRUList(pNode pHead)
{
    pNode pt=pHead->Pre;
    if(pt==pHead){
        printf("List is empty！！\n");
        exit(1);
    }
    //将尾部衔接
    pt->Pre->Next=pHead;
    pHead->Pre=pt->Pre;
    //释放删除点pt的内存
    free(pt);
    return 1;
}

//从链表中找到特定的LPN值，并返回节点的指针位置,如果不存在返回NULL
pNode FindLPNinList(pNode pHead,int LPN)
{
    pNode ps=NULL,pt=pHead->Next;
    int count=0;
    while(pt!=pHead)
    {
        count++;
        if(pt->LPN==LPN){
            ps=pt;
            ps->cold=0;
            break;
        }
        pt=pt->Next;
    }
    //调试输出语句遍历循环了多少次
//    printf("the while count is %d\n",count);
    return ps;
}

//将命中的数据移动到MRU位置,因为节点的类型一样，所以改函数的复用没问题
//都是移动到Hot-list的MRU位置
int MoveToHotMRU(pNode HHead,pNode HitNode)
{
    pNode pt=NULL,ps=NULL;
    //删除cold-list中的命中节点
    HitNode->cold=0;
    pt=HitNode->Pre;
    pt->Next=HitNode->Next;
    HitNode->Next->Pre=pt;
    //插入到hot-list队列中的MRU位置
    ps=HHead->Next;
    ps->Pre=HitNode;
    HHead->Next=HitNode;
    HitNode->Next=ps;
    HitNode->Pre=HHead;

    //更改对应的队列长度,服用存在问题，命中的可能是hot-list的队列
    ColdLength=GetListLength(ColdHead);
    HotLength=GetListLength(HotHead);
    //错误判断
    if(ColdLength!=GetListLength(ColdHead)||HotLength!=GetListLength(HotHead))
    {
        printf("Move To Hot MRU exist error\n");
        printf("ColdLength is %d\t the length of Cold-list is %d\n",ColdLength,GetListLength(ColdHead));
        printf("HotLength is %d\t the length of hot-list is %d\n",HotLength,GetListLength(HotHead));
        exit(-1);
    }
    return 1;
}

//找到剔除项AD-LRU的二次机会，返回的是要剔除的项的指针,优先提出干净页
pNode FindVicitmList(pNode pHead)
{
    pNode Victim=pHead->Pre,pt=NULL,ps=NULL;
    int count=0,L;
    int flag=0;
    L=GetListLength(pHead);
    //debug test
    if(IsEmptyList(pHead)!=0){
        printf("the list is empty!\n");
        exit(-1);
    }
    //debug test
    //先遍历寻找队列中尾部的干净页优先提出
    ps=pHead->Pre;
    while(ps!=pHead)
    {
        if(ps->isD==0){
            Victim=ps;
            //找到干净页则不用下面遍历寻找脏页剔除了
            flag=1;
            break;
        }
        //向前继续搜索干净页
        ps=ps->Pre;
    }
    //如果找不到干净页才循环遍历脏页剔除
    if (0 == flag) {
        while (Victim->cold != 1) {
            pt = Victim;
            Victim = Victim->Pre;
            pt->cold = 1;
            //之前的Victim移动到Head后面
            pt->Pre = pHead;
            pt->Next = pHead->Next;
            pHead->Next->Pre = pt;
            pHead->Next = pt;
            //衔接新的尾部
            pHead->Pre = Victim;
            Victim->Next = pHead;
            //debug
            count++;
            if (count > L + 2) {
                printf("exist error in while\n");
                break;
            }
            //debug
        }
        //test for debug脏页提出一定最后是尾部的LRU
        if (Victim != pHead->Pre) {
            printf("this operation exist error\n");
            exit(-1);
        }
    }
    return Victim;
}


//找到buffer中是否存在改LPN，存在返回改LPN的对应节点的指针,无论命中那个都是要移动到Hot-List的MRU
pNode FindLPNInBuffer(int LPN,int operation)
{
    pNode pt=NULL;
    pt=FindLPNinList(HotHead,LPN);
    if(pt==NULL)
    {
        pt=FindLPNinList(ColdHead,LPN);
    }
    //遍历结束
    if(pt!=NULL){
        //判断命中的类型
        buffer_hit++;
        pt->cold=0;
        if(operation==0){
            pt->isD=1;
            buffer_write_hit++;
            cache_write_num++;
        } else{
            buffer_read_hit++;
            cache_read_num++;
        }
    }else{
        buffer_miss_cnt++;
        if(operation==0){
            buffer_write_miss++;
        }else{
            buffer_read_miss++;
        }
    }
    return pt;
}

//添加新的数据到cold
void AddNewToCold(int LPN,int operation)
{
    InsertEleList(ColdHead,1,LPN,operation);
    ColdLength++;
    physical_read++;
    //debug test
    if(GetListLength(ColdHead)!=ColdLength){
        printf("the add new to cold list exist error\n");
        printf("the ColdLength is %d\t the Length of Cold-list is %d\n",ColdLength,GetListLength(ColdHead));
        exit(1);
    }
}


//删除链表中的对应的数据页项
int DelEleList(pNode pHead,pNode Victim)
{
    //首先做一个错误检测，判断相应的队列是否存在该Victim
    int flag=0;
    pNode pt=pHead->Pre;
    while(pt!=pHead){
        //找到对应的剔除选项
        if(pt==Victim)
        {
            flag=1;
            break;
        }
        pt=pt->Pre;
    }
    if(flag==0){
        printf("链表中不存在删除的数据项\n");
        exit(1);
    }
    //如果上述的检测都满足，则开始直接删除
    pt=Victim->Pre;
    //删除节点前后之间建立新的链接关系
    pt->Next=Victim->Next;
    Victim->Next->Pre=pt;
    //释放对应的节点
    free(Victim);
    return 0;
}

//删除操作,选择合适的队列剔除操作，根据min_lc,返回值非负数为要删除的脏页LPN
int ExcludeVicitm()
{
    pNode Victim=NULL;
    int DelLPN;
    //错误判断
    if(ColdLength!=GetListLength(ColdHead)||HotLength!=GetListLength(HotHead))
    {
        printf("Exclude Vicitm function error\n");
        printf("ColdLength is %d\t the length of Cold-list is %d\n",ColdLength,GetListLength(ColdHead));
        printf("HotLength is %d\t the length of hot-list is %d\n",HotLength,GetListLength(HotHead));
        exit(-1);
    }

    if(ColdLength>Min_lc){
        //选择cold-list剔除
        Victim=FindVicitmList(ColdHead);
        if(Victim==NULL){
            printf("Find VicitmList exist error\n");
            exit(-1);
        }
        //判断剔除的Vicitm是否为脏
        if(Victim->isD==1){
            physical_write++;
            DelLPN=Victim->LPN;
        }else{
            DelLPN=-1;
        }
        //剔除尾部的数据
//        DelLRUList(ColdHead);
        DelEleList(ColdHead,Victim);
        ColdLength--;
        //debug
        if(ColdLength!=GetListLength(ColdHead)){
            printf("Del LRU in Cold-List exist error\n");
            printf("ColdLength is %d\t the length of Cold-list is %d\n",ColdLength,GetListLength(ColdHead));
            exit(1);
        }
    }else{
        Victim=FindVicitmList(HotHead);
        if(Victim==NULL){
            printf("Find VicitmList exist error\n");
            exit(1);
        }
        if(Victim->isD==1){
            physical_write++;
            DelLPN=Victim->LPN;
        } else{
            DelLPN=-1;
        }
        //剔除尾部的数据
//        DelLRUList(HotHead);
        DelEleList(HotHead,Victim);
        HotLength--;
        //debug
        if(HotLength!=GetListLength(HotHead)){
            printf("Del LRU in Hot-List exist error\n");
            printf("HotLength is %d\t the length of hot-list is %d\n",HotLength,GetListLength(HotHead));
            exit(1);
        }
    }
    return DelLPN;
}

/***********************************************************************
  Initialize Flash Drive 
  ***********************************************************************/

void initFlash()
{
  blk_t total_blk_num;
  blk_t total_util_blk_num;
  blk_t total_extr_blk_num;

  // total number of sectors    
  total_util_sect_num  = flash_numblocks;
  total_extra_sect_num = flash_extrblocks;
  total_sect_num = total_util_sect_num + total_extra_sect_num; 

  // total number of blocks 
  total_blk_num      = total_sect_num / SECT_NUM_PER_BLK;     // total block number
  total_util_blk_num = total_util_sect_num / SECT_NUM_PER_BLK;    // total unique block number

  global_total_blk_num = total_util_blk_num;

  total_extr_blk_num = total_blk_num - total_util_blk_num;        // total extra block number

  ASSERT(total_extr_blk_num != 0);

  if (nand_init(total_blk_num, 3) < 0) {
    EXIT(-4); 
  }

  switch(ftl_type){

    // pagemap
    case 1: ftl_op = pm_setup(); break;
    // blockmap
    //case 2: ftl_op = bm_setup(); break;
    // o-pagemap 
    case 3: ftl_op = opm_setup(); break;
    // fast
    case 4: ftl_op = lm_setup(); break;

    default: break;
  }

  ftl_op->init(total_util_blk_num, total_extr_blk_num);

  nand_stat_reset();
}

void printWearout()
{
  int i;
  FILE *fp = fopen("wearout", "w");
  
  for(i = 0; i<nand_blk_num; i++)
  {
    fprintf(fp, "%d %d\n", i, nand_blk[i].state.ec); 
  }

  fclose(fp);
}


void endFlash()
{
  nand_stat_print(outputfile);
  ftl_op->end;
  nand_end();
}  

/***********************************************************************
  Send request (lsn, sector_cnt, operation flag)
  ***********************************************************************/

void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag)
{
	int size;
	//size_t (*op_func)(sect_t lsn, size_t size);
	size_t (*op_func)(sect_t lsn, size_t size, int mapdir_flag);

        if((start_blk_no + block_cnt) >= total_util_sect_num){
          printf("start_blk_no: %d, block_cnt: %d, total_util_sect_num: %d\n", 
              start_blk_no, block_cnt, total_util_sect_num);
          exit(0);
        }

	switch(operation){

	//write
	case 0:

		op_func = ftl_op->write;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;
	//read
	case 1:


		op_func = ftl_op->read;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;

	default: 
		break;
	}
}

void find_real_max()
{
  int i; 

  for(i=0;i < MAP_REAL_MAX_ENTRIES; i++) {
      if(opagemap[real_arr[i]].map_age > opagemap[real_max].map_age) {
          real_max = real_arr[i];
      }
  }
}

void find_real_min()
{
  
  int i,index; 
  int temp = 99999999;

  for(i=0; i < MAP_REAL_MAX_ENTRIES; i++) {
        if(opagemap[real_arr[i]].map_age <= temp) {
            real_min = real_arr[i];
            temp = opagemap[real_arr[i]].map_age;
            index = i;
        }
  }    
}

int find_min_ghost_entry()
{
  int i; 

  int ghost_min = 0;
  int temp = 99999999; 

  for(i=0; i < MAP_GHOST_MAX_ENTRIES; i++) {
    if( opagemap[ghost_arr[i]].map_age <= temp) {
      ghost_min = ghost_arr[i];
      temp = opagemap[ghost_arr[i]].map_age;
    }
  }
  return ghost_min;
}

void init_arr()
{

  int i;
  for( i = 0; i < MAP_REAL_MAX_ENTRIES; i++) {
      real_arr[i] = -1;
  }
  for( i = 0; i < MAP_GHOST_MAX_ENTRIES; i++) {
      ghost_arr[i] = -1;
  }
  for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
      cache_arr[i] = -1;
  }

}

/*
int search_table(int *arr, int size, int val) 
{
    int i;
    for(i =0 ; i < size; i++) {
        if(arr[i] == val) {
            return i;
        }
    }

    printf("shouldnt come here for search_table()=%d,%d",val,size);
    for( i = 0; i < size; i++) {
      if(arr[i] != -1) {
        printf("arr[%d]=%d ",i,arr[i]);
      }
    }
    exit(1);
    return -1;
}
**/

int find_free_pos( int *arr, int size)
{
    int i;
    for(i = 0 ; i < size; i++) {
        if(arr[i] == -1) {
            return i;
        }
    } 
    printf("shouldnt come here for find_free_pos()");
    exit(1);
    return -1;
}

void find_min_cache()
{
  int i; 
  int temp = 999999;

  for(i=0; i < CACHE_MAX_ENTRIES ;i++) {
      if(opagemap[cache_arr[i]].cache_age <= temp ) {
          cache_min = cache_arr[i];
          temp = opagemap[cache_arr[i]].cache_age;
      }
  }
}

int youkim_flag1=0;

double callFsim(unsigned int secno, int scount, int operation)
{
  double delay; 
  int bcount;
  unsigned int blkno; // pageno for page based FTL
  int cnt,z; int min_ghost;

  int pos=-1,pos_real=-1,pos_ghost=-1;

  if(ftl_type == 1){ }

  if(ftl_type == 3) {
      page_num_for_2nd_map_table = (opagemap_num / MAP_ENTRIES_PER_PAGE);
    
      if(youkim_flag1 == 0 ) {
        youkim_flag1 = 1;
        init_arr();
      }

      if((opagemap_num % MAP_ENTRIES_PER_PAGE) != 0){
        page_num_for_2nd_map_table++;
      }
  }
      
  // page based FTL 
  if(ftl_type == 1 ) { 
    blkno = secno / 4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // block based FTL 
  else if(ftl_type == 2){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }
  // o-pagemap scheme
  else if(ftl_type == 3 ) { 
    blkno = secno / 4;
    blkno += page_num_for_2nd_map_table;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // FAST scheme
  else if(ftl_type == 4){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }

  cnt = bcount;

  switch(operation)
  {
    //write/read
    case 0:
    case 1:

    while(cnt > 0)
    {
          cnt--;

        // page based FTL
        if(ftl_type == 1){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // blck based FTL
        else if(ftl_type == 2){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // opagemap ftl scheme
        else if(ftl_type == 3)
        {

          /************************************************
            primary map table 
          *************************************************/
          //1. pagemap in SRAM 

            rqst_cnt++;
          if((opagemap[blkno].map_status == MAP_REAL) || (opagemap[blkno].map_status == MAP_GHOST))
          {
            cache_hit++;

            opagemap[blkno].map_age++;

            if(opagemap[blkno].map_status == MAP_GHOST){

              if ( real_min == -1 ) {
                real_min = 0;
                find_real_min();
              }    
              if(opagemap[real_min].map_age <= opagemap[blkno].map_age) 
              {
                find_real_min();  // probably the blkno is the new real_min alwaz
                opagemap[blkno].map_status = MAP_REAL;
                opagemap[real_min].map_status = MAP_GHOST;

                pos_ghost = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,blkno);
                ghost_arr[pos_ghost] = -1;
                
                pos_real = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos_real] = -1;

                real_arr[pos_real]   = blkno; 
                ghost_arr[pos_ghost] = real_min; 
              }
            }
            else if(opagemap[blkno].map_status == MAP_REAL) 
            {
              if ( real_max == -1 ) {
                real_max = 0;
                find_real_max();
                printf("Never happend\n");
              }

              if(opagemap[real_max].map_age <= opagemap[blkno].map_age)
              {
                real_max = blkno;
              }  
            }
            else {
              printf("forbidden/shouldnt happen real =%d , ghost =%d\n",MAP_REAL,MAP_GHOST);
            }
          }

          //2. opagemap not in SRAM 
          else
          {
            //if map table in SRAM is full
            if((MAP_REAL_MAX_ENTRIES - MAP_REAL_NUM_ENTRIES) == 0)
            {
              if((MAP_GHOST_MAX_ENTRIES - MAP_GHOST_NUM_ENTRIES) == 0)
              { //evict one entry from ghost cache to DRAM or Disk, delay = DRAM or disk write, 1 oob write for invalidation 
                min_ghost = find_min_ghost_entry();
                  evict++;

                if(opagemap[min_ghost].update == 1) {
                  update_reqd++;
                  opagemap[min_ghost].update = 0;
                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table then update it

                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 0, 2);   // write into 2nd mapping table 
                } 
                opagemap[min_ghost].map_status = MAP_INVALID;

                MAP_GHOST_NUM_ENTRIES--;

                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                MAP_GHOST_NUM_ENTRIES++;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;

                pos = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,min_ghost);
                ghost_arr[pos]=-1;

                
                ghost_arr[pos]= real_min;
                
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;
              }
              else{
                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;
               
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;

                pos = find_free_pos(ghost_arr,MAP_GHOST_MAX_ENTRIES);
                ghost_arr[pos]=real_min;
                
                MAP_GHOST_NUM_ENTRIES++;
              }
            }

            flash_hit++;
            send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table

            opagemap[blkno].map_status = MAP_REAL;

            opagemap[blkno].map_age = opagemap[real_max].map_age + 1;
            real_max = blkno;
            MAP_REAL_NUM_ENTRIES++;
            
            pos = find_free_pos(real_arr,MAP_REAL_MAX_ENTRIES);
            real_arr[pos] = blkno;
          }

         //comment out the next line when using cache
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // FAST scheme  
        else if(ftl_type == 4){ 

          if(operation == 0){
            write_count++;
          }
          else read_count++;

          send_flash_request(blkno*4, 4, operation, 1); //cache_min is a page for page baseed FTL
          blkno++;
        }
    }
    break;
  }

  delay = calculate_delay_flash();

  return delay;
}

int zhoujie_flag=0;
//实现缓冲区算法的管理函数，输入参数说明
//secno 请求扇区地址；scount 请求扇区大小； operation 读写操作类型
//函数返回的是缓冲区操作的时延（包含底层send_request的函数调用）
double cacheManage(unsigned int secno,int scount,int operation)
{
  //定义时延变量
    double delay,flash_delay=0,cache_delay=0;
    int bcount,entry_count;
    unsigned int blkno;
  //定义算法需要使用的中间变量
    int  free_pos=-1;
    int  vicitm_page_index=-1;
    int  cnt=0;
  //关于调整Tau值的变化，观测周期(AdjusterCircle)及累计变量T_count(定期复位)

    //定义AD-LRU算法相关的中间变量
    pNode pt=NULL;
    int DelLPN;

  //本次函数是基于FAST的算法上层做缓冲区设计的（即ftl_type=4）
    if(ftl_type!=4){
      printf("算法基于FAST,配置文件中的ftl_type=4!\n");
      exit(-1);
    }
    //判断情况初始化自己的数组
    if(zhoujie_flag==0){
        zhoujie_flag=1;
        init_my_arr();
        //初始化AD-LRU相关的参数
        InitDoubleList();
        InitVariable();
    }
    //页对齐操作
    blkno=secno/4;
    bcount=(secno+scount-1)/4-(secno)/4+1;
    //重置cache_stat相关状态
    reset_cache_stat();
    cnt=bcount;

    switch (cache_type) {
        case 1://LRU algorithm
            while(cnt>0) {
                buffer_cnt++;
            if (NandPage[blkno].cache_status == CACHE_VALID) {
                buffer_hit++;
                NandPage[blkno].cache_age = NandPage[cache_max].cache_age + 1;
                cache_max_age_index = blkno;
                //根据读写类型区别操作
                if (operation == 0) {
                    NandPage[blkno].cache_update = 1;
                    buffer_write_hit++;
                    cache_write_num++;
                } else {
                    buffer_read_hit++;
                    cache_read_num++;
                }
            } else {
                //没有命中缓冲区
                buffer_miss_cnt++;
                //首先判断缓冲区有没有满
                if(CACHE_NUM_ENTRIES>=CACHE_MAX_ENTRIES)
                {
                    cache_min_age_index=my_find_cache_min(lru_cache_arr,CACHE_MAX_ENTRIES);
                    if(NandPage[cache_min_age_index].cache_update==1){
                        flash_delay+=callFsim(cache_min_age_index*4,4,0);
                    }
                    //重置NandPage相关的标识符
                    NandPage[cache_min_age_index].cache_age=0;
                    NandPage[cache_min_age_index].cache_update=0;
                    NandPage[cache_min_age_index].cache_status=CACHE_INVALID;
                    //从lru_cache_arr中删除对应的索引
                    vicitm_page_index=search_table(lru_cache_arr,CACHE_MAX_ENTRIES,cache_min_age_index);
                    //加入错误判读
                    if (vicitm_page_index==-1){
                        printf("the search_table find  vicitm of lru_arr error\n");
                        exit(-1);
                    }
                    lru_cache_arr[vicitm_page_index]=-1;
                    CACHE_NUM_ENTRIES--;
                    //加入错误判断
                    if(CACHE_NUM_ENTRIES!=calculate_arr_positive_num(lru_cache_arr,CACHE_MAX_ENTRIES)){
                        printf("lru数组中的有效项数和CACHE_NUM_ENTRIES不等\n");
                        exit(-1);
                    }
                    //debug-end
                }
                //读取新的数据到cache
                flash_delay+=callFsim(blkno*4,4,1);
                NandPage[blkno].cache_status=CACHE_VALID;
                NandPage[blkno].cache_age=NandPage[cache_max_age_index].cache_age+1;
                cache_max_age_index=blkno;
                free_pos=find_free_pos(lru_cache_arr,CACHE_MAX_ENTRIES);
                if(free_pos==-1){
                    printf("can't find free position for current LPN %d \n",blkno);
                    exit(0);
                }
                lru_cache_arr[free_pos]=blkno;
                if (operation==0){
                    NandPage[blkno].cache_update=1;
                    buffer_write_miss++;
                    cache_write_num++;
                }else{
                    buffer_read_miss++;
                    cache_read_num++;
                }
                CACHE_NUM_ENTRIES++;
                //加入错误判断
                if(CACHE_NUM_ENTRIES!=calculate_arr_positive_num(lru_cache_arr,CACHE_MAX_ENTRIES)){
                    printf("lru数组中的有效项数和CACHE_NUM_ENTRIES不等\n");
                    exit(-1);
                }
                //debug-end
            }//没有命中缓冲区操作结束
                cnt--;
                blkno++;
        }
        break;
        case 2://AD-LRU algorithm
            while(cnt>0){
                buffer_cnt++;
                pt=FindLPNInBuffer(blkno,operation);
                if(pt!=NULL){
                    //命中的都移动到Hot-list的队列中去
                    MoveToHotMRU(HotHead,pt);
                }else{
                    if(ColdLength+HotLength>=CACHE_MAX_ENTRIES){
                        //如果队列满了则选择剔除操作,提出的脏页LPN号
                        DelLPN=ExcludeVicitm();
                        if(DelLPN>=0){
                            flash_delay+=callFsim(DelLPN*4,4,0);
                        }
                    }
                    //添加新的数据到cold中
                    AddNewToCold(blkno,operation);
                    flash_delay+=callFsim(blkno*4,4,1);
                }
                cnt--;
                blkno++;
            }
            break;
    }
    //computed cache operation delay
    cache_delay=calculate_delay_cache();
    delay=flash_delay+cache_delay;
    //test point
    printf("the request blkno is %d\tcache_delay is %f flash_delay is %f\n",(blkno-bcount),cache_delay,flash_delay);
    return delay;

    //注意双链表的释放在对应的fast文件中释放
}



