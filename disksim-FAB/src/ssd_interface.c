/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu_
 *   
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * This source plays a role as bridiging disksim and flash simulator. 
 * 
 * Request processing flow: 
 *
 *  1. Request is sent to the simple flash device module. 
 *  2. This interface determines FTL type. Then, it sends the request
 *     to the lower layer according to the FTL type. 
 *  3. It returns total time taken for processing the request in the flash. 
 *
 */

#include "ssd_interface.h"
#include "disksim_global.h"
#include "dftl.h"

extern int merge_switch_num;
extern int merge_partial_num;
extern int merge_full_num;
int old_merge_switch_num = 0;
int old_merge_partial_num = 0;
int old_merge_full_num= 0;
int old_flash_gc_read_num = 0;
int old_flash_erase_num = 0;
int req_count_num = 1;
int cache_hit, rqst_cnt;
int flag1 = 1;
int count = 0;

int page_num_for_2nd_map_table;

#define MAP_REAL_MAX_ENTRIES 6552// real map table size in bytes
#define MAP_GHOST_MAX_ENTRIES 1640// ghost_num is no of entries chk if this is ok

#define CACHE_MAX_ENTRIES 300
int ghost_arr[MAP_GHOST_MAX_ENTRIES];
int real_arr[MAP_REAL_MAX_ENTRIES];
int cache_arr[CACHE_MAX_ENTRIES];

/***********************************************************************
  Variables for statistics    
 ***********************************************************************/
unsigned int cnt_read = 0;
unsigned int cnt_write = 0;
unsigned int cnt_delete = 0;
unsigned int cnt_evict_from_flash = 0;
unsigned int cnt_evict_into_disk = 0;
unsigned int cnt_fetch_miss_from_disk = 0;
unsigned int cnt_fetch_miss_into_flash = 0;

double sum_of_queue_time = 0.0;
double sum_of_service_time = 0.0;
double sum_of_response_time = 0.0;
unsigned int total_num_of_req = 0;

/***********************************************************************
  Mapping table
 ***********************************************************************/
int real_min = -1;
int real_max = 0;

/***********************************************************************
  Cache
 ***********************************************************************/
int cache_min = -1;
int cache_max = 0;

// Interface between disksim & fsim 

void reset_flash_stat()
{
  flash_read_num = 0;
  flash_write_num = 0;
  flash_gc_read_num = 0;
  flash_gc_write_num = 0; 
  flash_erase_num = 0;
  flash_oob_read_num = 0;
  flash_oob_write_num = 0; 
}

FILE *fp_flash_stat;
FILE *fp_gc;
FILE *fp_gc_timeseries;
double gc_di =0 ,gc_ti=0;


double calculate_delay_flash()
{
  double delay;
  double read_delay, write_delay;
  double erase_delay;
  double gc_read_delay, gc_write_delay;
  double oob_write_delay, oob_read_delay;

  oob_read_delay  = (double)OOB_READ_DELAY  * flash_oob_read_num;
  oob_write_delay = (double)OOB_WRITE_DELAY * flash_oob_write_num;

  read_delay     = (double)READ_DELAY  * flash_read_num; 
  write_delay    = (double)WRITE_DELAY * flash_write_num; 
  erase_delay    = (double)ERASE_DELAY * flash_erase_num; 

  gc_read_delay  = (double)GC_READ_DELAY  * flash_gc_read_num; 
  gc_write_delay = (double)GC_WRITE_DELAY * flash_gc_write_num; 


  delay = read_delay + write_delay + erase_delay + gc_read_delay + gc_write_delay + 
    oob_read_delay + oob_write_delay;

  if( flash_gc_read_num > 0 || flash_gc_write_num > 0 || flash_erase_num > 0 ) {
    gc_ti += delay;
  }
  else {
    gc_di += delay;
  }

  if(warm_done == 1){
    fprintf(fp_gc_timeseries, "%d\t%d\t%d\t%d\t%d\t%d\n", 
      req_count_num, merge_switch_num - old_merge_switch_num, 
      merge_partial_num - old_merge_partial_num, 
      merge_full_num - old_merge_full_num, 
      flash_gc_read_num,
      flash_erase_num);

    old_merge_switch_num = merge_switch_num;
    old_merge_partial_num = merge_partial_num;
    old_merge_full_num = merge_full_num;
    req_count_num++;
  }

  reset_flash_stat();

  return delay;
}


/***********************************************************************
  Initialize Flash Drive 
  ***********************************************************************/

void initFlash()
{
  blk_t total_blk_num;
  blk_t total_util_blk_num;
  blk_t total_extr_blk_num;

  // total number of sectors    
  total_util_sect_num  = flash_numblocks;
  total_extra_sect_num = flash_extrblocks;
  total_sect_num = total_util_sect_num + total_extra_sect_num; 

  // total number of blocks 
  total_blk_num      = total_sect_num / SECT_NUM_PER_BLK;     // total block number
  total_util_blk_num = total_util_sect_num / SECT_NUM_PER_BLK;    // total unique block number

  global_total_blk_num = total_util_blk_num;

  total_extr_blk_num = total_blk_num - total_util_blk_num;        // total extra block number

  ASSERT(total_extr_blk_num != 0);

  if (nand_init(total_blk_num, 3) < 0) {
    EXIT(-4); 
  }

  switch(ftl_type){

    // pagemap
    case 1: ftl_op = pm_setup(); break;
    // blockmap
    //case 2: ftl_op = bm_setup(); break;
    // o-pagemap 
    case 3: ftl_op = opm_setup(); break;
    // fast
    case 4: ftl_op = lm_setup(); break;

    default: break;
  }

  ftl_op->init(total_util_blk_num, total_extr_blk_num);

  nand_stat_reset();
}

void printWearout()
{
  int i;
  FILE *fp = fopen("wearout", "w");
  
  for(i = 0; i<nand_blk_num; i++)
  {
    fprintf(fp, "%d %d\n", i, nand_blk[i].state.ec); 
  }

  fclose(fp);
}


void endFlash()
{
  nand_stat_print(outputfile);
  ftl_op->end;
  nand_end();
}  

/***********************************************************************
  Send request (lsn, sector_cnt, operation flag)
  ***********************************************************************/

void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag)
{
	int size;
	//size_t (*op_func)(sect_t lsn, size_t size);
	size_t (*op_func)(sect_t lsn, size_t size, int mapdir_flag);

        if((start_blk_no + block_cnt) >= total_util_sect_num){
          printf("start_blk_no: %d, block_cnt: %d, total_util_sect_num: %d\n", 
              start_blk_no, block_cnt, total_util_sect_num);
          exit(0);
        }

	switch(operation){

	//write
	case 0:

		op_func = ftl_op->write;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;
	//read
	case 1:


		op_func = ftl_op->read;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;

	default: 
		break;
	}
}

void find_real_max()
{
  int i; 

  for(i=0;i < MAP_REAL_MAX_ENTRIES; i++) {
      if(opagemap[real_arr[i]].map_age > opagemap[real_max].map_age) {
          real_max = real_arr[i];
      }
  }
}

void find_real_min()
{
  
  int i,index; 
  int temp = 99999999;

  for(i=0; i < MAP_REAL_MAX_ENTRIES; i++) {
        if(opagemap[real_arr[i]].map_age <= temp) {
            real_min = real_arr[i];
            temp = opagemap[real_arr[i]].map_age;
            index = i;
        }
  }    
}

int find_min_ghost_entry()
{
  int i; 

  int ghost_min = 0;
  int temp = 99999999; 

  for(i=0; i < MAP_GHOST_MAX_ENTRIES; i++) {
    if( opagemap[ghost_arr[i]].map_age <= temp) {
      ghost_min = ghost_arr[i];
      temp = opagemap[ghost_arr[i]].map_age;
    }
  }
  return ghost_min;
}

void init_arr()
{

  int i;
  for( i = 0; i < MAP_REAL_MAX_ENTRIES; i++) {
      real_arr[i] = -1;
  }
  for( i = 0; i < MAP_GHOST_MAX_ENTRIES; i++) {
      ghost_arr[i] = -1;
  }
  for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
      cache_arr[i] = -1;
  }

}

int search_table(int *arr, int size, int val) 
{
    int i;
    for(i =0 ; i < size; i++) {
        if(arr[i] == val) {
            return i;
        }
    }

    printf("shouldnt come here for search_table()=%d,%d",val,size);
    for( i = 0; i < size; i++) {
      if(arr[i] != -1) {
        printf("arr[%d]=%d ",i,arr[i]);
      }
    }
    exit(1);
    return -1;
}

int find_free_pos( int *arr, int size)
{
    int i;
    for(i = 0 ; i < size; i++) {
        if(arr[i] == -1) {
            return i;
        }
    } 
    printf("shouldnt come here for find_free_pos()");
    exit(1);
    return -1;
}

void find_min_cache()
{
  int i; 
  int temp = 999999;

  for(i=0; i < CACHE_MAX_ENTRIES ;i++) {
      if(opagemap[cache_arr[i]].cache_age <= temp ) {
          cache_min = cache_arr[i];
          temp = opagemap[cache_arr[i]].cache_age;
      }
  }
}

int youkim_flag1=0;

double callFsim(unsigned int secno, int scount, int operation)
{
  double delay; 
  int bcount;
  unsigned int blkno; // pageno for page based FTL
  int cnt,z; int min_ghost;

  int pos=-1,pos_real=-1,pos_ghost=-1;

  if(ftl_type == 1){ }

  if(ftl_type == 3) {
      page_num_for_2nd_map_table = (opagemap_num / MAP_ENTRIES_PER_PAGE);
    
      if(youkim_flag1 == 0 ) {
        youkim_flag1 = 1;
        init_arr();
      }

      if((opagemap_num % MAP_ENTRIES_PER_PAGE) != 0){
        page_num_for_2nd_map_table++;
      }
  }
      
  // page based FTL 
  if(ftl_type == 1 ) { 
    blkno = secno / 4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // block based FTL 
  else if(ftl_type == 2){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }
  // o-pagemap scheme
  else if(ftl_type == 3 ) { 
    blkno = secno / 4;
    blkno += page_num_for_2nd_map_table;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // FAST scheme
  else if(ftl_type == 4){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }

  cnt = bcount;

  switch(operation)
  {
    //write/read
    case 0:
    case 1:

    while(cnt > 0)
    {
          cnt--;

        // page based FTL
        if(ftl_type == 1){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // blck based FTL
        else if(ftl_type == 2){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // opagemap ftl scheme
        else if(ftl_type == 3)
        {

          /************************************************
            primary map table 
          *************************************************/
          //1. pagemap in SRAM 

            rqst_cnt++;
          if((opagemap[blkno].map_status == MAP_REAL) || (opagemap[blkno].map_status == MAP_GHOST))
          {
            cache_hit++;

            opagemap[blkno].map_age++;

            if(opagemap[blkno].map_status == MAP_GHOST){

              if ( real_min == -1 ) {
                real_min = 0;
                find_real_min();
              }    
              if(opagemap[real_min].map_age <= opagemap[blkno].map_age) 
              {
                find_real_min();  // probably the blkno is the new real_min alwaz
                opagemap[blkno].map_status = MAP_REAL;
                opagemap[real_min].map_status = MAP_GHOST;

                pos_ghost = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,blkno);
                ghost_arr[pos_ghost] = -1;
                
                pos_real = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos_real] = -1;

                real_arr[pos_real]   = blkno; 
                ghost_arr[pos_ghost] = real_min; 
              }
            }
            else if(opagemap[blkno].map_status == MAP_REAL) 
            {
              if ( real_max == -1 ) {
                real_max = 0;
                find_real_max();
                printf("Never happend\n");
              }

              if(opagemap[real_max].map_age <= opagemap[blkno].map_age)
              {
                real_max = blkno;
              }  
            }
            else {
              printf("forbidden/shouldnt happen real =%d , ghost =%d\n",MAP_REAL,MAP_GHOST);
            }
          }

          //2. opagemap not in SRAM 
          else
          {
            //if map table in SRAM is full
            if((MAP_REAL_MAX_ENTRIES - MAP_REAL_NUM_ENTRIES) == 0)
            {
              if((MAP_GHOST_MAX_ENTRIES - MAP_GHOST_NUM_ENTRIES) == 0)
              { //evict one entry from ghost cache to DRAM or Disk, delay = DRAM or disk write, 1 oob write for invalidation 
                min_ghost = find_min_ghost_entry();
                  evict++;

                if(opagemap[min_ghost].update == 1) {
                  update_reqd++;
                  opagemap[min_ghost].update = 0;
                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table then update it

                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 0, 2);   // write into 2nd mapping table 
                } 
                opagemap[min_ghost].map_status = MAP_INVALID;

                MAP_GHOST_NUM_ENTRIES--;

                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                MAP_GHOST_NUM_ENTRIES++;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;

                pos = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,min_ghost);
                ghost_arr[pos]=-1;

                
                ghost_arr[pos]= real_min;
                
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;
              }
              else{
                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;
               
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;

                pos = find_free_pos(ghost_arr,MAP_GHOST_MAX_ENTRIES);
                ghost_arr[pos]=real_min;
                
                MAP_GHOST_NUM_ENTRIES++;
              }
            }

            flash_hit++;
            send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table

            opagemap[blkno].map_status = MAP_REAL;

            opagemap[blkno].map_age = opagemap[real_max].map_age + 1;
            real_max = blkno;
            MAP_REAL_NUM_ENTRIES++;
            
            pos = find_free_pos(real_arr,MAP_REAL_MAX_ENTRIES);
            real_arr[pos] = blkno;
          }

         //comment out the next line when using cache
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // FAST scheme  
        else if(ftl_type == 4){ 

          if(operation == 0){
            write_count++;
          }
          else read_count++;

          send_flash_request(blkno*4, 4, operation, 1); //cache_min is a page for page baseed FTL
          blkno++;
        }
    }
    break;
  }

  delay = calculate_delay_flash();
  
  //printf("LPN is %d\t req_type is %d\t delay is %f\n",(blkno-cnt),operation,delay);

  return delay;
}


/******************************ZhouJie***************************************/
//创建双向链表
pNode CreateList()
{
    int i,length=0,data=0;
    //分配初始内存
    pNode  pHead=(pNode)malloc(sizeof(Node));
    if(NULL==pHead){
        printf("malloc for pHead failed!\n");
        exit(-1);
    }
    pHead->BlkNum=-1;
    pHead->Pre=pHead;
    pHead->Next=pHead;
    pHead->BlkSize=-1;
    for ( i = 0; i <PAGE_PRE_BLK ; ++i) {
        pHead->list[i]=-1;
    }

    return pHead;
}

//判断链表是否为空
int IsEmptyList(pNode pHead)
{
    pNode pt=pHead->Next;
    if(pt==pHead)
    {
        return 1;
    }else
    {
        return 0;
    }
}

//返回链表的长度
int GetListLength(pNode pHead)
{
    int length=0;
    pNode pt=pHead->Next;
    while (pt !=pHead)
    {
        length++;
        pt=pt->Next;
    }
    return length;
}

//返回当前的缓冲区的大小
int GetCacheSize(pNode pHead)
{
    int length=0;
    pNode pt=pHead->Next;
    while (pt !=pHead)
    {
        length+=pt->BlkSize;
        pt=pt->Next;
    }
    return length;
}

void FreeList(pNode pHead)
{
    pNode pt=pHead->Next,ps;
    while (pt!=pHead)
    {
        ps=pt;
        pt=pt->Next;
        free(ps);
    }
    free(pHead);
}


//输出打印链表
void PrintList(pNode pHead)
{
    int i;
    pNode pt=pHead->Next;
    printf("the list is follow:\n");
    while(pt!=pHead)
    {
        printf("the BlkNum is %d\t,the LPN is:\n",pt->BlkNum);
        for ( i = 0; i <pt->BlkSize ; ++i) {
            printf("%d\t",pt->list[i]);
        }
        printf("\n");
        pt=pt->Next;
    }
}

//初始化相应的配置参数
void InitVariable()
{
    buf_size=1024*4;
    //后续的统计变量
    buffer_cnt=0;
    buffer_hit=0;
    buffer_miss_cnt=0;
    buffer_read_hit=0;
    buffer_read_miss=0;
    buffer_write_hit=0;
    buffer_write_miss=0;
    //物理读写次数
    all_extra_physical_read=0;
    physical_read=0;
    physical_write=0;
    //设定当前的cache的长度
    curr_cache_size=0;
    //当前的块节点的数量
    BlkNodeSize=0;
}

void resetCacheStat()
{
    cache_read_num=0;
    cache_write_num=0;
}

double ComputeCacheDelay()
{
    double delay=0.0;
    double cache_read_delay=0.0;
    double cache_write_delay=0.0;
    cache_read_delay=(double)cache_read_num*(double)CACHE_READ_DELAY;
    cache_write_delay=(double)cache_write_num*(double)CACHE_WRITE_DELAY;
    delay=cache_read_delay+cache_write_delay;
    resetCacheStat();
    return delay;
}

//输出对应的统计结果
void PrintResultStat()
{
    //输出和缓冲区统计相关的信息
    printf("****************************************\n");
    printf("the all buffer req count is %d\n",buffer_cnt);
    printf("the buffer miss count is %d\n",buffer_miss_cnt);
    printf("the buffer hit count is %d\n",buffer_hit);
    printf("the hit rate is %f\n",((double)buffer_hit)/buffer_cnt);
    printf("the buffer read miss count is %d\n",buffer_read_miss);
    printf("the buffer write miss count is %d\n",buffer_write_miss);
    printf("the buffer read hit count is %d\n",buffer_read_hit);
    printf("the buffer write hit count is %d\n",buffer_write_hit);
    printf("the physical read count is %d\n",physical_read);
    printf("the physical write count is %d\n",physical_write);
}

//遍历链表，寻找链表中对应的数据，若存在则返回该节点的指针
pNode SearchList(pNode pHead,int Val)
{
    pNode pt=NULL;
    pNode ps=pHead->Next;
    while(ps!=pHead)
    {
        if(ps->BlkNum==Val)
        {
            pt=ps;
            break;
        }
        ps=ps->Next;
    }
    return pt;
}

//判断缓冲区中是否存在该LPN,命中块，没有命中Hit=0
pNode IsLPNInBuffer(int LPN,int *Hit)
{
    int tempBlk;
    int i,flag=0;
    pNode pBlk;
    tempBlk=LPN/PAGE_PRE_BLK;
    pBlk=SearchList(CacheHead,tempBlk);
    if(pBlk==NULL){
        (*Hit)=0;
        return pBlk;
    }else{
        for ( i = 0; i <PAGE_PRE_BLK ; ++i) {
            if(pBlk->list[i]==LPN){
                flag=1;
                break;
            }
        }
    }
    (*Hit)= flag == 1 ? 1 : 0;
    return pBlk;
}

//加入测试函数
int FindValueInArr(int *arr,int Size,int Val)
{
    int index=-1,i;
    for ( i = 0; i <Size ; ++i) {
        if(arr[i]==Val){
            index=i;
            break;
        }
    }
    return index;

}

//从数组中找空闲的位置
int FindFreePos(int *arr, int Size)
{
    int i,index=-1;
    for(i=0;i<Size;i++){
        if(arr[i]==-1){
            index=i;
            return index;
        }
    }
    return index;
}

//将新的请求加到对应的数据块的位置
double AddNewToBuffer(int LPN,pNode pBlk)
{
    double delay=0.0;
    int tempBlk;
    int i,free_pos;
    pNode pt;
    tempBlk=LPN/PAGE_PRE_BLK;


    //表示之前就不存在该块
    if(pBlk==NULL){
        pBlk=(pNode)malloc(sizeof(Node));
        if(pBlk==NULL){
            printf("error happend in AddNewToBuffer:\n");
            printf("malloc for New Blk failed\n");
            exit(-1);
        }
        pBlk->BlkNum=tempBlk;
        pBlk->BlkSize=1;
        for ( i = 0; i <PAGE_PRE_BLK ; ++i) {
            pBlk->list[i]=-1;
        }
        physical_read++;
        //delay=FLASH_READ_DELAY;
         delay=callFsim(LPN*4,4,1);
        pBlk->list[0]=LPN;
        //将新的块插入到队列的头部
        pt=CacheHead->Next;
        pBlk->Next=pt;
        pBlk->Pre=CacheHead;
        pt->Pre=pBlk;
        CacheHead->Next=pBlk;
        curr_cache_size++;
        //插入新的块，则统计当前块的节点书
        BlkNodeSize++;
        //错误检测
        if(BlkNodeSize!=GetListLength(CacheHead)){
            printf("error happend in AddNewToBuffer  1-1\n");
            printf("BlkNodeSize is %d\t,CacheHead-list size is %d\n",BlkNodeSize,GetListLength(CacheHead));
            exit(-1);
        }


    }else{


        //错误检测
        if(pBlk->BlkNum!=tempBlk){
            printf("error happend in AddNewToBuffer 1-2-1-1\n");
            printf("pblb-BlkNum is %d\t tempBlk is %d\n",pBlk->BlkNum,tempBlk);
            exit(-1);
        }

        //之前就存在该块
        pt=pBlk->Next;
        pt->Pre=pBlk->Pre;
        pBlk->Pre->Next=pt;
        //将命中的块插入到队列的头部
        pBlk->Next=CacheHead->Next;
        pBlk->Pre=CacheHead;
        CacheHead->Next->Pre=pBlk;
        CacheHead->Next=pBlk;

        //错误检测
        if(BlkNodeSize!=GetListLength(CacheHead)){
            printf("error happend in AddNewToBuffer  1-2-1\n");
            printf("BlkNodeSize is %d\t,CacheHead-list size is %d\n",BlkNodeSize,GetListLength(CacheHead));
            exit(-1);
        }

        free_pos=FindFreePos(pBlk->list,PAGE_PRE_BLK);
        //test
        if(free_pos==-1){
            printf("error happend in AddNewToBuffer:\n");
            printf("can not find free pos for LPN %d in pblk-list\n",LPN);
            exit(-1);
        }
        physical_read++;
        pBlk->list[free_pos]=LPN;
        pBlk->BlkSize++;
        //delay=FLASH_READ_DELAY;
        delay=callFsim(LPN*4,4,1);
        curr_cache_size++;

        //错误检测
        if(BlkNodeSize!=GetListLength(CacheHead)){
            printf("error happend in AddNewToBuffer  1-2-2\n");
            printf("BlkNodeSize is %d\t,CacheHead-list size is %d\n",BlkNodeSize,GetListLength(CacheHead));
            exit(-1);
        }

    }


    //错误检测
    if(BlkNodeSize!=GetListLength(CacheHead)){
        printf("error happend in AddNewToBuffer\n");
        printf("BlkNodeSize is %d\t,CacheHead-list size is %d\n",BlkNodeSize,GetListLength(CacheHead));
        exit(-1);
    }
    if(curr_cache_size!=GetCacheSize(CacheHead)){
        printf("error happend in AddNewToBuffer\n");
        printf("cache_size is %d\t,CacheHead-list cache size is %d\n",curr_cache_size,GetCacheSize(CacheHead));
        exit(-1);
    }




    return delay;
}

//将命中的数据块移动队列的头部
int MoveToMRU(pNode pHead,pNode pHit)
{
    pNode ps;
    ps=pHit->Pre;
    //删除原来位置中的链接关系
    ps->Next=pHit->Next;
    pHit->Next->Pre=ps;
    //插入到对应的位置
    ps=pHead->Next;
    pHit->Next=ps;
    pHit->Pre=pHead;
    pHead->Next=pHit;
    ps->Pre=pHit;
    //错误检测

    return 0;
}

//将数据插入到(int)arr数组指定的位置pos，pos之后的数据往后挪动一位
int InsertArr(int *arr,int size,int data,int pos)
{
    int j;
    //首先做一个错误检测
    if(pos>=size&&pos<0){
        printf("error happend in InsertArr: Insert-pos:%d over bound:0-%d",pos,size-1);
        exit(-1);
    }
    for ( j = size-1; j >pos ; j--) {
        arr[j]=arr[j-1];
    }
    //在pos的位置插入数据
    arr[pos]=data;
    return 0;
}

//计算数组中非负数个数
int calculate_arr_positive_num(int *arr,int size)
{
    int i;
    int count=0;
    for ( i = 0; i <size ; ++i) {
        if(arr[i]>=0){
            count++;
        }
    }
    return count;
}

//删除尾部的靠近LRU位置最大的数据块
double DelMaxLRUBlk(pNode pHead,pNode  *pblk)
{
    double  delay=0.0;
    int MaxSize=0;
    int DelSize,DelBlkNum;
    //聚簇回写使用的中间变量
    int i,j,k,startLPN,LPNSize,CurrStart,DelArr[PAGE_PRE_BLK],InsertFlag=0;
    pNode pt=NULL,pMax=NULL,ps=NULL;
    pt=pHead->Next;


    for ( i = 0; i <PAGE_PRE_BLK ; ++i) {
        DelArr[i]=-1;
    }
    //遍历块节点的链表
    while(pt!=pHead)
    {
        if(pt->BlkSize>MaxSize){
            MaxSize=pt->BlkSize;
            pMax=pt;
        }
        pt=pt->Next;
    }
    //pMax就是找到的最大删除块节点
    DelSize=pMax->BlkSize;
    DelBlkNum=pMax->BlkNum;
    physical_write+=DelSize;

    if(pMax==(*pblk)){
        //printf("error happend in DelMaxLRUBlk\n");
        printf("Del PMax is Hit BLK\n ");
        (*pblk)=NULL;
    }



    //聚簇回写
    //遍历list中找到该块的请求开始的地址StartLPN,同时升序排列请求地址
    startLPN=(DelBlkNum+1)*PAGE_PRE_BLK;
    for ( i = 0,j=0; i <PAGE_PRE_BLK&&j<DelSize ; ++i) {
        if(pMax->list[i]>=0){
            if(pMax->list[i]<startLPN){
                startLPN=pMax->list[i];
            }
            //同时读入回写的地址，按照升序排列
            InsertFlag=0;
            for(k=0;k<j;k++){
                if(pMax->list[i]<DelArr[k]){
                    InsertArr(DelArr,PAGE_PRE_BLK,pMax->list[i],k);
                    InsertFlag=1;
                    break;
                }
            }
            if(InsertFlag==0){
                DelArr[j]=pMax->list[i];
            }
            j++;
        }
    }
    //错误判断
    if(DelSize!=calculate_arr_positive_num(DelArr,PAGE_PRE_BLK)){
        printf("error happend in DelMaxLRUBlk\n");
        printf("DelSize is %d\t,DelArr-size is %d\n",DelSize,calculate_arr_positive_num(DelArr,PAGE_PRE_BLK));
        exit(-1);
    }

    LPNSize=1;
    //遍历依次回写
    for ( i = 1; i <DelSize ; ++i) {
        if(DelArr[i]-DelArr[i-1]!=1){
            //聚簇回写
            //delay+=LPNSize*FLASH_WRITE_DELAY;
            delay+=callFsim(startLPN*4,4*LPNSize,0);
            startLPN=DelArr[i];
            LPNSize=1;
        }else{
            LPNSize++;
        }
    }
    //delete histroy-leave
    if(LPNSize>0){
		    //delay+=LPNSize*FLASH_WRITE_DELAY;
            delay+=callFsim(startLPN*4,4*LPNSize,0);
            startLPN=DelArr[i];
            LPNSize=0;
	}
    
    
    //删除该节点
    ps=pMax->Pre;
    ps->Next=pMax->Next;
    pMax->Next->Pre=ps;
    free(pMax);
    BlkNodeSize--;
    curr_cache_size=curr_cache_size-DelSize;

    //错误测试
    if(BlkNodeSize!=GetListLength(CacheHead)){
        printf("error happend in DelMaxLRUBlk\n");
        printf("BlkNodeSize is %d\t,CacheHead-list size is %d\n",BlkNodeSize,GetListLength(CacheHead));
        exit(-1);
    }
    if(curr_cache_size!=GetCacheSize(CacheHead)){
        printf("error happend in DelMaxLRUBlk\n");
        printf("cache_size is %d\t,CacheHead-list cache size is %d\n",curr_cache_size,GetCacheSize(CacheHead));
        exit(-1);
    }

    return delay;
}

int ZJ_flag=0;
//观测输出的变量
int ObserveCycle=1;
int CycleCount=0;
double DelaySum=0.0;
double AveDelay=0.0;
//last
int Last_buffer_read_miss=0;
int Last_bufer_write_miss=0;
int Last_buffer_cnt=0;
int Last_buffer_miss_cnt=0;
//curr
int Curr_buffer_read_miss;
int Curr_buffer_write_miss;
int Curr_buffer_cnt;
int Curr_buff_miss_cnt;


double CacheManage(int secno,int scount,int operation )
{
    double delay=0.0;
    int blkno,bcount,cnt,tempBlkNum,Hitflag;
    double cache_delay=0.0;
    double flash_delay=0.0;
    pNode pblk=NULL;

    if(ZJ_flag==0){
        CacheHead=CreateList();
        InitVariable();
        ZJ_flag=1;
    }
    //  页对齐
    blkno= secno / SEC_PRE_PAGE;
    bcount = (secno + scount - 1) / SEC_PRE_PAGE - (secno) / SEC_PRE_PAGE + 1;
    resetCacheStat();
    cnt=bcount;

    if(operation==0){
        while(cnt>0) {
			buffer_cnt++;
            pblk = IsLPNInBuffer(blkno, &Hitflag);
            if (Hitflag == 1) {
                //命中缓冲区
                buffer_hit++;
                buffer_write_hit++;
                cache_write_num++;
                //将命中的数据块移动到MRU的位置
                MoveToMRU(CacheHead, pblk);
            } else {
                //没有命中缓冲区
                buffer_miss_cnt++;
                buffer_write_miss++;
                cache_write_num++;
                //查看缓冲区是否满，满的话选择最大的块删除，同等大的块选择最靠近LRU的块删除
                if (curr_cache_size >= buf_size) {
                    flash_delay += DelMaxLRUBlk(CacheHead,&pblk);
                    //做一个错误判断
                    if (curr_cache_size != GetCacheSize(CacheHead)) {
                        printf("error happend in rand_reqeust delLPN\n");
                        exit(-1);
                    }

                }
                //添加新的数据到缓冲区
                flash_delay += AddNewToBuffer(blkno, pblk);
                //做一个错误判断
                if (curr_cache_size != GetCacheSize(CacheHead)) {
                    printf("error happend in rand_reqeust AddNewToBuffer\n");
                    exit(-1);
                }
            }
            cnt--;
            blkno++;
        }
    }else {
        //读请求只是简单的查看缓冲区是否存在该LPN，没有直接从Flash中读取
        while (cnt > 0) {
            buffer_cnt++;
            pblk = IsLPNInBuffer(blkno, &Hitflag);
            if (Hitflag == 1) {
                //命中缓冲区
                buffer_hit++;
                buffer_read_hit++;
                cache_read_num++;
                //针对读命中不做缓冲区的移动操作（论文里面明确写到，只有块中数据想再次写中才移动整个块到头部）
            } else {
                //没有命中缓冲区，则直接调用callfsim
                buffer_miss_cnt++;
                buffer_read_miss++;
                physical_read++;
                //flash_delay += FLASH_READ_DELAY;
                flash_delay+=callFsim(blkno*4,4,1);
            }
            blkno++;
            cnt--;

        }
    }


    //计算时间延迟
    cache_delay=ComputeCacheDelay();
    delay=cache_delay+flash_delay;

    //周期观测数据的输出
    if(0 == CycleCount % ObserveCycle && CycleCount != 0){
        AveDelay=DelaySum/ObserveCycle;
        DelaySum=0.0;
        printf("the %d Request,the AveDelay is %f\n",CycleCount,AveDelay);
        Curr_buff_miss_cnt=buffer_miss_cnt-Last_buffer_miss_cnt;
        Curr_buffer_cnt=buffer_cnt-Last_buffer_cnt;
        Curr_buffer_read_miss=buffer_read_miss-Last_buffer_read_miss;
        Curr_buffer_write_miss=buffer_write_miss-Last_bufer_write_miss;
        printf("the Curr_buff_cnt is %d\tbuffer miss cnt is %d\n",Curr_buffer_cnt,Curr_buff_miss_cnt);
        printf("the Curr buff read miss is %d\t write miss is %d\n",Curr_buffer_read_miss,Curr_buffer_write_miss);
        Last_buffer_cnt=buffer_cnt;
        Last_buffer_miss_cnt=buffer_miss_cnt;
        Last_buffer_read_miss=buffer_read_miss;
        Last_bufer_write_miss=buffer_write_miss;
        printf("..............................................................\n");
        CycleCount++;
    }else{
        DelaySum+=delay;
        CycleCount++;
    }
    return  delay;
}




