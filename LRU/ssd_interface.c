/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu_
 *   
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * This source plays a role as bridiging disksim and flash simulator. 
 * 
 * Request processing flow: 
 *
 *  1. Request is sent to the simple flash device module. 
 *  2. This interface determines FTL type. Then, it sends the request
 *     to the lower layer according to the FTL type. 
 *  3. It returns total time taken for processing the request in the flash. 
 *
 */

#include "ssd_interface.h"
#include "disksim_global.h"
#include "dftl.h"

extern int merge_switch_num;
extern int merge_partial_num;
extern int merge_full_num;
int old_merge_switch_num = 0;
int old_merge_partial_num = 0;
int old_merge_full_num= 0;
int old_flash_gc_read_num = 0;
int old_flash_erase_num = 0;
int req_count_num = 1;
int cache_hit, rqst_cnt;
int flag1 = 1;
int count = 0;

int page_num_for_2nd_map_table;

#define MAP_REAL_MAX_ENTRIES 6552// real map table size in bytes
#define MAP_GHOST_MAX_ENTRIES 1640// ghost_num is no of entries chk if this is ok

#define CACHE_MAX_ENTRIES 4096
int ghost_arr[MAP_GHOST_MAX_ENTRIES];
int real_arr[MAP_REAL_MAX_ENTRIES];
int cache_arr[CACHE_MAX_ENTRIES];
/*********ZhouJie**********************/
int lru_cache_arr[CACHE_MAX_ENTRIES] ;
int clru_cache_arr[CACHE_MAX_ENTRIES] ;
int dlru_cache_arr[CACHE_MAX_ENTRIES] ;
/****************************************************************
 * 导入相关LRU的算法
 * **************************************************************/
//reset cache state
void reset_cache_stat()
{
  cache_read_num = 0;
  cache_write_num = 0;
}

//计算cache的操作时延
double calculate_delay_cache()
{
  double delay;
  double cache_read_delay=0.0,cache_write_delay=0.0;
  cache_read_delay=(double)CACHE_READ_DELAY*cache_read_num;
  cache_write_delay=(double)CACHE_WRITE_DELAY*cache_write_num;
  delay=cache_read_delay+cache_write_delay;
  reset_cache_stat();
  return delay;
}

//初始化对应统计变量
void InitVariable()
{
  cache_max_index=0;
  cache_min_index=0;
  //初始化统计变量
  buffer_cnt=0;
  buffer_hit_cnt=0;
  buffer_miss_cnt=0;
  buffer_write_hit=0;
  buffer_write_miss=0;
  buffer_read_hit=0;
  buffer_read_miss=0;
  physical_read=0;
  physical_write=0;
  //初始化相应的CACHE中间变量
  CACHE_LRU_NUM_ENTRIES=0;
  CACHE_CLRU_NUM_ENTRIES=0;
  CACHE_DLRU_NUM_ENTRIES=0;

}

void FreeVariable()
{
  //释放对应的内存
  if(NandPage!=NULL)
  {
    free(NandPage);
  }
}

//输出对应的统计结果
void PrintResultStat()
{
  //输出和缓冲区统计相关的信息
  printf("****************************************\n");
  printf("the all buffer req count is %d\n",buffer_cnt);
  printf("the buffer miss count is %d\n",buffer_miss_cnt);
  printf("the hit rate is %f\n",(double)(buffer_cnt-buffer_miss_cnt)/buffer_cnt);
  printf("the buffer read miss count is %d\n",buffer_read_miss);
  printf("the buffer write miss count is %d\n",buffer_write_miss);
  printf("the buffer read hit count is %d\n",buffer_read_hit);
  printf("the buffer write hit count is %d\n",buffer_write_hit);
  printf("the physical read count is %d\n",physical_read);
  printf("the physical write count is %d\n",physical_write);
}

int my_find_cache_max(int *arr,int arrMaxSize)
{
  int i;
  int temp=-1;
  int cache_max=-1;
  for ( i = 0; i <arrMaxSize ; ++i) {
    if (arr[i]!=-1){
      if (NandPage[arr[i]].cache_age>temp){
        temp=NandPage[arr[i]].cache_age;
        cache_max=arr[i];
      }
    }
  }
  return cache_max;
}


//找到数组中索引对应的LPN的age最大或最小
int my_find_cache_min(int *arr,int arrMaxSize)
{
  int i;
  int temp=99999999;
  int cache_min=-1;
  for (i = 0; i <arrMaxSize ; ++i) {
    //attention arr[i]=-1 ,the NandPage[-1] is error
    if(arr[i]!=-1){
      if(NandPage[arr[i]].cache_age<temp){
        temp=NandPage[arr[i]].cache_age;
        cache_min=arr[i];
      }
    }
  }
  return cache_min;
}

//计算数组中非负数个数
int calculate_arr_positive_num(int *arr,int size)
{
  int i;
  int count=0;
  for ( i = 0; i <size ; ++i) {
    if(arr[i]>=0){
      count++;
    }
  }
  return count;
}

//初始化数组，参考init_arr
void init_my_arr()
{
    int i;
    for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
        lru_cache_arr[i] = -1;
        clru_cache_arr[i] =-1;
        dlru_cache_arr[i] =-1;
    }
}

//判断对应的LPN是否在缓冲区中，如果存在返回在cache_arr中的位置索引,如果不存在则返回-1
int FindLPNInCache(int LPN)
{
    int object;
    //先做一个错误，是否存在LPN超出NandPage的大小
    if(LPN>NandPage_Num)
    {
        printf("the error happenend in FindLPNInCache function\n");
        printf("the LPN:%d over the database volume\n",LPN);
        printf("the NandPage Num is %d\n",NandPage_Num);
        exit(1);
    }
    //通过NandPage的下标快速定位是否存在
    if(NandPage[LPN].cache_status==CACHE_INVALID){
        return -1;
    }
    //下面这部分其实可以砍掉
    object=search_table(lru_cache_arr,CACHE_MAX_ENTRIES,LPN);
    if(object==-1){
        printf("the search table can not find LPN:%d in cache_arr\n",LPN);
        exit(1);
    }
    return object;
}

//命中后修改对应的NandPage的状态,无论命中还是初次加载数据页都需要调用更新NandPage状态
void UpdateNandPageStat(int LPN,int operation)
{
    //先做一个错误，是否存在LPN超出NandPage的大小
    if(LPN>NandPage_Num)
    {
        printf("error happened in UpdateNandPageStat\n");
        printf("the LPN:%d over the database volume\n",LPN);
        printf("the NandPage Num is %d\n",NandPage_Num);
        exit(1);
    }
    NandPage[LPN].cache_age=NandPage[cache_max_index].cache_age+1;
    cache_max_index=LPN;
    NandPage[LPN].cache_status=CACHE_VALID;
    //根据读写类型区别操作
    if (operation == 0) {
        NandPage[LPN].cache_update = 1;
    }
}

//重置对应的NandPage的状态为初始状态的值
void ResetNandPageStat(int LPN)
{
    //先做一个错误，是否存在LPN超出NandPage的大小
    if(LPN>NandPage_Num)
    {
        printf("error happened in ResetPageStat function\n");
        printf("the LPN:%d over the database volume\n",LPN);
        printf("the NandPage Num is %d\n",NandPage_Num);
        exit(1);
    }
    NandPage[LPN].cache_age=0;
    NandPage[LPN].cache_status=CACHE_INVALID;
    NandPage[LPN].cache_update=0;
}

//选择lru_cache_arr中的剔除对象，返回的是lru_cache_arr即将提出的对象的索引
int FindVictimInLruCacheArr()
{
    //做一个错误检测，判断当前的Cache大小计算是否正常
    int L;
    int lru_Victim;
    //计算当前数组中存在多少有效的数据项
    L=calculate_arr_positive_num(lru_cache_arr,CACHE_MAX_ENTRIES);
    if(CACHE_LRU_NUM_ENTRIES!=L) {
        printf("CACHE NUM ENTRIES is %d\n",CACHE_LRU_NUM_ENTRIES);
        printf("lru list entry number is %d\n",L);
        exit(1);
    }
    //找到对应的最小LPN
    cache_min_index=my_find_cache_min(lru_cache_arr,CACHE_MAX_ENTRIES);
    //找到对应的位置
    lru_Victim=search_table(lru_cache_arr,CACHE_MAX_ENTRIES,cache_min_index);
    //加入错误判读
    if (lru_Victim==-1){
        printf("the search_table find  vicitm of lru_arr error\n");
        printf("the Victim LPN is %d\n",cache_min_index);
        exit(-1);
    }
    return lru_Victim;
}

//剔除lru数组中的剔除对象并重置相应的NandPage，统计对应的物理回写次数,返回读写延迟
double DelVictimFromLRU(int Victim_page_index)
{
    int Victim_LPN;
    double delay=0.0;
    Victim_LPN=lru_cache_arr[Victim_page_index];
    //先做一个错误，是否存在LPN超出NandPage的大小
    if(Victim_LPN>NandPage_Num)
    {
        printf("error happened in DelVicitm function\n");
        printf("the LPN:%d over the database volume\n",Victim_LPN);
        printf("the NandPage Num is %d\n",NandPage_Num);
        exit(1);
    }
    if(NandPage[Victim_LPN].cache_update==1){
        physical_write++;
        //这里可以调用对应的函数计算相应的读写延迟
//        delay+=FLASH_WRITE_DELAY;
        delay=callFsim(Victim_LPN*4,4,0);
    }
    //重置对应的NandPage的状态
    ResetNandPageStat(Victim_LPN);
    //从对应的lru数组中删除对应的索引
    lru_cache_arr[Victim_page_index]=-1;
    CACHE_LRU_NUM_ENTRIES--;
    //加入错误判断
    if(CACHE_LRU_NUM_ENTRIES!=calculate_arr_positive_num(lru_cache_arr,CACHE_MAX_ENTRIES)){
        printf("lru数组中的有效项数和CACHE_LRU_NUM_ENTRIES不等\n");
        printf("CACHE_LRU_NUM_ENTRIES is %d\n",CACHE_LRU_NUM_ENTRIES);
        printf("lru-list number is %d\n",calculate_arr_positive_num(lru_cache_arr,CACHE_MAX_ENTRIES));
        exit(1);
    }
    return delay;
}

double AddNewToLRU(int LPN,int operation)
{
    double delay=0.0;
    int free_pos=-1;
    //加入错误判断
    if(CACHE_LRU_NUM_ENTRIES!=calculate_arr_positive_num(lru_cache_arr,CACHE_MAX_ENTRIES)){
        printf("error happenend in AddNewtoLRU\n");
        printf("lru数组中的有效项数和CACHE_LRU_NUM_ENTRIES不等\n");
        printf("CACHE_LRU_NUM_ENTRIES is %d\n",CACHE_LRU_NUM_ENTRIES);
        printf("lru-list number is %d\n",calculate_arr_positive_num(lru_cache_arr,CACHE_NUM_ENTRIES));
        exit(1);
    }
    //找到数组中空余的位置
    free_pos=find_free_pos(lru_cache_arr,CACHE_MAX_ENTRIES);
    if(free_pos==-1){
        printf("can't find free position for current LPN %d \n",LPN);
        printf("the lru_cache_arr num is %d\n",calculate_arr_positive_num(lru_cache_arr,CACHE_MAX_ENTRIES));
        printf("CACHE_LRU_NUM_ENTRIES is %d\n",CACHE_LRU_NUM_ENTRIES);
        exit(-1);
    }
    lru_cache_arr[free_pos]=LPN;
    CACHE_LRU_NUM_ENTRIES++;
    //计算相应的时延
//    delay+=FLASH_READ_DELAY;
    delay+=callFsim(LPN*4,4,1);
    //更新对应的NandPage的状态
    UpdateNandPageStat(LPN,operation);
    if(operation==0){
        buffer_write_miss++;
    }else{
        buffer_read_miss++;
    }
    return delay;
}



/***********************************************************************
  Variables for statistics    
 ***********************************************************************/
unsigned int cnt_read = 0;
unsigned int cnt_write = 0;
unsigned int cnt_delete = 0;
unsigned int cnt_evict_from_flash = 0;
unsigned int cnt_evict_into_disk = 0;
unsigned int cnt_fetch_miss_from_disk = 0;
unsigned int cnt_fetch_miss_into_flash = 0;

double sum_of_queue_time = 0.0;
double sum_of_service_time = 0.0;
double sum_of_response_time = 0.0;
unsigned int total_num_of_req = 0;

/***********************************************************************
  Mapping table
 ***********************************************************************/
int real_min = -1;
int real_max = 0;

/***********************************************************************
  Cache
 ***********************************************************************/
int cache_min = -1;
int cache_max = 0;

// Interface between disksim & fsim 

void reset_flash_stat()
{
  flash_read_num = 0;
  flash_write_num = 0;
  flash_gc_read_num = 0;
  flash_gc_write_num = 0; 
  flash_erase_num = 0;
  flash_oob_read_num = 0;
  flash_oob_write_num = 0; 
}

FILE *fp_flash_stat;
FILE *fp_gc;
FILE *fp_gc_timeseries;
double gc_di =0 ,gc_ti=0;


double calculate_delay_flash()
{
  double delay;
  double read_delay, write_delay;
  double erase_delay;
  double gc_read_delay, gc_write_delay;
  double oob_write_delay, oob_read_delay;

  oob_read_delay  = (double)OOB_READ_DELAY  * flash_oob_read_num;
  oob_write_delay = (double)OOB_WRITE_DELAY * flash_oob_write_num;

  read_delay     = (double)READ_DELAY  * flash_read_num; 
  write_delay    = (double)WRITE_DELAY * flash_write_num; 
  erase_delay    = (double)ERASE_DELAY * flash_erase_num; 

  gc_read_delay  = (double)GC_READ_DELAY  * flash_gc_read_num; 
  gc_write_delay = (double)GC_WRITE_DELAY * flash_gc_write_num; 


  delay = read_delay + write_delay + erase_delay + gc_read_delay + gc_write_delay + 
    oob_read_delay + oob_write_delay;

  if( flash_gc_read_num > 0 || flash_gc_write_num > 0 || flash_erase_num > 0 ) {
    gc_ti += delay;
  }
  else {
    gc_di += delay;
  }

  if(warm_done == 1){
    fprintf(fp_gc_timeseries, "%d\t%d\t%d\t%d\t%d\t%d\n", 
      req_count_num, merge_switch_num - old_merge_switch_num, 
      merge_partial_num - old_merge_partial_num, 
      merge_full_num - old_merge_full_num, 
      flash_gc_read_num,
      flash_erase_num);

    old_merge_switch_num = merge_switch_num;
    old_merge_partial_num = merge_partial_num;
    old_merge_full_num = merge_full_num;
    req_count_num++;
  }

  reset_flash_stat();

  return delay;
}


/***********************************************************************
  Initialize Flash Drive 
  ***********************************************************************/

void initFlash()
{
  blk_t total_blk_num;
  blk_t total_util_blk_num;
  blk_t total_extr_blk_num;

  // total number of sectors    
  total_util_sect_num  = flash_numblocks;
  total_extra_sect_num = flash_extrblocks;
  total_sect_num = total_util_sect_num + total_extra_sect_num; 

  // total number of blocks 
  total_blk_num      = total_sect_num / SECT_NUM_PER_BLK;     // total block number
  total_util_blk_num = total_util_sect_num / SECT_NUM_PER_BLK;    // total unique block number

  global_total_blk_num = total_util_blk_num;

  total_extr_blk_num = total_blk_num - total_util_blk_num;        // total extra block number

  ASSERT(total_extr_blk_num != 0);

  if (nand_init(total_blk_num, 3) < 0) {
    EXIT(-4); 
  }

  switch(ftl_type){

    // pagemap
    case 1: ftl_op = pm_setup(); break;
    // blockmap
    //case 2: ftl_op = bm_setup(); break;
    // o-pagemap 
    case 3: ftl_op = opm_setup(); break;
    // fast
    case 4: ftl_op = lm_setup(); break;

    default: break;
  }

  ftl_op->init(total_util_blk_num, total_extr_blk_num);

  nand_stat_reset();
}

void printWearout()
{
  int i;
  FILE *fp = fopen("wearout", "w");
  
  for(i = 0; i<nand_blk_num; i++)
  {
    fprintf(fp, "%d %d\n", i, nand_blk[i].state.ec); 
  }

  fclose(fp);
}


void endFlash()
{
  nand_stat_print(outputfile);
  ftl_op->end;
  nand_end();
}  

/***********************************************************************
  Send request (lsn, sector_cnt, operation flag)
  ***********************************************************************/

void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag)
{
	int size;
	//size_t (*op_func)(sect_t lsn, size_t size);
	size_t (*op_func)(sect_t lsn, size_t size, int mapdir_flag);

        if((start_blk_no + block_cnt) >= total_util_sect_num){
          printf("start_blk_no: %d, block_cnt: %d, total_util_sect_num: %d\n", 
              start_blk_no, block_cnt, total_util_sect_num);
          exit(0);
        }

	switch(operation){

	//write
	case 0:

		op_func = ftl_op->write;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;
	//read
	case 1:


		op_func = ftl_op->read;
		while (block_cnt> 0) {
			size = op_func(start_blk_no, block_cnt, mapdir_flag);
			start_blk_no += size;
			block_cnt-=size;
		}
		break;

	default: 
		break;
	}
}

void find_real_max()
{
  int i; 

  for(i=0;i < MAP_REAL_MAX_ENTRIES; i++) {
      if(opagemap[real_arr[i]].map_age > opagemap[real_max].map_age) {
          real_max = real_arr[i];
      }
  }
}

void find_real_min()
{
  
  int i,index; 
  int temp = 99999999;

  for(i=0; i < MAP_REAL_MAX_ENTRIES; i++) {
        if(opagemap[real_arr[i]].map_age <= temp) {
            real_min = real_arr[i];
            temp = opagemap[real_arr[i]].map_age;
            index = i;
        }
  }    
}

int find_min_ghost_entry()
{
  int i; 

  int ghost_min = 0;
  int temp = 99999999; 

  for(i=0; i < MAP_GHOST_MAX_ENTRIES; i++) {
    if( opagemap[ghost_arr[i]].map_age <= temp) {
      ghost_min = ghost_arr[i];
      temp = opagemap[ghost_arr[i]].map_age;
    }
  }
  return ghost_min;
}

void init_arr()
{

  int i;
  for( i = 0; i < MAP_REAL_MAX_ENTRIES; i++) {
      real_arr[i] = -1;
  }
  for( i = 0; i < MAP_GHOST_MAX_ENTRIES; i++) {
      ghost_arr[i] = -1;
  }
  for( i = 0; i < CACHE_MAX_ENTRIES; i++) {
      cache_arr[i] = -1;
  }

}

/**
 * int search_table(int *arr, int size, int val) 
{
    int i;
    for(i =0 ; i < size; i++) {
        if(arr[i] == val) {
            return i;
        }
    }

    printf("shouldnt come here for search_table()=%d,%d",val,size);
    for( i = 0; i < size; i++) {
      if(arr[i] != -1) {
        printf("arr[%d]=%d ",i,arr[i]);
      }
    }
    exit(1);
    return -1;
}*
* */

int search_table(int *arr,int size,int val)
{
    int i;
    for (i = 0; i <size ; ++i) {
        if(arr[i]==val){
            return i;
        }
    }
    return -1;
}

int find_free_pos( int *arr, int size)
{
    int i;
    for(i = 0 ; i < size; i++) {
        if(arr[i] == -1) {
            return i;
        }
    } 
   // printf("shouldnt come here for find_free_pos()");
   // exit(1);
    return -1;
}

void find_min_cache()
{
  int i; 
  int temp = 999999;

  for(i=0; i < CACHE_MAX_ENTRIES ;i++) {
      if(opagemap[cache_arr[i]].cache_age <= temp ) {
          cache_min = cache_arr[i];
          temp = opagemap[cache_arr[i]].cache_age;
      }
  }
}

int youkim_flag1=0;

double callFsim(unsigned int secno, int scount, int operation)
{
  double delay; 
  int bcount;
  unsigned int blkno; // pageno for page based FTL
  int cnt,z; int min_ghost;

  int pos=-1,pos_real=-1,pos_ghost=-1;

  if(ftl_type == 1){ }

  if(ftl_type == 3) {
      page_num_for_2nd_map_table = (opagemap_num / MAP_ENTRIES_PER_PAGE);
    
      if(youkim_flag1 == 0 ) {
        youkim_flag1 = 1;
        init_arr();
      }

      if((opagemap_num % MAP_ENTRIES_PER_PAGE) != 0){
        page_num_for_2nd_map_table++;
      }
  }
      
  // page based FTL 
  if(ftl_type == 1 ) { 
    blkno = secno / 4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // block based FTL 
  else if(ftl_type == 2){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }
  // o-pagemap scheme
  else if(ftl_type == 3 ) { 
    blkno = secno / 4;
    blkno += page_num_for_2nd_map_table;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }  
  // FAST scheme
  else if(ftl_type == 4){
    blkno = secno/4;
    bcount = (secno + scount -1)/4 - (secno)/4 + 1;
  }

  cnt = bcount;

  switch(operation)
  {
    //write/read
    case 0:
    case 1:

    while(cnt > 0)
    {
          cnt--;

        // page based FTL
        if(ftl_type == 1){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // blck based FTL
        else if(ftl_type == 2){
          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // opagemap ftl scheme
        else if(ftl_type == 3)
        {

          /************************************************
            primary map table 
          *************************************************/
          //1. pagemap in SRAM 

            rqst_cnt++;
          if((opagemap[blkno].map_status == MAP_REAL) || (opagemap[blkno].map_status == MAP_GHOST))
          {
            cache_hit++;

            opagemap[blkno].map_age++;

            if(opagemap[blkno].map_status == MAP_GHOST){

              if ( real_min == -1 ) {
                real_min = 0;
                find_real_min();
              }    
              if(opagemap[real_min].map_age <= opagemap[blkno].map_age) 
              {
                find_real_min();  // probably the blkno is the new real_min alwaz
                opagemap[blkno].map_status = MAP_REAL;
                opagemap[real_min].map_status = MAP_GHOST;

                pos_ghost = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,blkno);
                ghost_arr[pos_ghost] = -1;
                
                pos_real = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos_real] = -1;

                real_arr[pos_real]   = blkno; 
                ghost_arr[pos_ghost] = real_min; 
              }
            }
            else if(opagemap[blkno].map_status == MAP_REAL) 
            {
              if ( real_max == -1 ) {
                real_max = 0;
                find_real_max();
                printf("Never happend\n");
              }

              if(opagemap[real_max].map_age <= opagemap[blkno].map_age)
              {
                real_max = blkno;
              }  
            }
            else {
              printf("forbidden/shouldnt happen real =%d , ghost =%d\n",MAP_REAL,MAP_GHOST);
            }
          }

          //2. opagemap not in SRAM 
          else
          {
            //if map table in SRAM is full
            if((MAP_REAL_MAX_ENTRIES - MAP_REAL_NUM_ENTRIES) == 0)
            {
              if((MAP_GHOST_MAX_ENTRIES - MAP_GHOST_NUM_ENTRIES) == 0)
              { //evict one entry from ghost cache to DRAM or Disk, delay = DRAM or disk write, 1 oob write for invalidation 
                min_ghost = find_min_ghost_entry();
                  evict++;

                if(opagemap[min_ghost].update == 1) {
                  update_reqd++;
                  opagemap[min_ghost].update = 0;
                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table then update it

                  send_flash_request(((min_ghost-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 0, 2);   // write into 2nd mapping table 
                } 
                opagemap[min_ghost].map_status = MAP_INVALID;

                MAP_GHOST_NUM_ENTRIES--;

                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                MAP_GHOST_NUM_ENTRIES++;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;

                pos = search_table(ghost_arr,MAP_GHOST_MAX_ENTRIES,min_ghost);
                ghost_arr[pos]=-1;

                
                ghost_arr[pos]= real_min;
                
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;
              }
              else{
                //evict one entry from real cache to ghost cache 
                MAP_REAL_NUM_ENTRIES--;
                find_real_min();
                opagemap[real_min].map_status = MAP_GHOST;
               
                pos = search_table(real_arr,MAP_REAL_MAX_ENTRIES,real_min);
                real_arr[pos]=-1;

                pos = find_free_pos(ghost_arr,MAP_GHOST_MAX_ENTRIES);
                ghost_arr[pos]=real_min;
                
                MAP_GHOST_NUM_ENTRIES++;
              }
            }

            flash_hit++;
            send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4, 4, 1, 2);   // read from 2nd mapping table

            opagemap[blkno].map_status = MAP_REAL;

            opagemap[blkno].map_age = opagemap[real_max].map_age + 1;
            real_max = blkno;
            MAP_REAL_NUM_ENTRIES++;
            
            pos = find_free_pos(real_arr,MAP_REAL_MAX_ENTRIES);
            real_arr[pos] = blkno;
          }

         //comment out the next line when using cache
          if(operation==0){
            write_count++;
            opagemap[blkno].update = 1;
          }
          else
             read_count++;

          send_flash_request(blkno*4, 4, operation, 1); 
          blkno++;
        }

        // FAST scheme  
        else if(ftl_type == 4){ 

          if(operation == 0){
            write_count++;
          }
          else read_count++;

          send_flash_request(blkno*4, 4, operation, 1); //cache_min is a page for page baseed FTL
          blkno++;
        }
    }
    break;
  }

  delay = calculate_delay_flash();
  //printf("LPN is %d\t req_type is %d\t delay is %f\n",(blkno-cnt),operation,delay);

  return delay;
}

/****************************************************************
 * 导入相关LRU的算法
 * **************************************************************/
/**************************************************
 *
 * @param secno
 * @param scount
 * @param operation
 * @return delay
 * 这个函数在DelVictim和AddNewToLRU的函数里面调用了callfsim函数计算时延
 */

int ZJ_flag=0;
// code run and show variable
int ObserveCycle=10000;
int CycleCount=0;
double DelaySum=0.0;
double AveDelay=0.0;
//last
int Last_buffer_read_miss=0;
int Last_bufer_write_miss=0;
int Last_buffer_cnt=0;
int Last_buffer_miss_cnt=0;
//curr
int Curr_buffer_read_miss;
int Curr_buffer_write_miss;
int Curr_buffer_cnt;
int Curr_buff_miss_cnt;



double CacheManage(unsigned int secno,int scount,int operation)
{
    //定义时延变量
    double delay,flash_delay=0,cache_delay=0;
    int bcount,entry_count;
    unsigned int blkno;
    //定义算法需要使用的中间变量
    int  free_pos=-1;
    int  vicitm_page_index=-1;
    int HitIndex;
    int  cnt=0;
//   第一次访问注意初始化
    if(ZJ_flag==0){
        InitVariable();
        init_my_arr();
        ZJ_flag=1;
    }
    //页对齐操作
    blkno=secno/4;
    bcount=(secno+scount-1)/4-(secno)/4+1;
    //重置cache_stat相关状态
    reset_cache_stat();
    cnt=bcount;
    while(cnt>0){
        buffer_cnt++;
        HitIndex=FindLPNInCache(blkno);
        if(HitIndex>0){
            //命中lru
            buffer_hit_cnt++;
            UpdateNandPageStat(blkno,operation);
            if(operation==0){
                buffer_write_hit++;
                cache_write_num++;
            }else
            {
                buffer_read_hit++;
                cache_read_num++;
            }

        }else{
            //未命中lru
            buffer_miss_cnt++;
            if(CACHE_LRU_NUM_ENTRIES>=CACHE_MAX_ENTRIES){
                //启动剔除操作
                vicitm_page_index=FindVictimInLruCacheArr();
                flash_delay+=DelVictimFromLRU(vicitm_page_index);
            }
            //将新的数据加入到lru
            flash_delay+=AddNewToLRU(blkno,operation);
        }
        cnt--;
        blkno++;
    }
    cache_delay=calculate_delay_cache();
    delay=cache_delay+flash_delay;
    //test point
    //printf("LRU alogrithm:\tthe request blkno is %d\tcache_delay is %f flash_delay is %f\n",(blkno-bcount),cache_delay,flash_delay);
    if(CycleCount%ObserveCycle==0&&CycleCount!=0){
		AveDelay=DelaySum/ObserveCycle;
		DelaySum=0.0;
		printf("the %d Request,the AveDelay is %f\n",CycleCount,AveDelay);
		Curr_buff_miss_cnt=buffer_miss_cnt-Last_buffer_miss_cnt;
		Curr_buffer_cnt=buffer_cnt-Last_buffer_cnt;
		Curr_buffer_read_miss=buffer_read_miss-Last_buffer_read_miss;
		Curr_buffer_write_miss=buffer_write_miss-Last_bufer_write_miss;
		printf("the Curr_buff_cnt is %d\tbuffer miss cnt is %d\n",Curr_buffer_cnt,Curr_buff_miss_cnt);
		printf("the Curr buff read miss is %d\t write miss is %d\n",Curr_buffer_read_miss,Curr_buffer_write_miss);
		Last_buffer_cnt=buffer_cnt;
		Last_buffer_miss_cnt=buffer_miss_cnt;
		Last_buffer_read_miss=buffer_read_miss;
		Last_bufer_write_miss=buffer_write_miss;
		printf("...........\n");
		CycleCount++;
	}else{
		DelaySum+=delay;
		CycleCount++;
	}
    
    
    return delay;
}

