/* 
 * Contributors: Youngjae Kim (youkim@cse.psu.edu)
 *               Aayush Gupta (axg354@cse.psu.edu)
 *
 * In case if you have any doubts or questions, kindly write to: youkim@cse.psu.edu 
 *
 * Description: This is a header file for ssd_interface.c.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fast.h"
#include "pagemap.h"
#include "flash.h"
#include "type.h"

#define READ_DELAY        (0.1309/4)
#define WRITE_DELAY       (0.4059/4)
#define ERASE_DELAY       1.5 
#define GC_READ_DELAY  READ_DELAY    // gc read_delay = read delay    
#define GC_WRITE_DELAY WRITE_DELAY  // gc write_delay = write delay 

#define OOB_READ_DELAY    0.0
#define OOB_WRITE_DELAY   0.0

struct ftl_operation * ftl_op;

#define PAGE_READ     0
#define PAGE_WRITE    1
#define OOB_READ      2
#define OOB_WRITE     3
#define BLOCK_ERASE   4
#define GC_PAGE_READ  5
#define GC_PAGE_WRITE 6

void reset_flash_stat();
double calculate_delay_flash();
void initFlash();
void endFlash();
void printWearout();
void send_flash_request(int start_blk_no, int block_cnt, int operation, int mapdir_flag);
void find_real_max();
void find_real_min();
int find_min_ghost_entry();
void synchronize_disk_flash();
void find_min_cache();
double callFsim(unsigned int secno, int scount, int operation);

int write_count;
int read_count;

int flash_read_num;
int flash_write_num;
int flash_gc_read_num;
int flash_gc_write_num;
int flash_erase_num;
int flash_oob_read_num;
int flash_oob_write_num;

int map_flash_read_num;
int map_flash_write_num;
int map_flash_gc_read_num;
int map_flash_gc_write_num;
int map_flash_erase_num;
int map_flash_oob_read_num;
int map_flash_oob_write_num;

int ftl_type;

extern int total_util_sect_num; 
extern int total_extra_sect_num;

int global_total_blk_num;

int warm_done; 

int total_er_cnt;
int flag_er_cnt;
int block_er_flag[20000];
int block_dead_flag[20000];
int wear_level_flag[20000];
int unique_blk_num; 
int unique_log_blk_num;
int last_unique_log_blk;

int total_extr_blk_num;
int total_init_blk_num;

//和缓冲区相关的状态标识
#define BUF_INVALID 0
#define BUF_VALID 1
#define BUF_INCLRU 2
#define BUF_INDLRU 3
//定义cache的读写延时
#define CACHE_READ_DELAY 0.0005
#define CACHE_WRITE_DELAY 0.0005
//自己定义flash的读写延迟
#define FLASH_READ_DELAY 0.05
#define FLASH_WRITE_DELAY 0.25
//cache (read/write count) variable
int cache_read_num;
int cache_write_num;
//表示当前的CLRU和DLRU状态
int CACHE_CLRU_NUM_ENTRIES;
int CACHE_DLRU_NUM_ENTRIES;
int CACHE_LRU_NUM_ENTRIES;

//当前的cache的age最小的索引，也就是LPN，NandPage的下标
int cache_min_index;
int cache_max_index;
//定义算法需要用到的结构体
//page_entry表示所用数据页在缓冲区可能的状态
struct my_page_entry{
    int cache_status;
    int cache_age;//在缓冲区滞留的时间，越久age越大
    int cache_update;//缓冲区更新的标志
};
unsigned  int NandPage_Num;//只表示数据页
struct  my_page_entry *NandPage;
//关于cache的读写统计 在对应的fast.c中的lm_init初始化
int buffer_cnt;
int buffer_hit_cnt;
int buffer_miss_cnt;
int buffer_write_hit;
int buffer_write_miss;
int buffer_read_hit;
int buffer_read_miss;
int physical_write;
int physical_read;

//和算法相关的函数定义
//测试需要用的初始化函数
void InitVariable();//初始化统计变量,还有分配NandPage的内存
//打印输出最后统计的结果
void PrintResultStat();
//注意释放分配的内存
void FreeVariable();
void init_my_arr();//初始化自己算法中所需要用到的数组
void reset_cache_stat();
double calculate_delay_cache();
int my_find_cache_min(int *arr,int arrMaxSize);
int my_find_cache_max(int *arr,int arrMaxSize);
int search_table(int *arr,int size,int val);
int calculate_arr_positive_num(int *arr,int size);
int find_free_pos(int *arr,int size);
double CacheManage(unsigned int secno,int scount,int operation);

int FindLPNInCache(int LPN);
//命中后修改对应的NandPage的状态,无论命中还是初次加载数据页都需要调用更新NandPage状态
void UpdateNandPageStat(int LPN,int operation);
//重置对应的NandPage的状态为初始状态的值
void ResetNandPageStat(int LPN);
//选择lru_cache_arr中的剔除对象，返回的是lru_cache_arr即将提出的对象的索引
int FindVictimInLruCacheArr();
//剔除lru数组中的剔除对象并重置相应的NandPage，统计对应的物理回写次数
double DelVictimFromLRU(int Victim_page_index);
double AddNewToLRU(int LPN,int operation);

